//==================================================================================================
//
// This code is compute the free-free Gaunt factors and integrated cross sections in the general
// case. The expressions from Elwert & Haug, 1969 are used.
//
//==================================================================================================
//
// Author: Jens Chluba
// first implementation: Aug 2019
// last modification   : Aug 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>
#include <complex>

//==================================================================================================
// required libs
//==================================================================================================
#include "routines.h"
#include "parser.h"
#include "Definitions.h"
#include "physical_consts.h"
#include "Patterson.h"
#include "Hyper_Geometric_2F1.h"
#include "Evaluate_G_ell.h"
#include "Bremsstrahlung.h"
#include "Bremsstrahlung-simple-functions.h"
#include "CUBA_wrapper.h"
#include "Gaunt_factor_thermal_average.h"
#include "Gaunt_factor_interpolation.h"
#include "Gaunt_factor_th_interpolation.h"
#include "Matrix_element_interpolation.h"
#include "interpolate_vH.h"

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

string selected_variables="default";

//==================================================================================================
string glob_selnorm="default";

double norm_function(double p1, double omega, int Z)
{
    if(glob_selnorm=="default") return 1.0;
    if(glob_selnorm=="cm^2") return 1.0e-27;
    else if(glob_selnorm=="omega/Z^2") return omega/Z/Z;

    cerr << " not a valid norm. Returning unity " << endl;
    return 1.0;
}

//==================================================================================================
// Structs constructors and distructors
//==================================================================================================
void Momenta_and_Angles::set_default_Momenta_and_Angles()
{
    p1=-1.0; omega=w=-1.0; p2=-1.0;
    gamma1=gamma2=-1.0;
    mu1=mu2=-10.0;
    phi=-10.0;
    
    Dxi=-1.0; Dmu21=Dp21=0.0;
    S1=S2=DS21=ximin=mut=o2=0.0;
    kap1=kap2=kap12=1.0;
    info="";
    xi_interpol=1.0e+7;
    
    return;
}

Momenta_and_Angles::Momenta_and_Angles(){ set_default_Momenta_and_Angles(); }   

Momenta_and_Angles::Momenta_and_Angles(double p1v, double omv, double mu1v)
{
    set_default_Momenta_and_Angles();
    p1=p1v; omega=omv;
    mu1=mu1v;

    compute_energy_values();
}

void Momenta_and_Angles::compute_derived_values()
{
    compute_energy_values();

    double wp1p2=p1*p1+p2*p2+omega*omega;
    //mut=pow((p1+p2)/omega, 2)-1.0;
    mut=2.0*(p1*p1+p1*p2-gamma1*omega)/o2;                     // numerically more stable
    DS21=DS21_func(mu1, mu2, Dmu21);
    S1=sqrt(1.0-mu1*mu1); S2=S1+DS21;
    kap1=2.0*(gamma1-p1*mu1); kap2=2.0*(gamma2-p2*mu2);
    kap12=kap1*kap2;
    
    Dxitot= 4.0*mut*p1*p2*S1*S2/kap12;                         // Factor of 2 was missing!
    
    ximin = 2.0*Dp21*Dmu21*omega;
    ximin+= omega*omega + 2.0*(p1*Dmu21+Dp21*mu1)*omega;
    ximin+= Dp21*(Dp21-2.0*p1*(S1*DS21+mu1*Dmu21));
    ximin+=-2.0*p1*p1*(S1*DS21+mu1*Dmu21);
    ximin*= mut/kap12;
    
    if(selected_variables=="xi") phi=acos(1.0-2.0*Dxi/Dxitot); // Factor of 2 was missing!
    else Dxi=-Dxitot*(cos(phi)-1.0)/2.0;                       // Factor of 1/2 was missing!

    xi=ximin+Dxi;

    return;
}

double Momenta_and_Angles::compute_Dxi(double phi)
{
    compute_derived_values();
    return (1.0-cos(phi))*Dxitot/2.0;
}

void Momenta_and_Angles::compute_energy_values()
{
    gamma1=gamma_func(p1);
    gamma2=gamma1-omega;
    if(gamma2>=1.0) p2=sqrt(p1*p1 + omega*(omega-2.0*gamma1));
    
    o2=omega*omega;
    E1=Emax_func(p1);
    E2=Emax_func(p2);
    w=omega/E1;
    Dp21=Dp2p1(*this);
    
    return;
}

void Momenta_and_Angles::set_values(double p1v, double omv, double mu1v, double mu2v, double phiv)
{
    p1=p1v; omega=omv;
    mu1=mu1v; mu2=mu2v; Dmu21=mu2-mu1;
    phi=phiv; Dxi=-1.0;
    
    compute_energy_values();
    compute_derived_values();
    
    return;
}

void Momenta_and_Angles::set_values_xi(double p1v, double omv, double mu1v, double Dmu21v, double Dxiv)
{
    p1=p1v; omega=omv;
    mu1=mu1v; Dmu21=Dmu21v; mu2=mu1+Dmu21;
    phi=-10.0; Dxi=Dxiv;
    
    compute_energy_values();
    compute_derived_values();
    
    return;
}

int Momenta_and_Angles::check_values()
{
    if(p1<0.0 || p2<0.0) return 1;
    if(gamma1<1.0 || gamma2<1.0) return 1;
    if(mu1<-1.0 || mu1>1.0) return 2;
    if(mu2<-1.0 || mu2>1.0) return 2;
    if(phi<0.0 || phi>TWOPI) return 2;
    
    return 0;
}

void Momenta_and_Angles::show()
{
    cout << p1 << " p2 = " << p2 << " " << omega << " " << Dxi << endl;
    cout << gamma1 << " " << gamma2 << endl;
    cout << mu1 << " " << mu2 << " " << phi << endl;
}

//==================================================================================================
// cross section functions
//==================================================================================================
void set_integration_scheme(string Integrator="CUBA", bool mess=1);

#include "./Bremsstrahlung-plugins/Bremsstrahlung-NR.cpp"
#include "./Bremsstrahlung-plugins/Bremsstrahlung-BH.cpp"
#include "./Bremsstrahlung-plugins/Bremsstrahlung-EH.cpp"

//==================================================================================================
double epsrel_precision_DEH   =5.0e-5;
double epsrel_precision_DEH_xi=5.0e-5;

double relative_precision_estimate(double epsrel, Momenta_and_Angles &V, int Z)
{
    if(V.info=="DEH" && selected_variables=="default") return epsrel_precision_DEH;
    if(V.info=="DEH" && selected_variables=="xi"     ) return epsrel_precision_DEH_xi;

    return epsrel;
}

double absolute_precision_estimate(double epsrel, Momenta_and_Angles &V, int Z)
{
    double ds=dsigma_dk_K(V, Z);
    ds=min(ds, dsigma_dk_BH_ana(V, Z));
    
    return max(1.0e-60, epsrel/4.0 * ds/(FOURPI*FOURPI) );
}

#include "./Bremsstrahlung-plugins/Patterson_wrapper.cpp"
#include "./Bremsstrahlung-plugins/Bremsstrahlung-parser.cpp"
#include "./Bremsstrahlung-plugins/Bremsstrahlung-batch.cpp"

//==================================================================================================
// Wrappers to choose the integration scheme
//==================================================================================================
double (*dsigma_dk_scheme)(void *p)=dsigma_dk;
double (*dsigma_dkdmu1_scheme)(double mu1, void *p)=dsigma_dkdmu1;
double (*dsigma_dkdOmega1dmu2_scheme)(double mu2, void *p)=dsigma_dkdOmega1dmu2;

void set_integration_scheme(string Integrator, bool mess)
{
    if(Integrator=="Patterson")
    {
        if(mess) cout << "\n set_integration_scheme:: Patterson integrator." << endl;
        
        selected_variables="default";
        dsigma_dk_scheme=dsigma_dk;
        dsigma_dkdmu1_scheme=dsigma_dkdmu1;
        dsigma_dkdOmega1dmu2_scheme=dsigma_dkdOmega1dmu2;
    }
    else if(Integrator=="Patterson-xi")
    {
        if(mess) cout << "\n set_integration_scheme:: Patterson integrator with xi-variables." << endl;
        
        selected_variables="xi";
        dsigma_dk_scheme=dsigma_dk_Dxi_I0;
        dsigma_dkdmu1_scheme=dsigma_dk_Dxi_I1;
        dsigma_dkdOmega1dmu2_scheme=dsigma_dk_Dxi_I2;
    }
    else if(Integrator=="CUBA")
    {
        if(mess) cout << "\n set_integration_scheme:: CUBA integrator." << endl;

        selected_variables="default";
        dsigma_dk_scheme=dsigma_dk_cuba;
        dsigma_dkdmu1_scheme=dsigma_dkdmu1_cuba;
        dsigma_dkdOmega1dmu2_scheme=dsigma_dkdOmega1dmu2_cuba;
    }
    else if(Integrator=="CUBA-xi")
    {
        if(mess) cout << "\n set_integration_scheme:: CUBA integrator with xi-variables." << endl;
        
        selected_variables="xi";
        dsigma_dk_scheme=dsigma_dk_Dxi_I0_cuba;
//         dsigma_dkdmu1_scheme=dsigma_dk_Dxi_I1_cuba;
//         dsigma_dkdOmega1dmu2_scheme=dsigma_dk_Dxi_I2_cuba;
    }
    else throw_error("set_integration_scheme", "integration scheme not found", 1);
    
    if(mess) cout << endl;
    
    return;
}

//==================================================================================================
// select cross section
//==================================================================================================
dsigma_func chose_cross_section(string sel="EH")
{
    if(selected_variables=="default")
    {
        if(sel=="EH") return dsigma_dkdOmega1dOmega2;
        else if(sel=="EH-interpol") return dsigma_dkdOmega1dOmega2_int;
        else if(sel=="EH-I") return dsigma_dkdOmega1dOmega2_I;
        else if(sel=="DEH" || sel=="DEH-interpol") return dsigma_dkdOmega1dOmega2_DEH;
        else if(sel=="BH") return dsigma_dkdOmega1dOmega2_BH;
        else if(sel=="EH-soft") return dsigma_dkdOmega1dOmega2_soft;
        else cerr << " chose_cross_section:: cross section not recognized" << endl;
    }
    
    cerr << " chose_cross_section:: variable not chosen" << endl;
    exit(0);
}

//==================================================================================================
// differential in all variables
//==================================================================================================
double dsigma_dkdOmega1dOmega2(Momenta_and_Angles &V, int Z, string sel="EH")
{
    return chose_cross_section(sel)(V, Z);
}

double dsigma_dkdOmega1dOmega2(double p1, double omega, double mu1, double mu2,
                               double phi, int Z, string sel="EH")
{
    Momenta_and_Angles V;
    V.set_values(p1, omega, mu1, mu2, phi);
    
    return dsigma_dkdOmega1dOmega2(V, Z, sel);
}

//==================================================================================================
// dphi or dDxi carried out
//==================================================================================================
double dsigma_dkdOmega1dmu2(double p1, double omega, double mu1, double mu2, int Z, string sel="EH")
{
    Momenta_and_Angles V(p1, omega, mu1);
    Integration_variables IV(Z, V, chose_cross_section(sel));

    return dsigma_dkdOmega1dmu2_scheme(mu2, (void *)&IV);
}

//==================================================================================================
// dphi and dmu2 carried out
//==================================================================================================
double dsigma_dkdOmega1(double p1, double omega, double mu1, int Z, string sel="EH")
{
    Momenta_and_Angles V(p1, omega, mu1);
    Integration_variables IV(Z, V, chose_cross_section(sel));

    return dsigma_dkdmu1_scheme(mu1, (void *)&IV);
}

//==================================================================================================
// only differential in domega
//==================================================================================================
double dsigma_dk(double p1, double omega, int Z, string sel="EH")
{
    Momenta_and_Angles V(p1, omega, 0.0);
    Integration_variables IV(Z, V, chose_cross_section(sel));

    return dsigma_dk_scheme((void *)&IV);
}

//==================================================================================================
// xi-variables
//==================================================================================================
dsigma_func chose_cross_section_xi(string sel="BH")
{
    if(selected_variables=="xi")
    {
        if(sel=="EH" || sel=="EH-interpol") return dsigma_dkdOmega1dOmega2_xi;
        else if(sel=="DEH") return dsigma_dkdOmega1dOmega2_xi_DEH;
        else if(sel=="DEH-interpol") return dsigma_dkdOmega1dOmega2_xi_DEH_int;
        else if(sel=="BH") return dsigma_dkdOmega1dOmega2_BH_xi;
        else if(sel=="EH-soft") return dsigma_dkdOmega1dOmega2_xi_soft;
        else cerr << " chose_cross_section_xi:: cross section not recognized" << endl;
    }
    
    cerr << " chose_cross_section_xi:: variable not chosen" << endl;
    exit(0);
}

//==================================================================================================
double dsigma_dkdOmega1dOmega2_xi(Momenta_and_Angles &V, int Z, string sel="BH")
{
    return chose_cross_section_xi(sel)(V, Z);
}

double dsigma_dkdOmega1dOmega2_xi(double p1, double omega, double mu1, double Dmu2,
                                  double Dxi, int Z, string sel="BH")
{
    Momenta_and_Angles V;
    V.set_values_xi(p1, omega, mu1, Dmu2, Dxi);
    
    return dsigma_dkdOmega1dOmega2_xi(V, Z, sel);
}

//==================================================================================================
double dsigma_dkdOmega1dmu2_xi(double p1, double omega, double mu1, double Dmu2, int Z, string sel="BH")
{
    Momenta_and_Angles V(p1, omega, mu1);
    Integration_variables IV(Z, V, chose_cross_section_xi(sel));
    
    return dsigma_dkdOmega1dmu2_scheme(Dmu2, (void *)&IV);
}

double dsigma_dkdOmega1_xi(double p1, double omega, double mu1, int Z, string sel="BH")
{
    Momenta_and_Angles V(p1, omega, mu1);
    Integration_variables IV(Z, V, chose_cross_section_xi(sel));
    
    return dsigma_dkdmu1_scheme(mu1, (void *)&IV);
}

double dsigma_dk_xi(double p1, double omega, int Z, string sel="BH")
{
    Momenta_and_Angles V(p1, omega, 0.0);
    Integration_variables IV(Z, V, chose_cross_section_xi(sel));
    
    return dsigma_dk_scheme((void *)&IV);
}

//==================================================================================================
// only differential in domega analytic
//==================================================================================================
double dsigma_dk_K(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return dsigma_dk_K(V, Z);
}

double dsigma_dk_BH_ana(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return dsigma_dk_BH_ana(V, Z);
}

double dsigma_dk_NR(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return dsigma_dk_NR(V, Z);
}

double dsigma_dk_NR_soft(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return dsigma_dk_NR_soft(V, Z);
}

double dsigma_dk_NR_rel(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return V.gamma1*V.gamma1*dsigma_dk_NR(V, Z);
}

double dsigma_dk_NR_rel_soft(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return V.gamma1*V.gamma1*dsigma_dk_NR_soft(V, Z);
}

//==================================================================================================
// general interface to cross section and Gaunt factors
//==================================================================================================
double dsigma_dk_sel(double p1, double omega, int Z, string sel)
{
    //----------------------------------------------------------------------------------------------
    // analytic cases which do not depend on chosen variables
    //----------------------------------------------------------------------------------------------
    if(sel=="K") return dsigma_dk_K(p1, omega, Z);
    else if(sel=="BHa") return dsigma_dk_BH_ana(p1, omega, Z);
    //
    else if(sel=="NR") return dsigma_dk_NR(p1, omega, Z);
    else if(sel=="NR-soft") return dsigma_dk_NR_soft(p1, omega, Z);
    else if(sel=="NR-interpol")
        return dsigma_dk_K(p1, omega, Z)*Gaunt_interpolated_NR(p1, omega, Z);
    //
    else if(sel=="NR-rel") return dsigma_dk_NR_rel(p1, omega, Z);
    else if(sel=="NR-soft-rel") return dsigma_dk_NR_rel_soft(p1, omega, Z);
    else if(sel=="NR-interpol-rel")
    {
        double gamma1=sqrt(1.0+p1*p1);
        double a1=Z*const_alpha*gamma1/p1;
        double f=gamma1*gamma1; //*pow(one_minus_exp_mx(TWOPI*a1)/(a1), 2);
        
        return f*dsigma_dk_K(p1, omega, Z)*Gaunt_interpolated_NR(p1, omega, Z);
    }
    else if(sel=="EH-Z-interpol")
        return dsigma_dk_K(p1, omega, Z)*Gaunt_interpolated_EH(p1, omega, Z);

    else if(sel=="Gen") return dsigma_dk_K(p1, omega, Z)*Gaunt_interpolated_general(p1, omega, Z);

    else if(sel=="vH")
        return dsigma_dk_K(p1, omega, Z)*Gaunt_factor_vH(p1, omega, Z);

    //----------------------------------------------------------------------------------------------
    // numerical cases
    //----------------------------------------------------------------------------------------------
    if(selected_variables=="default")
    {
        // numerical integration with default variables
        if(sel=="EH" || sel=="EH-interpol" || sel=="EH-soft" || sel=="BH")
            return dsigma_dk(p1, omega, Z, sel);
        
        else if(sel=="DEH" || sel=="DEH-interpol")
            return dsigma_dk(p1, omega, Z, sel) + dsigma_dk_BH_ana(p1, omega, Z);
    }
    else if(selected_variables=="xi")
    {
        // numerical integration with xi variables
        if(sel=="EH" || sel=="EH-interpol" || sel=="EH-soft" || sel=="BH")
            return dsigma_dk_xi(p1, omega, Z, sel);
        
        else if(sel=="DEH" || sel=="DEH-interpol")
            return dsigma_dk_xi(p1, omega, Z, sel) + dsigma_dk_BH_ana(p1, omega, Z);
    }
    
    cerr << " dsigma_dk_sel:: selection not allowed sel= " << sel << endl; exit(1);
    
    return 0.0;
}

double dsigma_dk_sel_xi(double p1, double omega, int Z, string sel)
{
    return dsigma_dk_sel(p1, omega, Z, sel);
}

double Gaunt_sel(double p1, double omega, int Z, string sel)
{
    return dsigma_dk_sel(p1, omega, Z, sel)/dsigma_dk_K(p1, omega, Z);
}

double Gaunt_sel_rel(double p1, double omega, int Z, string sel)
{
    return dsigma_dk_sel(p1, omega, Z, sel)/( (1.0+p1*p1)*dsigma_dk_K(p1, omega, Z) );
}

double Gaunt_sel_BH(double p1, double omega, int Z, string sel)
{
    return dsigma_dk_sel(p1, omega, Z, sel)/dsigma_dk_BH_ana(p1, omega, Z);
}

//==================================================================================================
// general interface for thermally-averaged Gaunt factor.
// The thermal average is carried out using precomputed tables and analytic expressions
//--------------------------------------------------------------------------------------------------
// cases:
// sel == "NR", "KL", "NR-rel"           : Karzas & Latter results
// sel == "BHa"                          : Bethe-Heitler analytic
// sel == "Gen"                          : general Elwert-Haug results for Z=1, 2, 3 and 4
//==================================================================================================
double Gaunt_th_sel(double Th_e, double x, int Z, string sel, string therm_av_limit)
{
    double (*Gaunt)(double p1, double omega, int Z);

    if(sel=="NR") Gaunt=Gaunt_interpolated_NR;
    else if(sel=="NR-rel") Gaunt=Gaunt_interpolated_NR_rel_corr;
    else if(sel=="KL") Gaunt=Gaunt_interpolated_NR_KL;
    else if(sel=="Gen") Gaunt=Gaunt_interpolated_general;
    else if(sel=="BHa") Gaunt=Gaunt_BH_ana;
    else throw_error("Gaunt_th_sel", "case not implented", 1);

    return Thermally_averaged_gaunt_factor(x*Th_e, Th_e, Z, Gaunt, therm_av_limit);
}

double Gaunt_th_sel(double Th_e, double x, int Z, string sel)
{
    return Gaunt_th_sel(Th_e, x, Z, sel, "exact");
}

double Gaunt_th_sel_rel(double Th_e, double x, int Z, string sel)
{
    return Gaunt_th_sel(Th_e, x, Z, sel, "exact-hf");
}

// precomputed tables an high frequency extrapolation
double Gaunt_th_interpol(double Th_e, double x, int Z)
{
    return Gaunt_th_interpolated(Th_e, x, Z);
}

//==================================================================================================
#include "./Bremsstrahlung-plugins/Bremsstrahlung-outputs.cpp"
#include "./Bremsstrahlung-plugins/Bremsstrahlung-tests.cpp"

//==================================================================================================
// main to play with library
//==================================================================================================
int main(int narg, char *args[])
{
    //----------------------------------------------------------------------------------------------
    // set Hypergeometric function precision
    //----------------------------------------------------------------------------------------------
    set_precision_2F1(1.0e-12);
    
    //test_cross_sections();
    //test_G0_ODE_solver();
    
    //cout << Gaunt_th_interpolated(0.001, 0.1, 1) << endl;
    //cout << Gaunt_th_sel_rel(0.001, 0.1, 1, "Gen") << endl;
    //exit(1);

    //----------------------------------------------------------------------------------------------
    // preparing runs with runmodes and runfiles if requested
    //----------------------------------------------------------------------------------------------
    string rfname=args[narg-1];
    string mode;
    
    if(narg==2 || narg==3 || narg==4) mode=args[1];
    else if(narg==1) mode="default";
    else throw_error("main", "not a possible runmode", 1);

    //----------------------------------------------------------------------------------------------
    // read parameters and create output directory if not already there
    //----------------------------------------------------------------------------------------------
    Run_Parameters RPars;
    if(mode!="default" && narg>2) read_runfile_info(rfname, RPars);

    bool dir_c=check_if_dir_exists(RPars.path);
    if(!dir_c) create_directory_if_it_does_not_exist(RPars.path);
    
    //----------------------------------------------------------------------------------------------
    // write runfiles for distributed computation
    //----------------------------------------------------------------------------------------------
    if(mode=="generate-scripts")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung generate-scripts runfile", 1);
        
        if(RPars.nthreads<=0) throw_error("main", "give number of threads >0", 1);

        create_scripts_Gaunt_factor_tables(RPars.p1min, RPars.p1max, RPars.np,
                                           RPars.wmin, RPars.nw, RPars.Z,
                                           RPars.low_freq_integrator,
                                           RPars.high_freq_integrator,
                                           RPars.batchmode,
                                           RPars.sel,
                                           RPars.nthreads,
                                           RPars.path);
    }
    //----------------------------------------------------------------------------------------------
    // compute Gaunt factor tables according to loaded info from runfile
    //----------------------------------------------------------------------------------------------
    else if(mode=="Gaunt-split")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-split runfile", 1);

        string fname=RPars.path+"/Gaunt_"+RPars.sel+RPars.add;
        
        output_Gaunt_factors_batch(fname, RPars.p1start, RPars.p1end,
                                   RPars.p1min, RPars.p1max, RPars.np,
                                   RPars.wmin, RPars.nw, RPars.Z,
                                   RPars.low_freq_integrator,
                                   RPars.high_freq_integrator,
                                   RPars.batchmode,
                                   RPars.sel, RPars.verbosity);
    }
    //----------------------------------------------------------------------------------------------
    // tables of thermally-averaged Gaunt factors
    //----------------------------------------------------------------------------------------------
    else if(mode=="Gaunt-th-table")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-th-table runfile", 1);
        
        string fname=RPars.path+"/Gaunt_th_Z"+int_to_string(RPars.Z, 2)+"_"+RPars.sel+RPars.add;

        
        output_Gaunt_factors_batch_th(fname,
                                      RPars.The_min, RPars.The_max, RPars.nThe,
                                      RPars.xmin, RPars.xmax, RPars.nx, RPars.Z,
                                      RPars.sel, RPars.batchmode, RPars.verbosity);
    }
    //----------------------------------------------------------------------------------------------
    // thermally-averaged Gaunt factor output
    //----------------------------------------------------------------------------------------------
    else if(mode=="Gaunt-thermal")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-thermal runfile", 1);

        double Th_e=RPars.Th_e;
        string label="-"+RPars.therm_av_limit+"_Z"+int_to_string(RPars.Z)+RPars.add;
/*
        if(Th_e<=1.2e-3)
        {
            // special case of Karzas & Latter
            output_thermally_averaged_Gaunts(RPars.path+"/Gaunt-th_av-NR-KL"+label,
                                             Th_e, RPars.Z, RPars.xmin, RPars.xmax,
                                             "NR-KL", "Karzas", RPars.nx);
            
            output_thermally_averaged_Gaunts(RPars.path+"/Gaunt-th_av-NR"+label,
                                             Th_e, RPars.Z, RPars.xmin, RPars.xmax,
                                             "NR", RPars.therm_av_limit, RPars.nx);
            
            output_thermally_averaged_Gaunts(RPars.path+"/Gaunt-th_av-NR-rel"+label,
                                             Th_e, RPars.Z, RPars.xmin, RPars.xmax,
                                             "NR-rel", RPars.therm_av_limit, RPars.nx);
        }
        
        output_thermally_averaged_Gaunts(RPars.path+"/Gaunt-th_av-BH"+label,
                                         Th_e, RPars.Z, RPars.xmin, RPars.xmax,
                                         "BH", RPars.therm_av_limit, RPars.nx);
*/
        output_thermally_averaged_Gaunts(RPars.path+"/Gaunt-th_av-Gen"+label,
                                         Th_e, RPars.Z, RPars.xmin, RPars.xmax,
                                         "Gen", RPars.therm_av_limit, RPars.nx);
    }
    //----------------------------------------------------------------------------------------------
    // Gaunt factor outputs for fixed values of p1
    //----------------------------------------------------------------------------------------------
    else if(mode=="Gaunt-outputs")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-outputs runfile", 1);

        string label="_Z"+int_to_string(RPars.Z)+RPars.add;
        output_Gaunt_factors(RPars.path+"/Gaunt_BH_ana"+label, RPars.wmin, "BHa", RPars.Z, RPars.nw);
        output_Gaunt_factors(RPars.path+"/Gaunt_NR_ana"+label, RPars.wmin, "NR-interpol", RPars.Z, RPars.nw);
        output_Gaunt_factors(RPars.path+"/Gaunt_Gen"+label, RPars.wmin, "Gen", RPars.Z, RPars.nw);
        output_Gaunt_factors(RPars.path+"/Gaunt_vH"+label, RPars.wmin, "vH", RPars.Z, RPars.nw);
    }
    else if(mode=="Gaunt-outputs-p1")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-outputs-p1 runfile", 1);
        
        string label="_Z"+int_to_string(RPars.Z)+RPars.add;
        
        output_Gaunt_factors(RPars.path+"/Gaunt_BH_ana"+label, RPars.p1vals, RPars.wmin,
                             "BHa", RPars.Z, RPars.nw);
        
        output_Gaunt_factors(RPars.path+"/Gaunt_NR_ana"+label, RPars.p1vals, RPars.wmin,
                             "NR-interpol", RPars.Z, RPars.nw);
        
        output_Gaunt_factors(RPars.path+"/Gaunt_Gen"+label, RPars.p1vals, RPars.wmin,
                             "Gen", RPars.Z, RPars.nw);
        
        output_Gaunt_factors(RPars.path+"/Gaunt_vH"+label, RPars.p1vals, RPars.wmin,
                             "vH", RPars.Z, RPars.nw);
    }

    //----------------------------------------------------------------------------------------------
    // Gaunt factor outputs for fixed values of p1 and varying Z
    //----------------------------------------------------------------------------------------------
    else if(mode=="Gaunt-outputs-Z")
    {
        if(narg!=3) throw_error("main", "usage: ./Bremsstrahlung Gaunt-outputs-Z runfile", 1);

        string label=RPars.add;
        
        output_Gaunt_factors_Z(RPars.path+"/Gaunt_EH_BH-Z"+label,
                               RPars.wmin, RPars.p1vals[0], "Gen", RPars.nw);
        
        output_Gaunt_factors_Z(RPars.path+"/Gaunt_NR_BH-Z"+label,
                               RPars.wmin, RPars.p1vals[0], "NR-interpol", RPars.nw);
    }
    //----------------------------------------------------------------------------------------------
    // test some functions if of interest
    //----------------------------------------------------------------------------------------------
    else if(mode=="tests")
    {
        test_G_ell();
        test_Hyper_Geo_2F1();
        test_functions();
        test_Gaunt_factor_interpolation();  // to check Gaunt-factor interpolation scheme
        test_Gaunt_factor_Z_scaling();      // to check specific values for NR case with rescaling
    }
    //----------------------------------------------------------------------------------------------
    // print help
    //----------------------------------------------------------------------------------------------
    else if(mode=="-h" || mode=="--help")
    {
        cout << "usage: ./Bremsstrahlung [mode] [runfile]" << endl;
        cout << "mode == 'generate-scripts', 'Gaunt-split', 'Gaunt-th-table', "
             << "'Gaunt-thermal', 'Gaunt-outputs', 'Gaunt-outputs-p1', 'Gaunt-outputs-Z', 'tests'" << endl;
    }

    if(mode!="default") exit(0);

    //==============================================================================================
    //
    // previous calculations
    //
    //==============================================================================================
    double p1=0.05, oh=Emax_func(p1);
    
    //----------------------------------------------------------------------------------------------
    // output differential cross section for various cases
    //----------------------------------------------------------------------------------------------
    //output_dsig_dkdOmega1dOmega2("./outputs/BH-diff_phi.dat", p1, oh*0.9, -0.5, -0.5, "BH", 50000);
    //output_dsig_dkdOmega1dOmega2("./outputs/EH-diff_phi.dat", p1, oh*0.9, -0.5, -0.5, "EH", 50000);
    //output_dsig_dkdOmega1dmu2("./outputs/Bethe-Heitler-differential.dat", p1, oh*1.0e-2, -0.0, "BH", 10000);
    //output_dsig_dkdOmega1dmu2("./outputs/Elwert-Haug-differential.dat", p1, oh*1.0e-1, -0.0, "EH", 10000);
    
    //set_integration_scheme("Patterson");
    //output_dsig_dkdOmega1dOmega2("./outputs/EH-diff.dat", p1, oh*0.9, -0.5, -0.5, "DEH", 50000);
    
    //output_dsig_dkdOmega1dOmega2_xi("./outputs/BH-diff_xi.dat", p1, oh*0.9, -0.5, -0.5, "BH", 50000);
    //output_dsig_dkdOmega1dOmega2_xi("./outputs/EH-diff_xi.dat", p1, oh*0.9, -0.5, -0.5, "EH", 50000);
    //output_dsig_dkdOmega1dmu2("./outputs/Bethe-Heitler-differential-xi.dat", p1, oh*1.0e-8, -0.1, "BH", 10000);
    //output_dsig_dkdOmega1("./outputs/Bethe-Heitler-differential-xi-mu1.dat", p1, oh*1.0e-8, "BH", 500);
    //exit(1);

    //----------------------------------------------------------------------------------------------
    // output Gaunt factors using various integration schemes and variables
    //----------------------------------------------------------------------------------------------
    output_Gaunt_factors("./outputs/Gaunt_EH-test-Z10.dat", 0.01, 1.0e-10, "DEH-interpol", 10, 200);

    return 0;
}

//==================================================================================================
//==================================================================================================
