#============================================================================================
# definitions
#============================================================================================
TOOLS_DIR = ./Tools

MAKE_COMMAND = make -f Makefile.in TOOLS_DIR=$(TOOLS_DIR)

#============================================================================================
# rules
#============================================================================================
all: 
	$(MAKE_COMMAND) all

lib: 
	$(MAKE_COMMAND) lib

bin: 
	$(MAKE_COMMAND) bin
	
pub:
	make -f Makefile.pub all

pubtar:
	make -f Makefile.pub tarball
	
#============================================================================================
# rules to clean up
#============================================================================================
clean:
	rm -f *.o

cleanall:
	rm -f *.o *~ Bremsstrahlung libBremsstrahlung.a

cleanallDEV:
	make cleanall
	rm -f $(TOOLS_DIR)/*.o $(TOOLS_DIR)/*.~
	rm -f $(TOOLS_DIR)/CUBA_wrapper/*.o $(TOOLS_DIR)/CUBA_wrapper/*.~
	rm -f $(TOOLS_DIR)/Definitions/*.o $(TOOLS_DIR)/Definitions/*.~
	rm -f $(TOOLS_DIR)/Simple_routines/*.o $(TOOLS_DIR)/Simple_routines/*.~
	rm -f $(TOOLS_DIR)/Integration/*.o $(TOOLS_DIR)/Integration/*~
	rm -f $(TOOLS_DIR)/Hypergeometric-Gell/*.o $(TOOLS_DIR)/Hypergeometric-Gell/*~
	rm -f $(TOOLS_DIR)/Bremsstrahlung-Tools/*.o $(TOOLS_DIR)/Bremsstrahlung-Tools/*~
	rm -f $(TOOLS_DIR)/van-hoof/*.o $(TOOLS_DIR)/van-hoof/*~
	rm -f $(TOOLS_DIR)/ODE_Solver/*.o $(TOOLS_DIR)/ODE_Solver/*~

tidy:
	make cleanallDEV

wipeDS:
	find . -type f -name \.DS_Store -print | xargs rm

#============================================================================================
#============================================================================================
