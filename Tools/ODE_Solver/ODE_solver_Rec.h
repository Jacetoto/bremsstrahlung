//==================================================================================================
// ODE_solver routines
//
// Purpose: solve a system of stiff ODEs with moderate dimension (n<~100).
//
// Basic Aspects: The solver is based on a 6th order Gears method (implicit && stiffly-stable) with
// variable time-step. The linear algebra system is solved iteratively using a biconjugate gradient
// method. A guess for the next solution is extrapolated from the previous solutions;
//
// Author: Jens Chluba with contributions from Geoff Vasil at CITA
//
// First implementation: 12/01/2010
// Last modification   : 31/03/2019
//==================================================================================================
// 31/03/2019: added Jacobian matrix setup check, which is useful for debugging [JC]
// 07/02/2019: tidied up solver structures using pointers [JC]
// 26/01/2019: added some step-size change settings + cleaned up the old routine [JC]
// 16/04/2018: added simple interpolation routine using the stored solutions
// 15/10/2013: added adaptive stepsize control to PDE+ODE+ADE routine
//
// 03/01/2012: modified PDE + ODE solver to allow additional OAEs.
//
// 28/06/2011: added option that allows to solve PDE+ODE systems, where the boundary conditions
//             are communicated as algebraic equations;
//
// 15/06/2011: set max order to 5; This was making the code a bit faster;
//             tidied the code a bit;
//==================================================================================================

#ifndef _ODE_SOLVER_REC_H_
#define _ODE_SOLVER_REC_H_

#include <vector>
#include <string>

using namespace std;

namespace ODE_solver_Rec
{
    
    void wait_f_r_loc(string mess);
    
    //==============================================================================================
    // Structure that contains the solution y and its derivative dy at redshift z
    //==============================================================================================
    struct ODE_solver_Solution
    {
        double z;
        vector<double> y;
        vector<double> dy;
    };
    
    //==============================================================================================
    struct ODE_solver_accuracies
    {
        vector<double> rel;
        vector<double> abs;
    };
    
    //==============================================================================================
    struct ODE_solver_matrix
    {
        vector<double> A;
        vector<int> row;
        vector<int> col;
        vector<int> diags;
    };

    //==============================================================================================
    struct ODE_Solver_data
    {
        void (*fcn_col_ptr)(int *neq, double *z, double *y, double *f, int col);
        void (*fcn_col_para_ptr)(int *neq, double *z, double *y, double *f, int col, void *p);
        bool num_jac;
        bool check_jacobian; // to check consistency of Jacobian
        //
        ODE_solver_Solution Stemp;
        ODE_solver_Solution *Sptr;
        ODE_solver_Solution *Snewptr, *Sguessptr;
        ODE_solver_Solution *Snptr[6];
        //
        ODE_solver_matrix *Jacobian_ptr;
        //
        vector<double> F, dY, abs_vector, rel_vector;
        //
        double Dz_z_last;
        double zstart_solver;
        double tolSol;
        int order;
        int count;
        int Jac_is_set;
        // counting number of significant up and down-steps
        int n_up, n_down;
        double direction;
        //
        int nPDE; // number of pde points (including boundary condition)
        int nODE; // number of odes 
        int nOAE; // number of ades
        int index_ODE, index_OAE;
        // to pass on parameters for the setup
        void *p;
        int max_it; // for iterative solution of full matrix problem
        
        void reset_Solver_Info()
        {
            Snewptr=NULL; // this indicates a fresh start is happening
            return;
        }
        
        // default values
        ODE_Solver_data()
        {
            reset_Solver_Info();
            check_jacobian=0; // to check consistency of Jacobian
        }
    };
    
    //==============================================================================================
    void allocate_memory(ODE_solver_Solution &Sz, ODE_solver_accuracies &tols, int neq); 
    
    void clear_memory(ODE_solver_Solution &Sz, ODE_solver_accuracies &tols);
    
    void clear_SolverData(ODE_Solver_data &SD);
    
    void clear_all_memory(ODE_solver_Solution &Sz, 
                          ODE_solver_accuracies &tols, 
                          ODE_Solver_data &SD);
    
    void set_verbosity(int verb);
    
    //==============================================================================================
    // ODE_Solver_set_up_solution_and_memory::
    //
    // This routine has to be called before the solver is called. In particular the memory is set 
    // and the function pointer for the ODE evaluation is given. 
    //
    // ODE_solver_Solution Sz.z should be set to the initial redshift;
    // ODE_solver_Solution rtol[] & atol[] should contain the relative error request;
    // ODE_solver_Solution Sz.y[] should contain the initial solution;
    // ODE_solver_Solution Sz.dy[] will be computed using fn_ptr;
    //
    // ODE_solver_accuracies tols.rel[] should contain the relative error request;
    // ODE_solver_accuracies tols.abs[] should contain the relative error request;
    //
    // fn_ptr declares the ODE system according to y'= f(z, y);
    //
    //==============================================================================================
    void ODE_Solver_set_up_solution_and_memory(ODE_solver_Solution &Sz, 
                                               ODE_solver_accuracies &tols, 
                                               ODE_Solver_data &ODE_Solver_info, 
                                               void (*fcn_ptr)(int *neq, double *z, 
                                                               double *y, double *f, 
                                                               int col), 
                                               bool num_jac=1);
    
    //==============================================================================================
    // to reset error requirements
    //==============================================================================================
    void ODE_Solver_set_errors(ODE_solver_accuracies &tols, ODE_Solver_data &ODE_Solver_info);
    
    //==============================================================================================
    // This routine has can only be called after the setup was done 
    // (ODE_Solver_set_up_solution_and_memory)
    //
    // After the cal ODE_solver_Solution Sz will contain the solution at time zend; initially Sz 
    // should contain the solution at zs. Previous solutions will also be stored by the routine.
    //
    //==============================================================================================
    int ODE_Solver_Solve_history(double zs, double zend,
                                 double Dz_in, double Dz_max, 
                                 ODE_solver_Solution &Sz, 
                                 ODE_Solver_data &ODE_Solver_info);
    
    //==============================================================================================
    // This routine has can only be called after the setup was done 
    // (ODE_Solver_set_up_solution_and_memory)
    //
    // After the cal ODE_solver_Solution Sz will contain the solution at time zend; initially Sz 
    // should contain the solution at zs. Previous solutions will also be stored by the routine.
    //
    // This routine is tailored for the problem of CMB anisotropies (Dodelson formulation)!!!
    //
    //==============================================================================================
    void ODE_Solver_set_up_solution_and_memory_Anisotropies(ODE_solver_Solution &Sz, 
                                                            ODE_solver_accuracies &tols, 
                                                            ODE_Solver_data &ODE_Solver_info, 
                                                            void (*fcn_ptr)(int *neq, double *z, 
                                                                            double *y, double *f, 
                                                                            int col, void *p), 
                                                            bool num_jac=1);

    int ODE_Solver_Solve_history_Anisotropies(double zs, double zend,
                                              double Dz_in, double Dz_max, 
                                              ODE_solver_Solution &Sz, 
                                              ODE_Solver_data &ODE_Solver_info, int n_Nu);
    
    //==============================================================================================
    // This routine has can only be called after the setup was done 
    // (ODE_Solver_set_up_solution_and_memory)
    //
    // After the cal ODE_solver_Solution Sz will contain the solution at time zend; initially Sz 
    // should contain the solution at zs. Previous solutions will also be stored by the routine.
    //
    // Furthermore, this routine allows to treat PDE + ODE + OAE systems. The PDE is assumed to come 
    // first while the next nODE elements are the ODEs followed by OAE ordinary equations. The 
    // boundary conditions are assumed to be given as algebraic equations in the setup routines.  
    // This is the assumes structure: bc, 1, 2, ..., nPDE-3, nPDE-2, bc, nODE, nOAE. 
    //
    //==============================================================================================
    // Oct 15th, 2013: added adaptive step-size control; Dz_in < Dz < Dz_max will be ensured;
    // to use this option make sure that NOT all relative errors are == 0
    // if this option is not required just set Dz_in==Dz_max (error control will have no effect)
    //==============================================================================================
    int ODE_Solver_Solve_history_PDE_ODE_OAE(double zs, double zend,
                                             double Dz_in, double Dz_max,
                                             ODE_solver_Solution &Sz, 
                                             ODE_Solver_data &ODE_Solver_info,
                                             int nODE, int nOAE, int setmaxorder=5);
    
    //----------------------------------------------------------------------------------------------
    // version with overshooting allow until z == zcrit (if zcrit==zend --> no overshooting)
    //----------------------------------------------------------------------------------------------
    int ODE_Solver_Solve_history_PDE_ODE_OAE(double zs, double zend, double zcrit,
                                             double Dz_in, double Dz_max,
                                             ODE_solver_Solution &Sz,
                                             ODE_Solver_data &ODE_Solver_info,
                                             int nODE, int nOAE, int setmaxorder=5);

    //==============================================================================================
    // to obtain interpolated solution between zs and ze. The solver should have been run before
    // and the solution at ze should be available already.
    //==============================================================================================
    int ODE_Solver_Interpolate_Solution(double z,
                                        ODE_Solver_data &ODE_Solver_info,
                                        ODE_solver_Solution &Sz);
}

#endif
//==================================================================================================
//==================================================================================================
