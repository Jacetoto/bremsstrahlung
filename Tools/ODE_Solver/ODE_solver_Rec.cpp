//==================================================================================================
// ODE_solver routines 
//
// Purpose: solve a system of stiff ODEs with moderate dimension (n<~100). 
// 
// Basic Aspects: The solver is based on a 6th order Gears method (implicit && stiffly-stable) with 
// variable time-step. The linear algebra system is solved iteratively using a biconjugate gradient 
// method. A guess for the next solution is extrapolated from the previous solutions; 
//
// Author: Jens Chluba with contributions from Geoff Vasil at CITA
//
// First implementation: 12/01/2010
// Last modification   : 31/03/2019
//==================================================================================================
// 31/03/2019: added Jacobian matrix setup check, which is useful for debugging [JC]
// 07/02/2019: tidied up solver structures using pointers [JC]
// 26/01/2019: added some step-size change settings + cleaned up the old routine [JC]
// 16/04/2018: added simple interpolation routine using the stored solutions
// 15/10/2013: added adaptive stepsize control to PDE+ODE+ADE routine
//
// 03/01/2012: modified PDE + ODE solver to allow additional OAEs.
//             
// 28/06/2011: added option that allows to solve PDE+ODE systems, where the boundary conditions
//             are communicated as algebraic equations;
//
// 15/06/2011: set max order to 5; This was making the code a bit faster; 
//             tidied the code a bit;
//==================================================================================================

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>

#include "ODE_solver_Rec.h"
#include <gsl/gsl_linalg.h>
#include "routines.h"

using namespace std;

int verbosity_ODE_solver=0;

#ifdef OPENMP_ACTIVATED
//#define OPENMP_ACTIVATED_ODE
#endif

//=======================================================================================
namespace ODE_solver_Rec
{
    void set_verbosity(int verb){ verbosity_ODE_solver=verb; return; }

    //===================================================================================
    void wait_f_r_loc(string mess)
    {
        char c;
        if(mess!="") cout << " " << mess << endl;
        cout << " To go on press return! " << endl;
        cin.get(c);
        return;
    }
    
    void wait_f_r_loc()
    {
        wait_f_r_loc("");
        return;
    }
    
    bool isnan_loc(double a)
    {
#if defined(__APPLE__) 
        return !(a==a);
#else 
        return isnan(a);
#endif
    }
    //===================================================================================

    
    //===================================================================================
    //
    // Linear-Algebra part
    //
    //===================================================================================
    
    //===================================================================================
    // All these routines are for simple linear algebra operations assuming that the
    // dimension of the problem is small (i.e. comparable to a few like in RECFAST)
    //===================================================================================
    
    //===================================================================================
    // scalar product c=a*b
    //===================================================================================
    double dot(const vector<double> &a, const vector<double> &b)
    {
        double scalar=0.0;
        for(unsigned int i=0; i<a.size(); i++) scalar+=a[i]*b[i];
        return scalar;
    }
    
    //===================================================================================
    // norm of vector a
    //===================================================================================
    double norm(const vector<double> &a){ return sqrt(dot(a, a)); }
    
    //===================================================================================
    // compute c=A*b (i.e. matrix times vector)
    //===================================================================================
    void A_times_b_is_c(const ODE_solver_matrix &Jac, 
                        const vector<double> &b, 
                        vector<double> &c)
    {
        //------------------------------------------------------
        // here it is assumed that A is symmetric with dimension 
        // equal to b & c; c is overwritten
        //------------------------------------------------------
        for(unsigned int j=0; j<c.size(); j++) c[j]=0.0;

        for(unsigned int i=0; i<Jac.A.size(); i++) 
            c[Jac.row[i]]+= Jac.A[i] * b[Jac.col[i]];
        
        return;
    }
    
    //===================================================================================
    // get inverse diagonal elements of matrix A
    //===================================================================================
    void Get_inverse_diags(int dim, const ODE_solver_matrix &Jac, vector<double> &d)
    {
        //-----------------------------------------------------------
        // here it is assumed that A is symmetric with dimension dim
        //-----------------------------------------------------------
        for(int i=0; i<dim; i++)
        {
            d[i]=Jac.A[Jac.diags[i]];
            
            if(d[i]==0)
            { 
                cerr << " error in preconditioner for element: " << i << endl; 
                exit(0); 
            }
            else d[i]=1.0/d[i];
        }
        
        return;
    }
    
    //===================================================================================
    // access to elements; time-consuming and just for debugging;
    //===================================================================================
    void show_col_entries(int cl, ODE_solver_matrix &Jac, vector<double > &b)
    {
        //return;
        cout.precision(16);
        cout << endl;
        for(unsigned int i=0; i<Jac.A.size(); i++) 
        {
            if(cl==Jac.col[i])
            { 
                cout << Jac.col[i] 
                << " " << Jac.row[i] 
                << " || " << Jac.A[i] 
                << " b= " << b[Jac.col[i]]<< endl;
            }
        }
        
        wait_f_r();
        return;
    }
    
    long Get_element_index(int c, int r, ODE_solver_matrix &Jac)
    {
        for(unsigned int i=0; i<Jac.A.size(); i++)
        {
            if(c==Jac.col[i] && r==Jac.row[i]) return i;
        }
        
        return -1;
    }
    
    double Get_element(int c, int r, ODE_solver_matrix &Jac)
    {
        long i=Get_element_index(c, r, Jac);
        return (i==-1 ? 0.0 : Jac.A[i]);
    }
    
    void show_whole_matrix(ODE_solver_matrix &Jac, vector<double > &b)
    {
        //return;
        cout.precision(3);
        cout << endl;
        for(int r=0; r<(int)Jac.diags.size(); r++)
        {
            cout << r << " : ";
            for(int c=0; c<(int)Jac.diags.size(); c++) 
            {
                cout.width(4);
                cout << left << scientific << Get_element(c, r, Jac) << "  ";
            }
            
            cout << "  ||  " << b[r] << endl << endl;
        }
        
        wait_f_r();
        return;
    }
    
    //===================================================================================
    // Iterative biconjugate gradiant routine -- BiCGSTAB
    //
    // BiCGSTAB solves the unsymmetric linear system Ax = b 
    // using the Preconditioned BiConjugate Gradient Stabilized method
    //
    // BiCGSTAB follows the algorithm described on p. 27 of the 
    // SIAM Templates book.
    //
    // The return value indicates convergence within max_iter (input)
    // iterations (0), or no convergence within max_iter iterations (1).
    //
    // Upon successful return, output arguments have the following values:
    //  
    //        x  --  approximate solution to Ax = b
    // max_iter  --  the number of iterations performed before the
    //               tolerance was reached
    //      tol  --  the residual after the final iteration
    //  
    // This routine was adapted from the IML++ http://math.nist.gov/iml++/
    //===================================================================================
    int BiCGSTAB_JC(const ODE_solver_matrix &Jac, const vector<double> &b,
                    vector<double> &x, int &max_iter, double &tol)
    {
        int neq=b.size();
        double resid;
        double rho_1=1.0, rho_2=1.0, alpha=1.0, beta=1.0, omega=1.0;
        //
        vector<double> p(neq), phat(neq), s(neq), shat(neq);
        vector<double> t(neq), v(neq), r(neq), rtilde(neq);
        vector<double> invdiag(neq);
        
        //------------------------------------------------------
        // this is to precondition the Matrix (i.e. A=M*Atilde)
        // here simply the inverse diagonal elements are used
        //------------------------------------------------------
        Get_inverse_diags(neq, Jac, invdiag);
        
        double normb = norm(b);
        //------------------------------------------------------
        // r = b - A*x
        //------------------------------------------------------
        A_times_b_is_c(Jac, x, r);
        for(int i=0; i<neq; i++) r[i]=b[i]-r[i]; 
        //
        rtilde = r;
        
        if (normb == 0.0) normb = 1;
        
        if ((resid = norm(r) / normb) <= tol) 
        {
            tol = resid;
            max_iter = 0;
            return 0;
        }
        
        for (int k = 1; k <= max_iter; k++) 
        {
            rho_1 = dot(rtilde, r);
            if (rho_1 == 0) 
            {
                tol = norm(r) / normb;
                return 2;
            }
            
            if (k == 1) p = r;
            else 
            {
                beta = (rho_1/rho_2) * (alpha/omega);
                for(int i=0; i<neq; i++) p[i] = r[i] + beta * (p[i] - omega * v[i]);
            }
            //------------------------------------------------------
            // preconditioning phat
            //------------------------------------------------------
            for(int i=0; i<neq; i++) phat[i]=invdiag[i]*p[i];
            
            //------------------------------------------------------
            // v = A * phat;
            //------------------------------------------------------
            A_times_b_is_c(Jac, phat, v);
            
            alpha = rho_1 / dot(rtilde, v);
            for(int i=0; i<neq; i++) s[i] = r[i] - alpha * v[i];
            
            if ((resid = norm(s)/normb) < tol) 
            {
                for(int i=0; i<neq; i++) x[i] += alpha * phat[i];
                tol = resid;
                return 0;
            }
            //------------------------------------------------------
            // preconditioning shat
            //------------------------------------------------------
            for(int i=0; i<neq; i++) shat[i]=invdiag[i]*s[i];
            
            //------------------------------------------------------
            // t = A * shat;
            //------------------------------------------------------
            A_times_b_is_c(Jac, shat, t);
            
            omega = dot(t,s) / dot(t,t);
            //
            for(int i=0; i<neq; i++) x[i] += alpha * phat[i] + omega * shat[i];
            for(int i=0; i<neq; i++) r[i] = s[i] - omega * t[i];
            
            rho_2 = rho_1;
            if ((resid = norm(r) / normb) < tol) 
            {
                tol = resid;
                max_iter = k;
                return 0;
            }
            
            if (omega == 0) 
            {
                tol = norm(r) / normb;
                return 3;
            }
        }
        
        tol = resid;
        return 1;
    }

    //===================================================================================
    // improved version with better error check
    //===================================================================================
    int BiCGSTAB_JC_II(const ODE_solver_matrix &Jac, const vector<double> &b,
                       vector<double> &x, int &max_iter, double &tol)
    {
        int neq=b.size();
        double resid;
        double rho_1=1.0, rho_2=1.0, alpha=1.0, beta=1.0, omega=1.0;
        //
        vector<double> p(neq), phat(neq), s(neq), shat(neq);
        vector<double> t(neq), v(neq), r(neq), rtilde(neq);
        vector<double> invdiag(neq), xold;
        
        //------------------------------------------------------
        // this is to precondition the Matrix (i.e. A=M*Atilde)
        // here simply the inverse diagonal elements are used
        //------------------------------------------------------
        Get_inverse_diags(neq, Jac, invdiag);
        
        double normb = norm(b);

        //for(unsigned int i=0; i<x.size(); i++) cout << b[i] << " " << x[i] << " norm " << normb << endl;
        //wait_f_r();

        // trivial equation A * x = 0
        if(normb==0.0){ for(int i=0; i<neq; i++) x[i]=0.0; max_iter=0; return 5; }
        
        //------------------------------------------------------
        // r = b - A*x
        //------------------------------------------------------
        A_times_b_is_c(Jac, x, r);
        for(int i=0; i<neq; i++) r[i]=b[i]-r[i];
        rtilde = r;
        xold = x;
        
        if ((resid = norm(r) / normb) <= tol)
        {
            tol = resid;
            max_iter = 0;
            return 0;
        }
        
        for (int k = 1; k <= max_iter; k++)
        {
            rho_1 = dot(rtilde, r);
            if (rho_1 == 0) { tol = norm(r) / normb; return 2; }
            
            if (k == 1) p = r;
            else
            {
                beta = (rho_1/rho_2) * (alpha/omega);
                for(int i=0; i<neq; i++) p[i] = r[i] + beta * (p[i] - omega * v[i]);
            }
            //------------------------------------------------------
            // preconditioning phat
            //------------------------------------------------------
            for(int i=0; i<neq; i++) phat[i]=invdiag[i]*p[i];
            
            //------------------------------------------------------
            // v = A * phat;
            //------------------------------------------------------
            A_times_b_is_c(Jac, phat, v);
            
            alpha = rho_1 / dot(rtilde, v);
            for(int i=0; i<neq; i++) s[i] = r[i] - alpha * v[i];
            
            //------------------------------------------------------
            // preconditioning shat
            //------------------------------------------------------
            for(int i=0; i<neq; i++) shat[i]=invdiag[i]*s[i];
            
            //------------------------------------------------------
            // t = A * shat;
            //------------------------------------------------------
            A_times_b_is_c(Jac, shat, t);
            
            omega = dot(t,s) / dot(t,t);
            //
            for(int i=0; i<neq; i++) x[i] += alpha * phat[i] + omega * shat[i];
            for(int i=0; i<neq; i++) r[i] = s[i] - omega * t[i];
            
            rho_2 = rho_1;
            
            //------------------------------------------------------
            // check result
            //------------------------------------------------------
            bool iterate_again=0;
            resid=0.0;
            for(int i=0; i<neq; i++)
            {
                double dx=abs((x[i]-xold[i])/(1.0e-300+x[i]));
                resid+=dx*dx;
                if(dx>tol){ iterate_again=1; break; }
            }
            resid=sqrt(resid)/neq;
            
            //------------------------------------------------------
            // return result if converged
            //------------------------------------------------------
            if(!iterate_again)
            {
                tol = resid;
                max_iter = k;
                return 0;
            }
            
            if (omega == 0) { tol = norm(r) / normb; return 3; }
            
            xold = x;
        }
        
        tol = resid;
        return 1;
    }

    //===================================================================================
    // Solving the Matrix Equation A*x == b for x using GSL
    //===================================================================================
    int ODE_Solver_Solve_LA_system_GSL(ODE_solver_matrix &M, 
                                       vector<double > &bi, 
                                       vector<double > &x, 
                                       double tol, 
                                       int verbose=0)
    {
        //cout << " Entering GSL " << endl;
        
        int npxi=bi.size();
        
        vector<double> a_data(npxi*npxi, 0.0);
        
        for(int r=0; r<(int)M.col.size(); r++)
        {
            int ind=M.col[r]+npxi*M.row[r];
            a_data[ind]=M.A[r];
        }
        
        gsl_matrix_view m=gsl_matrix_view_array (&a_data[0], npxi, npxi);
        gsl_vector_view b=gsl_vector_view_array (&bi[0], npxi);
        gsl_vector *x_GSL = gsl_vector_alloc (npxi);
        
        int s;
        gsl_permutation * p = gsl_permutation_alloc (npxi);
        //cout << " LU decomposition " << endl;
        gsl_linalg_LU_decomp (&m.matrix, p, &s);
        //cout << " LU solve " << endl;
        gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x_GSL);
        
        for(int k=0; k<npxi; k++) x[k]=x_GSL->data[k];
        
        gsl_permutation_free (p);
        gsl_vector_free (x_GSL);    
        a_data.clear();
        
        return 0;
    }

    //===================================================================================
    // Solving the Matrix Equation A*x == b for x
    //===================================================================================
    int ODE_Solver_Solve_LA_system(ODE_solver_matrix &Jac, 
                                   vector<double > &b, 
                                   vector<double > &x, 
                                   double tol, 
                                   int verbose=0)
    {
        int ifail=0, maxit = 50000;                       // Maximum iterations
        
        //for(unsigned int i=0; i<x.size(); i++) x[i]=0.0; // reset x-vector
        
        //ifail = BiCGSTAB_JC(Jac, b, x, maxit, tol);      // Solve linear system
        ifail = BiCGSTAB_JC_II(Jac, b, x, maxit, tol);      // Solve linear system

        if(verbose>0 && ifail!=0)
        {
            cout << endl << endl;
            cout << " BiCG flag = " << ifail << endl;
            cout << " iterations performed: " << maxit << endl;
            cout << " tolerance achieved  : " << tol << endl;
            cout << " Something bad happened to the ODE system. Restarting solver. " 
                 << endl;
            
            if(isnan_loc(tol)) ifail=10;
        }
        
        cout << " average tolerance achieved  : " << tol << " with " << maxit << " iterations" << endl;
        
        return ifail;
    }    
        
    //===================================================================================
    //
    // Jacobian and ODE function part
    //
    //===================================================================================
    
    //===================================================================================
    // setup for indices of equations 
    //===================================================================================
    void set_equation_indices(int nPDE, int nODE, int nOAE, 
                              ODE_Solver_data &SD)
    {
        SD.nPDE=nPDE;
        SD.nODE=nODE;
        SD.nOAE=nOAE;
        
        SD.index_ODE=nPDE;
        SD.index_OAE=SD.index_ODE+nODE;
        
        return;
    }
    
    //===================================================================================
    // right hand side of the differential equation system dX/dz = g(z, X)
    //===================================================================================
    void ODE_solver_f(int neq, double z, double *y, double *f, 
                      const ODE_Solver_data &SD)
    {
        //==================================================================
        // evaluate the rhs of system  dy/dz == g(t,y)
        //==================================================================
        SD.fcn_col_ptr(&neq, &z, &y[0], &f[0], -1); 
        return;
    }
    
    void ODE_solver_f_col(int neq, double z, double *y, double *f, 
                          int col, const ODE_Solver_data &SD)
    {
        //==================================================================
        // evaluate the rhs of system  dy/dz == g(t,y)
        //==================================================================
        //SD.fcn_col_ptr(&neq, &z, &y[0], &f[0], col);
        SD.fcn_col_ptr(&neq, &z, &y[0], &f[0], -1);
        return;
    }
    
    //===================================================================================
    //
    // to get numerical Jacobian of system
    //
    //===================================================================================
    inline double ODE_solver_df_dx_2point(const double &fp1, const double &fm1, 
                                          const double &h, double eps)
    { 
        if(fm1==0.0) return (fp1-fm1)/(2.0*h);        
        double dum=fp1/fm1-1.0;
        return fabs(dum)<=eps ? 0.0 : dum*fm1/(2.0*h);
    }
    
    //===================================================================================
    void ODE_solver_jac_2p(int neq, int col, double z, double Dz, 
                           double *y, double *r, ODE_Solver_data &SD)
    { 
        //===============================================================================
        // r[i] is reset here
        // y[i] should contain the solution at z
        //===============================================================================
        double y0=y[col], Dyj=y[col]*1.0e-8, eps=1.0e-12;
        if(y0==0.0) Dyj=1.0e-8;
        
        vector<double> fp1(neq), f0(neq), fm1(neq);

        //===============================================================================
        // derivatives with respect to Xj.
        // A two-point formula is used. It has accuracy O(h^2)
        //===============================================================================
        // get f(yj+Dyj)
        y[col]=y0+Dyj;
        ODE_solver_f_col(neq, z, &y[0], &fp1[0], col, SD);
        // get f(yj-Dyj)
        y[col]=y0-Dyj;
        ODE_solver_f_col(neq, z, &y[0], &fm1[0], col, SD);
        // restore y again
        y[col]=y0;
        ODE_solver_f_col(neq, z, &y[0], &f0[0], col, SD); // required for consistency

        //===============================================================================
        // define numerical derivative
        //===============================================================================
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
        for(int k=0; k<neq; k++) r[k]=ODE_solver_df_dx_2point(fp1[k], fm1[k], Dyj, eps);
        
        //===============================================================================
        // define jacobian
        //===============================================================================
        // PDE + ODE part; OAE part nothing needs to be done
        for(int k=1; k<SD.index_ODE-1; k++) r[k]=-Dz*r[k];
        for(int k=SD.index_ODE; k<SD.index_OAE; k++) r[k]=-Dz*r[k];

        //===========================================================================
        // add unity to the i==j element for inner points of PDE and all ODEs
        //===========================================================================
        if( (col>0 && col<SD.index_ODE-1) || 
            (col>=SD.index_ODE && col<SD.index_OAE) ) r[col]+=1.0;   
        
        return; 
    }
    
    //===================================================================================
    //
    // to get the Jacobian from user
    //
    //===================================================================================
    void ODE_solver_jac_user(int neq, int col, double z, double Dz, 
                             double *y, double *r, ODE_Solver_data &SD)
    { 
        SD.fcn_col_ptr(&neq, &z, y, r, col);
        
        //===============================================================================
        // define jacobian
        //===============================================================================
        // PDE + ODE part; OAE part nothing needs to be done
        for(int k=1; k<SD.index_ODE-1; k++) r[k]=-Dz*r[k];
        for(int k=SD.index_ODE; k<SD.index_OAE; k++) r[k]=-Dz*r[k];
        
        //===========================================================================
        // add unity to the i==j element for inner points of PDE and all ODEs
        //===========================================================================
        if( (col>0 && col<SD.index_ODE-1) || 
           (col>=SD.index_ODE && col<SD.index_OAE) ) r[col]+=1.0;   
        
        return; 
    }
        
    //===================================================================================
    //
    // to get numerical Jacobian of the system with parameters
    //
    //===================================================================================
    void ODE_solver_f_param(int neq, double z, double *y, double *f, 
                            ODE_Solver_data &SD)
    {
        //==================================================================
        // evaluate the rhs of system  dy/dz == g(t,y)
        //==================================================================
        SD.fcn_col_para_ptr(&neq, &z, &y[0], &f[0], -1, SD.p); 
        return;
    }
    
    void ODE_solver_f_param_col(int neq, double z, double *y, double *f, int col, 
                                ODE_Solver_data &SD)
    {
        //==================================================================
        // evaluate the rhs of system  dy/dz == g(t,y)
        //==================================================================
        SD.fcn_col_para_ptr(&neq, &z, &y[0], &f[0], col, SD.p); 

        return;
    }
    
    void ODE_solver_jac_2p_param(int neq, int col, double z, double Dz, 
                                 double *y, double *r, 
                                 ODE_Solver_data &SD)
    { 
        //===============================================================================
        // r[i] is reset here
        // y[i] should contain the solution at z
        //===============================================================================
        double y0=y[col], Dyj=y[col]*1.0e-8, eps=1.0e-12;
        if(y0==0.0) Dyj=1.0e-8;
        
        vector<double> fp1(neq), fm1(neq);
        
        //===============================================================================
        // derivatives with respect to Xj.
        // A two-point formula is used. It has accuracy O(h^2)
        //===============================================================================
        // get f(yj+Dyj)
        y[col]=y0+Dyj;
        ODE_solver_f_param_col(neq, z, &y[0], &fp1[0], col, SD);
        // get f(yj-Dyj)
        y[col]=y0-Dyj;
        ODE_solver_f_param_col(neq, z, &y[0], &fm1[0], col, SD);
        // restore y again
        y[col]=y0;
        
        //===============================================================================
        // define numerical derivative
        //===============================================================================
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
        for(int k=0; k<neq; k++)
        {
            r[k]=ODE_solver_df_dx_2point(fp1[k], fm1[k], Dyj, eps);
            r[k]=-Dz*r[k];
        }
        
        //===========================================================================
        // add unity to the i==j element
        //===========================================================================
        r[col]+=1.0;   
        
        return; 
    }
    
    //===================================================================================
    void ODE_solver_jac_user_param(int neq, int col, double z, double Dz, 
                                   double *y, double *r, 
                                   ODE_Solver_data &SD)
    { 
        SD.fcn_col_para_ptr(&neq, &z, y, r, col, SD.p);
        
        //===============================================================================
        // define derivative
        //===============================================================================
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
        for(int k=0; k<neq; k++) r[k]=-Dz*r[k];
        
        //===========================================================================
        // add unity to the i==j element
        //===========================================================================
        r[col]+=1.0;   
        
        return; 
    }

    //===================================================================================
    void ODE_solver_compute_Jacobian_Matrix(double z, double Dz, int neq, double *y, 
                                            ODE_solver_matrix &Jac, 
                                            ODE_Solver_data &SD, int mess=0)
    {
        //===============================================================================
        // z is the current redshift
        // y[] should contain the solution for which the jacobian is needed
        // Jac[] should have dimension neq*neq
        //===============================================================================
        if(mess==1) cout << " entering full Jacobinan update. " << endl;
        
        Jac.A.clear();
        Jac.row.clear();
        Jac.col.clear();
        Jac.diags.clear();
        
        //===============================================================================
        // this vector will be used to store the jacobian after calls of f_i(X)
        //===============================================================================
        vector<double> Jij(neq);
        
        //===============================================================================
        // fill Matrix with values
        //===============================================================================
        void (*Jac_func)(int, int, double, double, double *, double *, ODE_Solver_data &);
        
        if(SD.num_jac) Jac_func=ODE_solver_jac_2p;
        else Jac_func=ODE_solver_jac_user;
        
        for(int J=0; J<neq; J++)
        {
            Jac_func(neq, J, z, Dz, y, &Jij[0], SD);
            
            for(int row=0; row<neq; row++)
                if(Jij[row]!=0.0)
                {
                    Jac.A.push_back(Jij[row]);
                    Jac.col.push_back(J);
                    Jac.row.push_back(row);
                
                    if(J==row) Jac.diags.push_back((int)(Jac.A.size()-1));
                }
        }
        
        if(mess==1) cout << " Number of non-zero elements = " << Jac.A.size() 
                         << ". Full matrix has " << neq*neq << " elements " << endl;
        
        if((int)Jac.diags.size()!=neq) 
            cout << " Hmmm. That should not happen... " << endl;
        
        return;
    }
        
    //===================================================================================
    //
    // this function has to be called to set/reset errors
    //
    //===================================================================================
    void ODE_Solver_set_errors(ODE_solver_accuracies &tols, 
                               ODE_Solver_data &ODE_Solver_info)
    {
        int neq=tols.rel.size();
        
        ODE_Solver_info.abs_vector.resize(neq);
        ODE_Solver_info.rel_vector.resize(neq);
        
        //===============================================================================
        // Copy absolute & relative accuracies
        //===============================================================================
        ODE_Solver_info.abs_vector=tols.abs;
        ODE_Solver_info.rel_vector=tols.rel;
        
        // find minimal relative tolerance
        double minrtol=1.0;
        for(int k=0; k<neq; k++)
        {
            if(minrtol>tols.rel[k] && tols.rel[k]>0.0) minrtol=tols.rel[k];
        }
        
        if(verbosity_ODE_solver>=1) 
            cout << " setting tolerance for Jacobian iteration to " << minrtol << endl;
        ODE_Solver_info.tolSol=minrtol;
        
        return;
    }
    
    //===================================================================================
    //
    // memory setup for ODE_solver_Solution and ODE_solver_accuracies
    //
    //===================================================================================
    void allocate_memory(ODE_solver_Solution &Sz, int neq)
    {
        Sz.y.resize(neq);
        Sz.dy.resize(neq);
        return;
    }
        
    void allocate_memory(ODE_solver_accuracies &tols, int neq)
    {
        tols.rel.resize(neq);
        tols.abs.resize(neq);
        return;
    }

    void allocate_memory(ODE_solver_Solution &Sz, ODE_solver_accuracies &tols, int neq)
    {
        allocate_memory(Sz, neq);
        allocate_memory(tols, neq);
        return;
    }
    
    //===================================================================================
    void clear_memory(ODE_solver_Solution &Sz)
    {
        Sz.y.clear();
        Sz.dy.clear();
        return;
    }

    void clear_memory(ODE_solver_accuracies &tols)
    {
        tols.rel.clear();
        tols.abs.clear();
        return;
    }

    void clear_memory(ODE_solver_Solution &Sz, ODE_solver_accuracies &tols)
    {
        clear_memory(Sz);
        clear_memory(tols);
        return;
    }
    
    //===================================================================================
    void clear_SolverData(ODE_Solver_data &SD)
    {
        clear_memory(SD.Stemp);
        clear_memory(*SD.Snewptr);
        clear_memory(*SD.Sguessptr);
        for(int k=0; k<6; k++) clear_memory(*SD.Snptr[k]);
        
        SD.F.clear();
        SD.dY.clear();
        SD.abs_vector.clear();
        SD.rel_vector.clear();
        
        SD.Jacobian_ptr->A.clear();
        SD.Jacobian_ptr->row.clear();
        SD.Jacobian_ptr->col.clear();
        SD.Jacobian_ptr->diags.clear();
        
        delete SD.Snewptr;
        delete SD.Sguessptr;
        for(int k=0; k<6; k++) delete SD.Snptr[k];
        delete SD.Jacobian_ptr;

        return;
    }

    //===================================================================================
    void clear_all_memory(ODE_solver_Solution &Sz, 
                          ODE_solver_accuracies &tols, 
                          ODE_Solver_data &SD)
    {
        clear_memory(Sz, tols);
        clear_SolverData(SD);
        return;
    }

    //===================================================================================
    //
    // this function has to be called to set up the memory and the initial solution
    //
    //===================================================================================
    void ODE_Solver_setup(ODE_solver_Solution &Sz, 
                          ODE_solver_accuracies &tols, 
                          ODE_Solver_data &ODE_Solver_info)
    {
        int neq=Sz.y.size();
        
        if(verbosity_ODE_solver>=1)
        {
            if(ODE_Solver_info.Snewptr==NULL) cout << "\n Setting up memory " << endl;
            else  cout << "\n Resetting memory " << endl;
            cout << " Total number of equations = " << neq << endl;
        }
        
        //-------------------------------------------------------------
        // create vectors & allocate memory
        //------------------------------------------------------------- 
        ODE_Solver_info.F.resize(neq);
        ODE_Solver_info.dY.resize(neq);
        ODE_Solver_info.Sptr=NULL;
        
        if(ODE_Solver_info.Snewptr==NULL)
        {
            //wait_f_r_loc("Memory");
            ODE_Solver_info.Snewptr=new ODE_solver_Solution;
            ODE_Solver_info.Sguessptr=new ODE_solver_Solution;
            for(int k=0; k<6; k++) ODE_Solver_info.Snptr[k]=new ODE_solver_Solution;
            ODE_Solver_info.Jacobian_ptr= new ODE_solver_matrix;
        }
        
        ODE_Solver_info.Stemp.y.resize(neq); ODE_Solver_info.Stemp.dy.resize(neq); 
        //
        ODE_Solver_info.Snewptr->y.resize(neq); ODE_Solver_info.Snewptr->dy.resize(neq); 
        ODE_Solver_info.Sguessptr->y.resize(neq); ODE_Solver_info.Sguessptr->dy.resize(neq);
        // 
        for(int k=0; k<6; k++) 
        {
            ODE_Solver_info.Snptr[k]->y.resize(neq); 
            ODE_Solver_info.Snptr[k]->dy.resize(neq); 
        }
        
        //===============================================================================
        // copy solution
        //===============================================================================
        ODE_Solver_info.Snptr[0]->z=Sz.z;
        ODE_Solver_info.Snptr[0]->y=Sz.y;
        ODE_Solver_info.Snptr[0]->dy=Sz.dy;
        
        if(ODE_Solver_info.p==NULL) ODE_solver_f(neq, ODE_Solver_info.Snptr[0]->z, 
                                                 &ODE_Solver_info.Snptr[0]->y[0], 
                                                 &ODE_Solver_info.Snptr[0]->dy[0], 
                                                 ODE_Solver_info);
        
        else ODE_solver_f_param(neq, ODE_Solver_info.Snptr[0]->z, 
                                &ODE_Solver_info.Snptr[0]->y[0], 
                                &ODE_Solver_info.Snptr[0]->dy[0], 
                                ODE_Solver_info);
        
        //===============================================================================
        // other data
        //===============================================================================
        ODE_Solver_info.Dz_z_last=0;    
        ODE_Solver_info.zstart_solver=ODE_Solver_info.Snptr[0]->z; 
        ODE_Solver_info.order=1;    
        ODE_Solver_info.count=1;
        ODE_Solver_info.Jac_is_set=0;
        //  
        ODE_Solver_info.n_up=0;
        ODE_Solver_info.n_down=0;
        ODE_Solver_info.max_it=0;
        
        //===============================================================================
        // Copy absolute & relative accuracies
        //===============================================================================
        ODE_Solver_set_errors(tols, ODE_Solver_info);
        
        return;
    }
    
    //===================================================================================
    void ODE_Solver_set_up_solution_and_memory(ODE_solver_Solution &Sz, 
                                               ODE_solver_accuracies &tols, 
                                               ODE_Solver_data &ODE_Solver_info, 
                                               void (*fcn_ptr)(int *neq, double *z, 
                                                               double *y, double *f, 
                                                               int col), 
                                               bool num_jac)
    {
        ODE_Solver_info.fcn_col_ptr=fcn_ptr;
        ODE_Solver_info.fcn_col_para_ptr=NULL;
        ODE_Solver_info.p=NULL;
        ODE_Solver_info.num_jac=num_jac;
        ODE_Solver_setup(Sz, tols, ODE_Solver_info);
        
        return;
    }
    
    //===================================================================================
    void ODE_Solver_set_up_solution_and_memory_Anisotropies(ODE_solver_Solution &Sz, 
                                                            ODE_solver_accuracies &tols, 
                                                            ODE_Solver_data &ODE_Solver_info, 
                                                            void (*fcn_ptr)(int *neq, double *z, 
                                                                            double *y, double *f, 
                                                                            int col, void *p), 
                                                            bool num_jac)
    {
        ODE_Solver_info.fcn_col_ptr=NULL;
        ODE_Solver_info.fcn_col_para_ptr=fcn_ptr;
        ODE_Solver_info.num_jac=num_jac;
        ODE_Solver_setup(Sz, tols, ODE_Solver_info);
        
        return;
    }
    
    //===================================================================================
    //
    // time-step using Gears-method
    // order can be ==1; 2; 3; 4; 5; 6;
    //
    //===================================================================================
    
    //===================================================================================
    // aux-functions alpha_i coefficients
    //===================================================================================
    double ODE_Solver_Gears_fk1(double rk)
    { return 2.0+rk; } 
    
    double ODE_Solver_Gears_fk2(double r1, double rk)
    {
        double r=ODE_Solver_Gears_fk1(r1)*ODE_Solver_Gears_fk1(rk);
        return r-1.0;
    } 
    
    double ODE_Solver_Gears_fk3(double r1, double r2, double rk)
    {
        double r=ODE_Solver_Gears_fk2(r1, r2)*ODE_Solver_Gears_fk1(rk);
        return r-ODE_Solver_Gears_fk1(r1+r2);
    } 
    
    double ODE_Solver_Gears_fk4(double r1, double r2, double r3, double rk)
    {
        double r=ODE_Solver_Gears_fk3(r1, r2, r3)*ODE_Solver_Gears_fk1(rk);
        r-=ODE_Solver_Gears_fk1(r1+r2)*r3;
        return r-ODE_Solver_Gears_fk2(r1, r2);
    } 
    
    inline double ODE_Solver_Gears_fk5(double r1, double r2, double r3, 
                                       double r4, double rk)
    {
        double r=ODE_Solver_Gears_fk4(r1, r2, r3, r4)*ODE_Solver_Gears_fk1(rk);
        r-=(ODE_Solver_Gears_fk2(r1, r2+r3)+r2*r3)*r4;
        return r-ODE_Solver_Gears_fk3(r1, r2, r3);
    } 
    
    //===================================================================================
    // alpha_i coefficients
    //===================================================================================
    double ODE_Solver_delta0(double r1, double r2, double r3, double r4, double r5, 
                             double a1, double a2, double a3, double a4, double a5)
    { return 1.0 + a1*r1 + a2*r2 + a3*r3 + a4*r4 + a5*r5; }
    
    double ODE_Solver_alp0(double a1, double a2, double a3, double a4, double a5)
    { return 1.0 - a1 - a2 - a3 - a4 - a5; }
    
    double ODE_Solver_alp1(double r1, double r2, double r3, double r4, double r5, 
                           double a2, double a3, double a4, double a5)
    { return -((1.0 + a2*r2*(2.0+r2) + a3*r3*(2.0+r3) + a4*r4*(2.0+r4) 
                    + a5*r5*(2.0+r5))/(r1*(2.0+r1))); }
    
    double ODE_Solver_alp2(double r1, double r2, double r3, double r4, double r5, 
                           double a3, double a4, double a5)
    {   
        double t=(1.0+r1);
        return -( t*t 
                 +a3*r3*(r1-r3)*ODE_Solver_Gears_fk2(r1, r3)
                 +a4*r4*(r1-r4)*ODE_Solver_Gears_fk2(r1, r4)
                 +a5*r5*(r1-r5)*ODE_Solver_Gears_fk2(r1, r5) 
                )
                /( r2*(r1-r2)*ODE_Solver_Gears_fk2(r1, r2) ); 
    }
    
    double ODE_Solver_alp3(double r1, double r2, double r3, double r4, double r5, 
                           double a4, double a5)
    { 
        double t=(1.0+r1)*(1.0+r2);
        return -( t*t
                 +a4*r4*(r1-r4)*(r2-r4)*ODE_Solver_Gears_fk3(r1, r2, r4) 
                 +a5*r5*(r1-r5)*(r2-r5)*ODE_Solver_Gears_fk3(r1, r2, r5) 
                ) 
                /( r3*(r1-r3)*(r2-r3)*ODE_Solver_Gears_fk3(r1, r2, r3) ) ; 
    }
    
    double ODE_Solver_alp4(double r1, double r2, double r3, double r4, double r5, 
                           double a5)
    { 
        double t=(1.0+r1)*(1.0+r2)*(1.0+r3);
        return -( t*t
                 +a5*r5*(r1-r5)*(r2-r5)*(r3-r5)*ODE_Solver_Gears_fk4(r1, r2, r3, r5) 
                )
                /( r4*(r1-r4)*(r2-r4)*(r3-r4)*ODE_Solver_Gears_fk4(r1, r2, r3, r4) ); 
    }
    
    double ODE_Solver_alp5(double r1, double r2, double r3, double r4, double r5)
    { 
        double t=(1.0+r1)*(1.0+r2)*(1.0+r3)*(1.0+r4);
        return -t*t/( r5*(r1-r5)*(r2-r5)*(r3-r5)*(r4-r5)
                      *ODE_Solver_Gears_fk5(r1, r2, r3, r4, r5) ); 
    }

    double ODE_Solver_alpi(int i, const double *r, const double *a)
    {
        if(i==2) return ODE_Solver_alp1(r[0], r[1], r[2], r[3], r[4], a[2], a[3], a[4], a[5]);
        if(i==3) return ODE_Solver_alp2(r[0], r[1], r[2], r[3], r[4], a[3], a[4], a[5]);
        if(i==4) return ODE_Solver_alp3(r[0], r[1], r[2], r[3], r[4], a[4], a[5]);
        if(i==5) return ODE_Solver_alp4(r[0], r[1], r[2], r[3], r[4], a[5]);
        if(i==6) return ODE_Solver_alp5(r[0], r[1], r[2], r[3], r[4]);
        
        return 0;
    }

    //===================================================================================
    // Gear's corrector (implicit)
    //===================================================================================
    double ODE_Solver_compute_ynp1(int order, ODE_solver_Solution &Snp1_new, 
                                   const ODE_solver_Solution &Snp1_guess, 
                                   ODE_solver_Solution *Sn[6])
    {
        if(order<1 || order>6)
        { 
            cerr << " check order for ODE_Solver_compute_ynp1 " << endl; 
            exit(0); 
        }
        
        //===============================================================================
        // the structures yn, ynm1, ynm2 contain the information from the previous time 
        // steps the structure ynp1 contains the current version of ynp1 and dynm1
        //===============================================================================
        double delta=1.0;
        double Dznp1=Snp1_guess.z-Sn[0]->z;
        //
        double a[6]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        double Dzn[5]={0.0, 0.0, 0.0, 0.0, 0.0};
        double rho[5]={0.0, 0.0, 0.0, 0.0, 0.0};
        
        for(int k=2; k<=order; k++){ Dzn[k-2]=Sn[0]->z-Sn[k-1]->z; rho[k-2]=Dzn[k-2]/Dznp1; }
        
        for(int k=order; k>=2; k--) a[k-1]=ODE_Solver_alpi(k, rho, a);
        
        a[0]=ODE_Solver_alp0(a[1], a[2], a[3], a[4], a[5]); 
        
        delta=ODE_Solver_delta0(rho[0], rho[1], rho[2], rho[3], rho[4], 
                                a[1], a[2], a[3], a[4], a[5]);
        
        // add up different orders
        for(int i=0; i<(int)Snp1_new.y.size(); i++) 
        {
            Snp1_new.y[i]=Dznp1*delta*Snp1_guess.dy[i];
            
            double Sa=0.0;
            for(int mo=order-1; mo>=0; mo--) Sa+=a[mo]*Sn[mo]->y[i];
            
            Snp1_new.y[i]+=Sa;
        }

        return delta;   
    }
    
    //===================================================================================
    // beta_i coefficients for extrapolation with avariable step-size
    // These expression where derived by Geoff Vasil (CITA)
    //===================================================================================
    double ODE_Solver_beta0(double b1, double b2, double b3, double b4, double b5)
    { return 1.0 - b1 - b2 - b3 - b4 - b5; }
    
    double ODE_Solver_beta1(double r1, double r2, double r3, double r4, double r5, 
                            double b2, double b3, double b4, double b5)
    { return -(1.0 + b2*r2 + b3*r3 + b4*r4 + b5*r5)/r1; }
    
    double ODE_Solver_beta2(double r1, double r2, double r3, double r4, double r5, 
                            double b3, double b4, double b5)
    { return -( (1.0+r1) + b3*r3*(r1-r3) + b4*r4*(r1-r4) + b5*r5*(r1-r5) )/((r1-r2)*r2); }
    
    double ODE_Solver_beta3(double r1, double r2, double r3, double r4, double r5, 
                            double b4, double b5)
    { return ( (1.0+r1)*(1.0+r2) + b4*r4*(r1-r4)*(r2-r4) 
                                 + b5*r5*(r1-r5)*(r2-r5) )/((r1-r3)*r3*(r3-r2)); }
    
    double ODE_Solver_beta4(double r1, double r2, double r3, double r4, double r5, 
                            double b5)
    { return -( (1.0+r1)*(1.0+r2)*(1.0+r3) + b5*r5*(r1-r5)*(r2-r5)*(r3-r5) )
              /((r3-r4)*r4*(r4-r1)*(r4-r2)); }
    
    double ODE_Solver_beta5(double r1, double r2, double r3, double r4, double r5)
    { return (1.0+r1)*(1.0+r2)*(1.0+r3)*(1.0+r4)/((r2-r5)*r5*(r5-r1)*(r5-r3)*(r5-r4)); }
    
    double ODE_Solver_betai(int i, const double *r, const double *b)
    {
        if(i==2) return ODE_Solver_beta1(r[0], r[1], r[2], r[3], r[4], b[2], b[3], b[4], b[5]);
        if(i==3) return ODE_Solver_beta2(r[0], r[1], r[2], r[3], r[4], b[3], b[4], b[5]);
        if(i==4) return ODE_Solver_beta3(r[0], r[1], r[2], r[3], r[4], b[4], b[5]);
        if(i==5) return ODE_Solver_beta4(r[0], r[1], r[2], r[3], r[4], b[5]);
        if(i==6) return ODE_Solver_beta5(r[0], r[1], r[2], r[3], r[4]);
        
        return 0;
    }    
    
    //===================================================================================
    //
    // extrapolation using old function values
    //
    //===================================================================================
    void ODE_Solver_extrapolate_ynp1(int order, ODE_solver_Solution &Snp1, 
                                     ODE_solver_Solution *Sn[6])
    {
        if(order<1 || order>6)
        { 
            cerr << " check order for ODE_Solver_extrapolate_ynp1 " << endl; 
            exit(0); 
        }
        
        //================================================================================
        // the structures yn, ynm1,... contain the information from the previous time 
        // steps the structure ynp1 contains the current version of ynp1 and dynm1
        //================================================================================
        double Dznp1=Snp1.z-Sn[0]->z;
        //
        double b[6]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        double Dzn[5]={0.0, 0.0, 0.0, 0.0, 0.0};
        double rho[5]={0.0, 0.0, 0.0, 0.0, 0.0};
        
        for(int k=2; k<=order; k++){ Dzn[k-2]=Sn[0]->z-Sn[k-1]->z; rho[k-2]=Dzn[k-2]/Dznp1; }
        
        for(int k=order; k>=2; k--) b[k-1]=ODE_Solver_betai(k, rho, b);
        
        b[0]=ODE_Solver_beta0(b[1], b[2], b[3], b[4], b[5]);
        
        // add up different orders
        for(int i=0; i<(int)Sn[0]->y.size(); i++) 
        {
            double Sa=0.0;
            for(int mo=order-1; mo>=0; mo--) Sa+=b[mo]*Sn[mo]->y[i];
            
            Snp1.y[i]=Sa;
        }    
        
        return; 
    }
    
    //===================================================================================
    //
    // polynomial interpolation using old function values
    //
    //===================================================================================
    void ODE_Solver_interpolate(int order,
                                ODE_solver_Solution *Sn[6],
                                ODE_solver_Solution &Sn_interpol)
    {
        if(order<1 || order>6)
            throw_error("ODE_Solver_interpolate", "check order", 1);

        double Dzn[6]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        double rho[6]={1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

        // interpolation coefficients
        for(int k=0; k<order; k++) Dzn[k]=Sn_interpol.z-Sn[k]->z;

        for(int k=0; k<order; k++)
            for(int j=0; j<order; j++)
                if(k!=j) rho[k]*=Dzn[j]/(Sn[k]->z-Sn[j]->z);

        // add up different orders of the polynomial
        for(int i=0; i<(int)Sn[0]->y.size(); i++)
        {
            double y=0.0, dy=0.0;
            for(int mo=order-1; mo>=0; mo--)
            {
                y +=rho[mo]*Sn[mo]->y [i];
                dy+=rho[mo]*Sn[mo]->dy[i];
            }

            Sn_interpol.y [i]=y;
            Sn_interpol.dy[i]=dy;
        }

        return;
    }

    //===================================================================================
    //
    // Functions for the ODE solver (error checking)
    //
    //===================================================================================
    inline double ODE_Solver_error_check(double z, double rtol, double atol, double y,
                                         double Dy, int mflag, int component,
                                         int &error_type, string mess="")
    {
        Dy=fabs(Dy);
        error_type=0;

        double err_aim=rtol*fabs(y), fac=1.0e+10;
        if(err_aim<=atol)
        { 
            if(mflag>=3) cout << " Time-stepper: absolute error control for ( " << mess
                              << " / component " << component << " ) at z= " 
                              << z << " and y= " << y << " |Dy|= " << Dy 
                              << " Dy_rel= " << err_aim << " Dy_abs= " << atol << endl;
            err_aim=atol;
            error_type=1;
        }
        
        if(Dy!=0.0) fac=err_aim/Dy;
        
        return fac;
    }
    
    //===================================================================================
    //
    // error and step-size estimate
    //
    //===================================================================================
    int ODE_Solver_estimate_error_and_next_possible_step_size(int order, const double zin,
                                                              double z, double zend,
                                                              double &Dz_z, const double Dz_z_max,
                                                              ODE_Solver_data &ODE_Solver_info,
                                                              const ODE_solver_Solution &S,
                                                              ODE_solver_Solution *Sn[6],
                                                              int messflag=1)
    {
        //--------------------------------------------------------
        // even if higher order is used just go with fourth order
        // for error check (added June 15)
        //--------------------------------------------------------        
        int loc_order=(int) min(max(order, 1), 6);
        
        //--------------------------------------------------------
        // S contains best approximation to order 'order'
        // *Stemp will contain best approximation to order 'order-1'
        //--------------------------------------------------------
        int neq=S.y.size();
        ODE_Solver_info.Stemp.z=z;
        ODE_Solver_extrapolate_ynp1(order, ODE_Solver_info.Stemp, Sn);
        
        //--------------------------------------------------------
        // compute difference
        //--------------------------------------------------------
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
        for(int i=0; i<neq; i++) ODE_Solver_info.Stemp.y[i]-=S.y[i];
        
        //--------------------------------------------------------
        // estimate errors
        //--------------------------------------------------------
        double power, fac=1.0e+10, dum;
        int max_err_time=-1, err_type;
        //--------------------------------------------------------
        if(loc_order<=1) power=1.0;
        else power=1.0/loc_order;
        
        for(int l=0; l<neq; l++)
        {
            if(ODE_Solver_info.rel_vector[l]!=0.0) 
            {
                dum=ODE_Solver_error_check(z, ODE_Solver_info.rel_vector[l],
                                           ODE_Solver_info.abs_vector[l], S.y[l],
                                           ODE_Solver_info.Stemp.y[l], messflag,
                                           l, err_type, "?");

                if(fac>dum){ fac=dum; max_err_time=l; }
            }
        }
        
        if(messflag>=2 && max_err_time!=-1)
        {
            cout << " Max error in component " << max_err_time
                 << " (of " << neq << ") hitting "
                 << (err_type==0 ? "relative error " : "absolute error ")
                 << (err_type==0 ?
                     ODE_Solver_info.rel_vector[max_err_time] :
                     ODE_Solver_info.abs_vector[max_err_time])
                 << endl;
        }
        
        //--------------------------------------------------------
        // change step-size; 
        // limit change by different values, depending on success
        //--------------------------------------------------------
        // control of step-size changes. Old quoted setting were
        // numerically unstable. Most crucial was change of
        // 'up_thresh', which controls the minimally required
        // change upwards [added 26.01.19, JC]
        //--------------------------------------------------------
        double max_fac    =5.0; // was =100.0;
        double down_thresh=0.7; // was =0.9;
        double up_thresh  =1.3; // was =1.1;
        //--------------------------------------------------------
        double Dz_z_min=1.0e-10;
        double Dz_z_new=Dz_z*min(max_fac, pow(fac, power));

        Dz_z_new=min(max(Dz_z_new, Dz_z_min), Dz_z_max);

        if(messflag==1)
        {
            cout << " Estimated change: ";
            cout << " order= " << order << " estimate order= " << loc_order 
                 << " current step-size: " << Dz_z 
                 << " -- new suggested step-size: " << Dz_z_new << " -- Dz= " << zin*Dz_z_new 
                 << " z= " << z 
                 << " zend= " << zend << " Dz= " << z-zend << " zin= " << zin << " Dz= " << zin-z;

            if(Dz_z_new==Dz_z) cout << " -- no change " << endl;
            else if(Dz_z_new==Dz_z_max) cout << " -- maximal step-size reached " << endl;
            else if(Dz_z_new>Dz_z) cout << " -- increase by: " << Dz_z_new/Dz_z << endl;
            else cout << " -- decrease by: " << Dz_z_new/Dz_z << endl;
        }
        
        //--------------------------------------------------------
        // to put limits on the changes in Dz (not too small)
        //--------------------------------------------------------
        if(Dz_z*down_thresh>Dz_z_new)
        {
            Dz_z=Dz_z_new;

            ODE_Solver_info.n_down=(int) min(ODE_Solver_info.n_down+1, 5);
            
            //--------------------------------------------------------
            // if there were several subsequent decreases of the 
            // step-size take some more drastic action
            //--------------------------------------------------------
            if(ODE_Solver_info.n_down>=3)
            {
                if(ODE_Solver_info.n_down>=5)
                {
                    Dz_z=Dz_z_min;
                    cout << "\n Reset to minimal step-size " << endl;
                }
                else 
                {
                    Dz_z/=pow(2.0, ODE_Solver_info.n_down-1); 
                    cout << "\n Checking to decreasing the step-size by additional factor of " 
                         << pow(2.0, ODE_Solver_info.n_down-1) << endl;
                }
            }
            
            if(Dz_z*0.5>Dz_z_new) return 1;
        }
        else if(Dz_z*up_thresh<Dz_z_new)
        {
            Dz_z=min(Dz_z_new, Dz_z_max);

            ODE_Solver_info.n_down=0;
            
            return 0;
        }
        
        ODE_Solver_info.n_down=0;
        return 0;
    }
    
    //===================================================================================
    void ODE_solver_compute_Jacobian_Matrix_Anisotropies(double z, double Dz, 
                                                         int neq, double *y, 
                                                         ODE_solver_matrix &Jac, 
                                                         ODE_Solver_data &SD,
                                                         int mess=0)
    {
        //===============================================================================
        // z is the current redshift
        // y[] should contain the solution for which the jacobian is needed
        // Jac[] should have dimension neq*neq
        //===============================================================================
        if(mess==1) cout << " entering full Jacobinan update. " << endl;
        
        if(Jac.A.size()==0)
        {
            Jac.A.resize(neq*neq);
            Jac.row.resize(neq*neq);
            Jac.col.resize(neq*neq);
            Jac.diags.resize(neq);
        }
        
        //===============================================================================
        // this vector will be used to store the jacobian after calls of f_i(X)
        //===============================================================================
        vector<double> Jij(neq);
        
        //===============================================================================
        // fill Matrix with values
        //===============================================================================
        void (*Jac_func)(int, int, double, double, double *, double *, ODE_Solver_data &);
        
        if(SD.num_jac) Jac_func=ODE_solver_jac_2p_param;
        else Jac_func=ODE_solver_jac_user_param;

        for(int J=0; J<neq; J++)
        {
            Jac_func(neq, J, z, Dz, y, &Jij[0], SD);
            
            for(int row=0; row<neq; row++)
            {
                Jac.A[J+row*neq]=Jij[row];
                Jac.col[J+row*neq]=J;
                Jac.row[J+row*neq]=row;
                
                if(J==row) Jac.diags[J]=(int)(Jac.A.size()-1);
            }
        }
        
        if(mess==1) cout << " Number of non-zero elements = " << Jac.A.size() 
            << ". Full matrix has " << neq*neq << " elements " << endl;
        
        if((int)Jac.diags.size()!=neq) 
            cout << " Hmmm. That should not happen... " << endl;
        
        return;
    }

    //===================================================================================
    void show_whole_matrix(vector<double> Jac, vector<double > &b)
    {
        //return;
        cout.precision(4);
        cout << endl;
        
        int n=b.size();
        for(int r=0; r<n; r++)
        {
            for(int c=0; c<n; c++) 
            {
                cout.width(4);
                cout << left << scientific << Jac[c+n*r] << "  ";
                if(c==4) cout << "|  ";
                if(c==9) cout << "|  ";
                if(c==14) cout << "|  ";
            }
            
            cout << "  ||  " << b[r] << endl << endl;
        }
        
        wait_f_r();
        return;
    }

    //===================================================================================
    void eliminate_n_lower_plus_phot(int nlow, double fac_el, int i_gamma, int col, int npxi, 
                                     int n_elim, vector<int> &elim_ind, 
                                     vector<double > &A, vector<double > &bi)
    {
        bi[col]/=fac_el;
        for(int k=1; k<=nlow; k++) bi[col+k]-=A[col+npxi*(col+k)]*bi[col];
        
        bi[i_gamma+0]-=A[col+npxi*(i_gamma+0)]*bi[col];
        bi[i_gamma+1]-=A[col+npxi*(i_gamma+1)]*bi[col];

        for(int i=n_elim-1; i>=0; i--) 
        {
            A[elim_ind[i]+npxi*col]/=fac_el;
            for(int k=1; k<=nlow; k++) 
                A[elim_ind[i]+npxi*(col+k)]-=A[col+npxi*(col+k)]*A[elim_ind[i]+npxi*col];

            A[elim_ind[i]+npxi*(i_gamma+0)]-=A[col+npxi*(i_gamma+0)]*A[elim_ind[i]+npxi*col];
            A[elim_ind[i]+npxi*(i_gamma+1)]-=A[col+npxi*(i_gamma+1)]*A[elim_ind[i]+npxi*col];
        }
        
        return;
    }
    
    //===================================================================================
    void eliminate_n_lower(int nlow, double fac_el, int col, int npxi, 
                           int n_elim, vector<int> &elim_ind, 
                           vector<double > &A, vector<double > &bi)
    {
        bi[col]/=fac_el;
        for(int k=1; k<=nlow; k++) bi[col+k]-=A[col+npxi*(col+k)]*bi[col];
        
        for(int i=n_elim-1; i>=0; i--) 
        {
            A[elim_ind[i]+npxi*col]/=fac_el;
            for(int k=1; k<=nlow; k++) 
                A[elim_ind[i]+npxi*(col+k)]-=A[col+npxi*(col+k)]*A[elim_ind[i]+npxi*col];
        }
        
        return;
    }
    
    //===================================================================================
    void eliminate_n_lower_plus_pol(int nlow, double fac_el, int i_gamma_P, int col, int npxi, 
                                     int n_elim, vector<int> &elim_ind, 
                                     vector<double > &A, vector<double > &bi)
    {
        bi[col]/=fac_el;
        for(int k=1; k<=nlow; k++) bi[col+k]-=A[col+npxi*(col+k)]*bi[col];
        
        bi[i_gamma_P+0]-=A[col+npxi*(i_gamma_P+0)]*bi[col];
        bi[i_gamma_P+2]-=A[col+npxi*(i_gamma_P+2)]*bi[col];
        
        for(int i=n_elim-1; i>=0; i--) 
        {
            A[elim_ind[i]+npxi*col]/=fac_el;
            for(int k=1; k<=nlow; k++) 
                A[elim_ind[i]+npxi*(col+k)]-=A[col+npxi*(col+k)]*A[elim_ind[i]+npxi*col];
            
            A[elim_ind[i]+npxi*(i_gamma_P+0)]-=A[col+npxi*(i_gamma_P+0)]*A[elim_ind[i]+npxi*col];
            A[elim_ind[i]+npxi*(i_gamma_P+2)]-=A[col+npxi*(i_gamma_P+2)]*A[elim_ind[i]+npxi*col];
        }
        
        return;
    }
    
    //===================================================================================
    // Solving the Matrix Equation A*x == b for x for the cosmological perturbation
    // equations. The sparesness and band structure are used for the high l-modes.
    //===================================================================================
    int ODE_Solver_Solve_LA_system_Anisotropies(ODE_solver_matrix &M, 
                                                vector<double > &bi, 
                                                vector<double > &x, 
                                                int n_Nu,
                                                int verbose=0)
    {
        int npxi=bi.size();
        int i_gamma=n_Nu+5;
        int n_gamma=(npxi-i_gamma)/2;
        int i_gamma_P=i_gamma+n_gamma;
        int n_elim;
        vector<int> elim_ind(npxi);
        
//        ODE_solver_matrix Mc=M;
//        vector<double > bic=bi; 
//        vector<double > xc=x;
//        ODE_Solver_Solve_LA_system_GSL(Mc, bic, xc, 1.0e-4);
        
        //===================================================================================
        // eliminate first column
        //===================================================================================
        int col=0;
        
        double fac_el=M.A[col+npxi*col];
        
//        show_whole_matrix(M.A, bi);

        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+3;
        elim_ind[3]=col+5;
        elim_ind[4]=col+7;
        elim_ind[5]=i_gamma+0;
        elim_ind[6]=i_gamma+2;
        n_elim=7;
        
        eliminate_n_lower_plus_phot(6, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate second column
        //===================================================================================
        col=1;

        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+2;
        elim_ind[3]=col+4;
        elim_ind[4]=col+6;
        elim_ind[5]=i_gamma+0;
        elim_ind[6]=i_gamma+2;
        n_elim=7;
        
        eliminate_n_lower_plus_phot(5, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate third column
        //===================================================================================
        col=2;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+3;
        elim_ind[3]=col+5;
        elim_ind[4]=i_gamma+0;
        elim_ind[5]=i_gamma+2;
        n_elim=6;
        
        eliminate_n_lower_plus_phot(4, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);

        //===================================================================================
        // eliminate fourth column
        //===================================================================================
        col=3;

        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+2;
        elim_ind[3]=col+4;
        elim_ind[4]=i_gamma+0;
        elim_ind[5]=i_gamma+2;
        n_elim=6;
        
        eliminate_n_lower_plus_phot(3, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate fifth column
        //===================================================================================
        col=4;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+3;
        elim_ind[3]=i_gamma+0;
        elim_ind[4]=i_gamma+1;
        elim_ind[5]=i_gamma+2;
        n_elim=6;
        
        eliminate_n_lower_plus_phot(2, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate first neutrino column
        //===================================================================================
        col=5;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+2;
        elim_ind[3]=i_gamma+0;
        elim_ind[4]=i_gamma+1;
        elim_ind[5]=i_gamma+2;
        n_elim=6;
        
        eliminate_n_lower_plus_phot(1, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate remaining neutrino columns
        //===================================================================================
        for(col=6; col<i_gamma-1; col++)
        {
            fac_el=M.A[col+npxi*col];
            elim_ind[0]=col;
            elim_ind[1]=col+1;
            elim_ind[2]=i_gamma+0;
            elim_ind[3]=i_gamma+1;
            elim_ind[4]=i_gamma+2;
            n_elim=5;
            
            eliminate_n_lower_plus_phot(1, fac_el, i_gamma, col, npxi, n_elim, elim_ind, M.A, bi);
        }

        //===================================================================================
        // eliminate last neutrino column
        //===================================================================================
        col=i_gamma-1;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=i_gamma+0;
        elim_ind[2]=i_gamma+1;
        elim_ind[3]=i_gamma+2;
        n_elim=4;
        
        eliminate_n_lower(2, fac_el, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate first photon column
        //===================================================================================
        col=i_gamma;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+2;
        n_elim=3;
        
        eliminate_n_lower(1, fac_el, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate second photon column
        //===================================================================================
        col=i_gamma+1;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        n_elim=2;
        
        eliminate_n_lower(1, fac_el, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate remaining photon columns
        //===================================================================================
        for(col=i_gamma+2; col<i_gamma_P-1; col++)
        {
            fac_el=M.A[col+npxi*col];
            elim_ind[0]=col;
            elim_ind[1]=col+1;
            elim_ind[2]=i_gamma_P+0;
            elim_ind[3]=i_gamma_P+2;
            n_elim=4;
            
            eliminate_n_lower_plus_pol(1, fac_el, i_gamma_P, col, npxi, n_elim, elim_ind, M.A, bi);
        }
    
        //===================================================================================
        // eliminate last photon column
        //===================================================================================
        col=i_gamma_P-1;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=i_gamma_P+0;
        elim_ind[2]=i_gamma_P+2;
        n_elim=3;
        
        eliminate_n_lower_plus_pol(0, fac_el, i_gamma_P, col, npxi, n_elim, elim_ind, M.A, bi);

        //===================================================================================
        // eliminate first polarization  column
        //===================================================================================
        col=i_gamma_P;
        
        fac_el=M.A[col+npxi*col];
        elim_ind[0]=col;
        elim_ind[1]=col+1;
        elim_ind[2]=col+2;
        n_elim=3;
        
        eliminate_n_lower(2, fac_el, col, npxi, n_elim, elim_ind, M.A, bi);
        
        //===================================================================================
        // eliminate remaining polarization columns
        //===================================================================================
        for(col=i_gamma_P+1; col<npxi-1; col++)
        {
            fac_el=M.A[col+npxi*col];
            elim_ind[0]=col;
            elim_ind[1]=col+1;
            n_elim=2;
            
            eliminate_n_lower(1, fac_el, col, npxi, n_elim, elim_ind, M.A, bi);
        }
        
        
        fac_el=M.A[col+npxi*col];
        bi[col]/=fac_el;
        M.A[col+npxi*col]/=fac_el;

//        show_whole_matrix(M.A, bi);
    
        //===================================================================================
        // solve
        //===================================================================================
        x[npxi-1]=bi[npxi-1];

        // polarization
        for(col=npxi-2; col>i_gamma_P; col--)
            x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col];
        
        // polarization monopole
        x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+2]*M.A[col+2+npxi*col];
        
        // photons (last col)
        col--;
        x[col]=bi[col];
        for(int i=0; i<3; i++) x[col]+=-x[i_gamma_P+i]*M.A[i_gamma_P+i+npxi*col];
        
        // photons (rest)
        for(col--; col>i_gamma; col--)
        {
            x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col];
            for(int i=0; i<3; i++) 
                x[col]+=-x[i_gamma_P+i]*M.A[i_gamma_P+i+npxi*col];
        }
        
        // photon monopole
        x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+2]*M.A[col+2+npxi*col];
        
        // neutrinos
        col--;
        x[col]=bi[col];
        for(int i=0; i<3; i++) x[col]+=-x[i_gamma+i]*M.A[i_gamma+i+npxi*col];
        
        for(col--; col>5; col--)
        {
            x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col];
            for(int i=0; i<3; i++) x[col]+=-x[i_gamma+i]*M.A[i_gamma+i+npxi*col];
        }

        // neutrino monopole
        x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+2]*M.A[col+2+npxi*col];
        for(int i=0; i<3; i++) x[col]+=-x[i_gamma+i]*M.A[i_gamma+i+npxi*col];
        
        // baryon velocity
        col--;
        x[col]=bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+3]*M.A[col+3+npxi*col];
        for(int i=0; i<3; i++) x[col]+=-x[i_gamma+i]*M.A[i_gamma+i+npxi*col];

        // baryon density
        col--;
        x[col] =bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+2]*M.A[col+2+npxi*col];
        x[col]-=x[col+4]*M.A[col+4+npxi*col];
        x[col]+=-x[i_gamma+0]*M.A[i_gamma+0+npxi*col]-x[i_gamma+2]*M.A[i_gamma+2+npxi*col];
        
        // DM velocity
        col--;
        x[col] =bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+3]*M.A[col+3+npxi*col];
        x[col]-=x[col+5]*M.A[col+5+npxi*col];
        x[col]+=-x[i_gamma+0]*M.A[i_gamma+0+npxi*col]-x[i_gamma+2]*M.A[i_gamma+2+npxi*col];
        
        // DM density
        col--;
        x[col] =bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+2]*M.A[col+2+npxi*col];
        x[col]-=x[col+4]*M.A[col+4+npxi*col];
        x[col]-=x[col+6]*M.A[col+6+npxi*col];
        x[col]+=-x[i_gamma+0]*M.A[i_gamma+0+npxi*col]-x[i_gamma+2]*M.A[i_gamma+2+npxi*col];

        // phi
        col--;
        x[col] =bi[col]-x[col+1]*M.A[col+1+npxi*col]-x[col+3]*M.A[col+3+npxi*col];
        x[col]-=x[col+5]*M.A[col+5+npxi*col];
        x[col]-=x[col+7]*M.A[col+7+npxi*col];
        x[col]+=-x[i_gamma+0]*M.A[i_gamma+0+npxi*col]-x[i_gamma+2]*M.A[i_gamma+2+npxi*col];
       
        elim_ind.clear();

//        for(int k=0; k<(int)xc.size(); k++) cout << k << " " << x[k] << " " << xc[k] << endl;
//        wait_f_r();

        return 0;
    }
    
    //===================================================================================
    //
    // setting up the equation system for matrix solver
    //
    //===================================================================================
    int ODE_Solver_compute_new_Solution(int order, const double zout, const double reltol, 
                                        ODE_Solver_data &ODE_Solver_info, 
                                        ODE_solver_Solution * &Snew, 
                                        ODE_solver_Solution *Sn[6])
    {
        //==============================================================
        double zin=Sn[0]->z, Dz=zout-zin, delta=0;
        int neq=Sn[0]->y.size(), converged=0;
        
        //==============================================================
        // tolerances for Jacobian convergence
        //==============================================================
        double tolJac = reltol/2.0;
        
        //==============================================================
        int Jac_loops=0;
        int LA_error=0;
        int mess=0;
        
        //==============================================================
        // set initial values for iteration: extrapolate from z--> zout
        //==============================================================
        ODE_Solver_info.Sguessptr->z=Snew->z=zout;
        ODE_Solver_extrapolate_ynp1(order, *ODE_Solver_info.Sguessptr, Sn); 
        
        do{
            Jac_loops++;
            
            if(LA_error!=0)
            {
                ODE_Solver_extrapolate_ynp1(order, *ODE_Solver_info.Sguessptr, Sn); 
                LA_error=0;
                if(mess>=2) cout << " resetting Solver " << endl; 
            }
            
            //--------------------------------------------------------
            // redshift z==zn --> compute f_i(yguess, z)
            //--------------------------------------------------------
            ODE_solver_f(neq, zout, 
                         &ODE_Solver_info.Sguessptr->y[0], 
                         &ODE_Solver_info.Sguessptr->dy[0], ODE_Solver_info);
            
            //--------------------------------------------------------
            // after calling this function, ynew will contain y(yguess)
            // delta is the coefficient in front of h f(y); it depends 
            // on the order that is used to compute things
            //--------------------------------------------------------
            delta=ODE_Solver_compute_ynp1(order, *Snew, *ODE_Solver_info.Sguessptr, Sn);
            
            //--------------------------------------------------------
            // this is rhs of J_F Dx = -F
            //--------------------------------------------------------
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
            for(int i=0; i<neq; i++) 
                ODE_Solver_info.F[i]=Snew->y[i]-ODE_Solver_info.Sguessptr->y[i]; 
            
            //--------------------------------------------------------
            // compute Jacobian
            //--------------------------------------------------------
            ODE_solver_compute_Jacobian_Matrix(zout, Dz*delta, neq, 
                                               &ODE_Solver_info.Sguessptr->y[0], 
                                               *ODE_Solver_info.Jacobian_ptr,
                                               ODE_Solver_info); 
            
            //--------------------------------------------------------
            // solve equation for dY
            //--------------------------------------------------------
            LA_error=ODE_Solver_Solve_LA_system_GSL(*ODE_Solver_info.Jacobian_ptr, 
                                                    ODE_Solver_info.F, 
                                                    ODE_Solver_info.dY, tolJac, 1);
            
            //--------------------------------------------------------
            // if nan was produced return main
            //--------------------------------------------------------
            if(LA_error==10) return 10;
            
            //--------------------------------------------------------
            // update current solution
            //--------------------------------------------------------
            if(LA_error==0) 
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
                for(int i=0; i<neq; i++) 
                    ODE_Solver_info.Sguessptr->y[i]+=ODE_Solver_info.dY[i];
            
            //--------------------------------------------------------
            // check convergence of iteration
            //--------------------------------------------------------
            converged=1;
            if(LA_error==0)
            {
                for(int k=0; k<neq; k++) 
                    if( ODE_Solver_info.rel_vector[k]!=0.0 && 
                       (fabs(ODE_Solver_info.dY[k])>=
                        max(ODE_Solver_info.rel_vector[k]*fabs(ODE_Solver_info.Sguessptr->y[k]), 
                            ODE_Solver_info.abs_vector[k]) 
                        ) )
                    { 
                        converged=0; 
                        if(mess>=2) cout << " " << k << " " 
                                         << fabs(ODE_Solver_info.dY[k]) << " " 
                                         << ODE_Solver_info.rel_vector[k]
                                            *fabs(ODE_Solver_info.Sguessptr->y[k]) << " " 
                                         << ODE_Solver_info.abs_vector[k] << endl;
                        break; 
                    }
            }
            
            if(converged==0)
            {
                if(!(Jac_loops%5))
                { 
                    tolJac/=2.0; 
                    cout << " Tightening Jac error setting at it-# " << Jac_loops << endl; 
                }
                
                if(Jac_loops>25) 
                {   
                    converged=1;
                    if(mess>=2){ cout << " Not very happy :S " << endl; wait_f_r_loc(); } 
                    return 1; 
                }
            }
        } 
        while(converged==0);
        
        if(mess>=1) cout << "\n Number of Jacobian iterations " << Jac_loops 
                         << " needed for propagation from z= " << zin 
                         << " by Dz= " << zin-zout << endl;
        
        //--------------------------------------------------------
        // swap variables, so that Snew contains new solution
        //--------------------------------------------------------
        ODE_Solver_info.Sptr=Snew;
        Snew=ODE_Solver_info.Sguessptr;
        ODE_Solver_info.Sguessptr=ODE_Solver_info.Sptr;
        ODE_Solver_info.Sptr=NULL;
        
        return 0;
    }

    //===================================================================================
    //
    // do time-step
    //
    //===================================================================================
    int ODE_Solver_Solve_history(double zs, double zend, 
                                 double Dz_in, double Dz_max, 
                                 ODE_solver_Solution &Sz, 
                                 ODE_Solver_data &ODE_Solver_info)
    {   
        if(ODE_Solver_info.Snewptr==NULL)
        {
            cout << " ODE_Solver_Solve_history:: please set up the memory first! Exiting" 
                 << endl;
            exit(0);
        }
        
        //===========================================================================
        // no PDEs, no OAEs
        //===========================================================================
        set_equation_indices(0, Sz.y.size(), 0, ODE_Solver_info);
        
        //===========================================================================
        // define integration direction
        //===========================================================================
        if(zs>zend) ODE_Solver_info.direction=1;
        else ODE_Solver_info.direction=-1;
        
        //===========================================================================
        // setup
        //===========================================================================
        double zout=zs, zin=zs;
        double Dz_z=max(Dz_in/zs, ODE_Solver_info.Dz_z_last), Dz_z_max;
        int redo_run=0, LAerror=0;
        int maxorder=5;
        int messflag=verbosity_ODE_solver;
        
        if(zs!=0.0) Dz_z_max=fabs((zend-zs)/zs);
        else Dz_z_max=fabs(zend-zs);
        
        if(Dz_max!=0.0 && zs!=0.0) Dz_z_max=Dz_max/zs;
        
        Dz_z=min(Dz_z, Dz_z_max);

        //===========================================================================
        // for initial step reduce the accuracy
        //===========================================================================
        // make sure that max order is not set larger
        ODE_Solver_info.order=(int)min(ODE_Solver_info.order, maxorder);
        
        //===========================================================================
        // try some small step for very first integration
        //===========================================================================
        if(ODE_Solver_info.Dz_z_last==0.0) Dz_z=ODE_Solver_info.tolSol/2.0;
        
        do{ 
            if(ODE_Solver_info.direction==1) 
            {
                if(zin<0.0) zout=max(zin*(1.0+Dz_z), zend); 
                else if(zin>ODE_Solver_info.tolSol) zout=max(zin*(1.0-Dz_z), zend); 
                else zout=max(-ODE_Solver_info.tolSol, zend); 
            }
            else
            {
                if(zin<-ODE_Solver_info.tolSol) zout=min(zin*(1.0-Dz_z), zend); 
                else if(zin>0.0) zout=min(zin*(1.0+Dz_z), zend); 
                else zout=min(ODE_Solver_info.tolSol, zend); 
            }
            
            LAerror=ODE_Solver_compute_new_Solution(ODE_Solver_info.order, zout, 
                                                    ODE_Solver_info.tolSol, 
                                                    ODE_Solver_info, 
                                                    ODE_Solver_info.Snewptr, 
                                                    ODE_Solver_info.Snptr);
            
            if(LAerror==1)
            {
                LAerror=0;
                Dz_z/=2.0;
                Dz_z=max(Dz_z, 1.0e-10);
                zout=zin; 
                redo_run=1;
            }
            else if(LAerror==10){ return 10; }
            else if(LAerror==0)
            {
                //--------------------------------------------------------
                // estimate error and next possible stepsize
                //--------------------------------------------------------
                redo_run=0;
                redo_run=ODE_Solver_estimate_error_and_next_possible_step_size(ODE_Solver_info.order,
                                                                               zin, zout, zend, Dz_z, 
                                                                               Dz_z_max, ODE_Solver_info, 
                                                                               *ODE_Solver_info.Snewptr, 
                                                                               ODE_Solver_info.Snptr,
                                                                               messflag);
                
                //--------------------------------------------------------
                // accepting the current step if 'redo_run==0'
                //--------------------------------------------------------
                if(redo_run==0)
                {
                    ODE_Solver_info.Sptr=ODE_Solver_info.Snptr[5];
                    for(int k=5; k>0; k--) ODE_Solver_info.Snptr[k]=ODE_Solver_info.Snptr[k-1];
                    ODE_Solver_info.Snptr[0]=ODE_Solver_info.Snewptr;
                    ODE_Solver_info.Snewptr=ODE_Solver_info.Sptr;
                    ODE_Solver_info.Sptr=NULL;
                    ODE_Solver_info.count++;
                    
                    if(ODE_Solver_info.count>=ODE_Solver_info.order+1)
                    {
                        ODE_Solver_info.order=(int)min(ODE_Solver_info.order+1, maxorder);
                        ODE_Solver_info.count=1;
                    }
                    //
                    zin=zout;
                }
                else{ redo_run=1; ODE_Solver_info.Jac_is_set=0; }
            }
            else{ cout << " Unknown error in LA-solver accurred: " << LAerror << endl; exit(1); }
        }       
        while(ODE_Solver_info.direction*zout>ODE_Solver_info.direction*zend);
        
        //===========================================================================
        // if run was accepted then Snptr contains new solution!!!
        //===========================================================================
        ODE_Solver_info.Dz_z_last=Dz_z;
        //
        Sz.z =ODE_Solver_info.Snptr[0]->z;
        Sz.y =ODE_Solver_info.Snptr[0]->y;
        Sz.dy=ODE_Solver_info.Snptr[0]->dy;
        
        return 0;
    }
        
    //===================================================================================
    //
    // setting up the equation system for matrix solver
    //
    //===================================================================================
    int ODE_Solver_compute_new_Solution_Anisotropies(int order, const double zout, 
                                                     const double reltol, 
                                                     ODE_Solver_data &ODE_Solver_info, 
                                                     ODE_solver_Solution * &Snew,
                                                     ODE_solver_Solution *Sn[6], int n_Nu)
    {
        //==============================================================
        double zin=Sn[0]->z, Dz=zout-zin, delta=0;
        int neq=Sn[0]->y.size(), converged=0;
        
        //==============================================================
        // tolerances for Jacobian convergence
        //==============================================================
        double tolJac = reltol/3.0;
        
        //==============================================================
        int Jac_loops=0;
        int LA_error=0;
        int mess=0;
        
        //==============================================================
        // set initial values for iteration: extrapolate from z--> zout
        //==============================================================
        ODE_Solver_info.Sguessptr->z=Snew->z=zout;
        ODE_Solver_extrapolate_ynp1(order, *ODE_Solver_info.Sguessptr, Sn);
        
        do{
            Jac_loops++;
            
            if(LA_error!=0)
            {
                ODE_Solver_extrapolate_ynp1(order, *ODE_Solver_info.Sguessptr, Sn);
                LA_error=0;
                if(mess>=2) cout << " resetting Solver " << endl; 
            }
            
            //--------------------------------------------------------
            // redshift z==zn --> compute f_i(yguess, z)
            //--------------------------------------------------------
            ODE_solver_f_param(neq, zout, 
                               &ODE_Solver_info.Sguessptr->y[0], 
                               &ODE_Solver_info.Sguessptr->dy[0],
                               ODE_Solver_info);
            
            //--------------------------------------------------------
            // after calling this function, ynew will contain y(yguess)
            // delta is the coefficient in front of h f(y); it depends 
            // on the order that is used to compute things
            //--------------------------------------------------------
            delta=ODE_Solver_compute_ynp1(order, *Snew, *ODE_Solver_info.Sguessptr, Sn);
            
            //--------------------------------------------------------
            // this is rhs of J_F Dx = -F
            //--------------------------------------------------------
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
            for(int i=0; i<neq; i++) 
                ODE_Solver_info.F[i]=Snew->y[i]-ODE_Solver_info.Sguessptr->y[i]; 
            
            //--------------------------------------------------------
            // compute Jacobian
            //--------------------------------------------------------
            ODE_solver_compute_Jacobian_Matrix_Anisotropies(zout, Dz*delta, neq, 
                                                            &ODE_Solver_info.Sguessptr->y[0], 
                                                            *ODE_Solver_info.Jacobian_ptr,
                                                            ODE_Solver_info); 
            
            //--------------------------------------------------------
            // solve equation for dY
            //--------------------------------------------------------
            LA_error=ODE_Solver_Solve_LA_system_Anisotropies(*ODE_Solver_info.Jacobian_ptr, 
                                                             ODE_Solver_info.F, 
                                                             ODE_Solver_info.dY, n_Nu, 1);
           
/*           LA_error=ODE_Solver_Solve_LA_system_GSL(*ODE_Solver_info.Jacobian_ptr, 
                                                    ODE_Solver_info.F, 
                                                    ODE_Solver_info.dY, 1);
*/           
            //--------------------------------------------------------
            // if nan was produced return main
            //--------------------------------------------------------
            if(LA_error==10) return 10;
            
            //--------------------------------------------------------
            // update current solution
            //--------------------------------------------------------
            if(LA_error==0) 
#ifdef OPENMP_ACTIVATED_ODE    
#pragma omp parallel for default(shared)	
#endif	
                for(int i=0; i<neq; i++) 
                    ODE_Solver_info.Sguessptr->y[i]+=ODE_Solver_info.dY[i];
            
            //--------------------------------------------------------
            // check convergence of iteration
            //--------------------------------------------------------
            converged=1;
            if(LA_error==0)
            {
                for(int k=0; k<neq; k++) 
                    if( ODE_Solver_info.rel_vector[k]!=0.0 && 
                       (fabs(ODE_Solver_info.dY[k])>=
                        max(ODE_Solver_info.rel_vector[k]
                            *fabs(ODE_Solver_info.Sguessptr->y[k]), ODE_Solver_info.abs_vector[k]) 
                        ) )
                    { 
                        converged=0; 
                        if(mess>=2) cout << " " << k << " " << fabs(ODE_Solver_info.dY[k]) << " " 
                            << ODE_Solver_info.rel_vector[k]
                            *fabs(ODE_Solver_info.Sguessptr->y[k]) << " " 
                            << ODE_Solver_info.abs_vector[k] << endl;
                        break; 
                    }
            }
            
            if(converged==0)
            {
                if(!(Jac_loops%5))
                { 
                    tolJac/=2.0; 
                    cout << " Tightening Jac error setting at it-# " << Jac_loops << endl; 
                }
                
                if(Jac_loops>25) 
                {   
                    converged=1;
                    if(mess>=2){ cout << " Not very happy :S " << endl; wait_f_r_loc(); } 
                    return 1; 
                }
            }
        } 
        while(converged==0);
        
        if(mess>=1) cout << "\n Number of Jacobian iterations " << Jac_loops 
            << " needed for propagation from z= " << zin 
            << " by Dz= " << zin-zout << endl;
        
        //--------------------------------------------------------
        // swap variables, so that Snew contains new solution
        //--------------------------------------------------------
        ODE_Solver_info.Sptr=Snew;
        Snew=ODE_Solver_info.Sguessptr;
        ODE_Solver_info.Sguessptr=ODE_Solver_info.Sptr;
        ODE_Solver_info.Sptr=NULL;
        
        return 0;
    }

    //===================================================================================
    //
    // do time-step
    //
    //===================================================================================
    int ODE_Solver_Solve_history_Anisotropies(double zs, double zend, 
                                              double Dz_in, double Dz_max, 
                                              ODE_solver_Solution &Sz, 
                                              ODE_Solver_data &ODE_Solver_info, int n_Nu)
    {   
        if(ODE_Solver_info.Snewptr==NULL)
        {
            cout << " ODE_Solver_Solve_history:: please set up the memory first! Exiting" 
                 << endl;
            exit(0);
        }
        
        //===========================================================================
        // define integration direction
        //===========================================================================
        if(zs>zend) ODE_Solver_info.direction=1;
        else ODE_Solver_info.direction=-1;
        
        //===========================================================================
        // setup
        //===========================================================================
        double zout=zs, zin=zs;
        double Dz_z=max(Dz_in/zs, ODE_Solver_info.Dz_z_last), Dz_z_max;
        int redo_run=0, LAerror=0;
        int maxorder=5;
        int messflag=verbosity_ODE_solver;
        
        if(zs!=0.0) Dz_z_max=fabs((zend-zs)/zs);
        else Dz_z_max=fabs(zend-zs);
        
        if(Dz_max!=0.0 && zs!=0.0) Dz_z_max=Dz_max/zs;
        
        Dz_z=min(Dz_z, Dz_z_max);
        
        //===========================================================================
        // for initial step reduce the accuracy
        //===========================================================================
        // make sure that max order is not set larger
        ODE_Solver_info.order=(int)min(ODE_Solver_info.order, maxorder);
        
        //===========================================================================
        // try some small step for very first integration
        //===========================================================================
        if(ODE_Solver_info.Dz_z_last==0.0) Dz_z=ODE_Solver_info.tolSol/2.0;
        
        do{ 
            if(ODE_Solver_info.direction==1) 
            {
                if(zin<0.0) zout=max(zin*(1.0+Dz_z), zend); 
                else if(zin>ODE_Solver_info.tolSol) zout=max(zin*(1.0-Dz_z), zend); 
                else zout=max(-ODE_Solver_info.tolSol, zend); 
            }
            else
            {
                if(zin<-ODE_Solver_info.tolSol) zout=min(zin*(1.0-Dz_z), zend); 
                else if(zin>0.0) zout=min(zin*(1.0+Dz_z), zend); 
                else zout=min(ODE_Solver_info.tolSol, zend); 
            }
            
            LAerror=ODE_Solver_compute_new_Solution_Anisotropies(ODE_Solver_info.order, zout, 
                                                                 ODE_Solver_info.tolSol, 
                                                                 ODE_Solver_info, 
                                                                 ODE_Solver_info.Snewptr, 
                                                                 ODE_Solver_info.Snptr, n_Nu);
            
            if(LAerror==1)
            {
                LAerror=0;
                Dz_z/=2.0;
                Dz_z=max(Dz_z, 1.0e-10);
                zout=zin; 
                redo_run=1;
            }
            else if(LAerror==10){ return 10; }
            else if(LAerror==0)
            {
                //--------------------------------------------------------
                // estimate error and next possible stepsize
                //--------------------------------------------------------
                redo_run=0;
                redo_run=ODE_Solver_estimate_error_and_next_possible_step_size(ODE_Solver_info.order,
                                                                               zin, zout, zend, Dz_z, 
                                                                               Dz_z_max, ODE_Solver_info, 
                                                                               *ODE_Solver_info.Snewptr, 
                                                                               ODE_Solver_info.Snptr,
                                                                               messflag);
                
                redo_run=0;
                
                //--------------------------------------------------------
                // accepting the current step if 'redo_run==0'
                //--------------------------------------------------------
                if(redo_run==0)
                {
                    ODE_Solver_info.Sptr=ODE_Solver_info.Snptr[5];
                    for(int k=5; k>0; k--) ODE_Solver_info.Snptr[k]=ODE_Solver_info.Snptr[k-1];
                    ODE_Solver_info.Snptr[0]=ODE_Solver_info.Snewptr;
                    ODE_Solver_info.Snewptr=ODE_Solver_info.Sptr;
                    ODE_Solver_info.Sptr=NULL;
                    ODE_Solver_info.count++;
                    
                    if(ODE_Solver_info.count>=ODE_Solver_info.order+1)
                    {
                        ODE_Solver_info.order=(int)min(ODE_Solver_info.order+1, maxorder);
                        ODE_Solver_info.count=1;
                    }
                    //
                    zin=zout;
                }
                else{ redo_run=1; ODE_Solver_info.Jac_is_set=0; }
            }
            else{ cout << " Unknown error in LA-solver accurred: " << LAerror << endl; exit(1); }
        }       
        while(ODE_Solver_info.direction*zout>ODE_Solver_info.direction*zend);
        
        //===========================================================================
        // if run was accepted then Snptr contains new solution!!!
        //===========================================================================
        ODE_Solver_info.Dz_z_last=Dz_z;
        //
        Sz.z =ODE_Solver_info.Snptr[0]->z;
        Sz.y =ODE_Solver_info.Snptr[0]->y;
        Sz.dy=ODE_Solver_info.Snptr[0]->dy;
        
        return 0;
    }
    
    //===================================================================================
    // Solving the Matrix Equation A*x == b for x
    // Here it is assume that i=[0, index_nODE-1] is PDE while i=[index_nODE, index_OAE-1]
    // is a coupled ODE and i=[index_nOAE, neq-1] couples OAEs. 
    // The PDE is assumed to have 4 off-diagonals
    //===================================================================================
    int ODE_Solver_Solve_LA_system_PDE_ODE(ODE_solver_matrix &Jac, 
                                           vector<double > &b, 
                                           vector<double > &x, int verbose=0)
    {
        int neq=b.size();

        //===============================================================================
        // eliminate 0. column
        //===============================================================================
        double el_ij[6];
        int ind=0, i_diag, m, last=neq-1;
        
        i_diag=Jac.diags[ind];
        el_ij[0]=Jac.A[i_diag];
        Jac.A[i_diag]=1.0;
        for(int k=1; k<=4; k++) Jac.A[Jac.diags[ind+k]-k]/=el_ij[0];
        Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
        b[ind]/=el_ij[0];
        
        //===============================================================================
        // sub-diagonals + last row & col
        //===============================================================================
        for(int k=1; k<=2+1; k++)
        {
            el_ij[k]=Jac.A[k+i_diag];
            Jac.A[k+i_diag]=0.0;
        }
        
        b[1+ind]-=el_ij[1]*b[ind];
        b[2+ind]-=el_ij[2]*b[ind];
        b[last] -=el_ij[3]*b[ind];
        
        for(int l=1; l<5; l++)
        {
            m=Jac.diags[ind+l]-l;
            Jac.A[m+1]-=el_ij[1]*Jac.A[m];
            Jac.A[m+2]-=el_ij[2]*Jac.A[m];
            Jac.A[m+l+3]-=el_ij[3]*Jac.A[m];
        }     

        int l=last-ind;
        {
            m=Jac.diags[ind+l]-l;
            Jac.A[m+1]-=el_ij[1]*Jac.A[m];
            Jac.A[m+2]-=el_ij[2]*Jac.A[m];
            Jac.A[Jac.diags[last]]-=el_ij[3]*Jac.A[m];
        }     
        
        //===============================================================================
        // eliminate 1. column
        //===============================================================================
        ind=1;
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            for(int k=1; k<=3; k++) Jac.A[Jac.diags[ind+k]-k]/=el_ij[0];
            Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
            b[ind]/=el_ij[0];

            //===============================================================================
            // sub-diagonals + last row & col
            //===============================================================================
            for(int k=1; k<=2+1; k++)
            {
                el_ij[k]=Jac.A[k+i_diag];
                Jac.A[k+i_diag]=0.0;
            }

            b[1+ind]-=el_ij[1]*b[ind];
            b[2+ind]-=el_ij[2]*b[ind];
            b[last] -=el_ij[3]*b[ind];

            for(int l=1; l<4; l++)
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]-=el_ij[1]*Jac.A[m];
                Jac.A[m+2]-=el_ij[2]*Jac.A[m];
                Jac.A[m+l+3]-=el_ij[3]*Jac.A[m];
            }     

            int l=last-ind;
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]-=el_ij[1]*Jac.A[m];
                Jac.A[m+2]-=el_ij[2]*Jac.A[m];
                Jac.A[Jac.diags[last]]-=el_ij[3]*Jac.A[m];
            }     
        }
        
        //===============================================================================
        // eliminate column 2..neq-9
        //===============================================================================
        for(ind=2; ind<=neq-9; ind++)
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            for(int k=1; k<=2; k++) Jac.A[Jac.diags[ind+k]-k]/=el_ij[0];
            Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
            b[ind]/=el_ij[0];

            //===========================================================================
            // sub-diagonals + last row & col
            //===========================================================================
            for(int k=1; k<=2+1; k++)
            {
                el_ij[k]=Jac.A[k+i_diag];
                Jac.A[k+i_diag]=0.0;
            }

            b[1+ind]-=el_ij[1]*b[ind];
            b[2+ind]-=el_ij[2]*b[ind];
            b[last] -=el_ij[3]*b[ind];
            
            for(int l=1; l<3; l++)
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]  -=el_ij[1]*Jac.A[m];
                Jac.A[m+2]  -=el_ij[2]*Jac.A[m];
                Jac.A[m+l+3]-=el_ij[3]*Jac.A[m];
            }     
           
            int l=last-ind;
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]-=el_ij[1]*Jac.A[m];
                Jac.A[m+2]-=el_ij[2]*Jac.A[m];
                Jac.A[Jac.diags[last]]-=el_ij[3]*Jac.A[m];
           }     
        }
            
        //===============================================================================
        // eliminate column neq-8 .. neq-4
        //===============================================================================
        for(ind=neq-8; ind<=neq-4; ind++)
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            for(int k=1; k<=2; k++) Jac.A[Jac.diags[ind+k]-k]/=el_ij[0];
            Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
            b[ind]/=el_ij[0];
            
            //===========================================================================
            // sub-diagonals + last row & col
            //===========================================================================
            int kmax=2+1;
            if(ind==neq-6) kmax=4+1;
            else if(ind==neq-5) kmax=3+1;
            else if(ind==neq-4) kmax=2+1;
                
            for(int k=1; k<=kmax; k++)
            {
                el_ij[k]=Jac.A[k+i_diag];
                Jac.A[k+i_diag]=0.0;
            }
            
            for(int k=1; k<kmax; k++) b[k+ind]-=el_ij[k]*b[ind];
            b[last]-=el_ij[kmax]*b[ind];

            for(int l=1; l<3; l++)
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]-=el_ij[1]*Jac.A[m];
                Jac.A[m+2]-=el_ij[2]*Jac.A[m];
                
                if(ind==neq-4) Jac.A[m+3]-=el_ij[3]*Jac.A[m];
                else if(ind==neq-5)
                {
                    Jac.A[m+3]-=el_ij[3]*Jac.A[m];
                    Jac.A[m+4]-=el_ij[4]*Jac.A[m];
                }
                else if(ind==neq-6)
                {
                    Jac.A[m+3]-=el_ij[3]*Jac.A[m];
                    Jac.A[m+4]-=el_ij[4]*Jac.A[m];
                    Jac.A[m+5]-=el_ij[5]*Jac.A[m];
                }
                else if(ind==neq-7)
                {
                    Jac.A[m+6]-=el_ij[3]*Jac.A[m];
                }
                else if(ind==neq-8)
                {
                    if(l==1) Jac.A[m+l+3]-=el_ij[3]*Jac.A[m];
                    else Jac.A[m+7]-=el_ij[3]*Jac.A[m];
                }
            }     
            
            int l=last-ind;
            {
                m=Jac.diags[ind+l]-l;
                Jac.A[m+1]-=el_ij[1]*Jac.A[m];
                Jac.A[m+2]-=el_ij[2]*Jac.A[m];
                
                if(ind==neq-4) Jac.A[m+3]-=el_ij[3]*Jac.A[m];
                else if(ind==neq-5)
                {
                    Jac.A[m+3]-=el_ij[3]*Jac.A[m];
                    Jac.A[m+4]-=el_ij[4]*Jac.A[m];
                }
                else if(ind==neq-6)
                {
                    Jac.A[m+3]  -=el_ij[3]*Jac.A[m];
                    Jac.A[m+4]  -=el_ij[4]*Jac.A[m];
                    Jac.A[m+5]  -=el_ij[5]*Jac.A[m];
                }
                else if(ind==neq-7 || ind==neq-8)
                    Jac.A[Jac.diags[last]]-=el_ij[3]*Jac.A[m];
            }     
        }

        //===============================================================================
        // eliminate column neq-3
        //===============================================================================
        ind=neq-3;
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            Jac.A[Jac.diags[ind+1]-1]/=el_ij[0];
            Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
            b[ind]/=el_ij[0];
            
            //===========================================================================
            // only last two rows in addition
            //===========================================================================
            for(int k=1; k<=2; k++)
            {
                el_ij[k]=Jac.A[k+i_diag];
                Jac.A[k+i_diag]=0.0;
            }
            
            for(int k=1; k<2; k++) b[k+ind]-=el_ij[k]*b[ind];
            b[last]-=el_ij[2]*b[ind];

            for(int l=1; l<=2; l++)
                for(int k=1; k<=2; k++)
                {
                    m=Jac.diags[ind+l]-l;
                    Jac.A[m+k]-=el_ij[k]*Jac.A[m];
                }            
        }

        //===============================================================================
        // eliminate column neq-2
        //===============================================================================
        ind=neq-2;
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            Jac.A[Jac.diags[last]-last+ind]/=el_ij[0];
            b[ind]/=el_ij[0];
            
            //===========================================================================
            // only last row in addition
            //===========================================================================
            int k=1;
            el_ij[k]=Jac.A[k+i_diag];
            Jac.A[k+i_diag]=0.0;
            b[k+ind]-=el_ij[k]*b[ind];

            m=Jac.diags[ind+1]-1;
            Jac.A[Jac.diags[last]]-=el_ij[1]*Jac.A[m];
        }

        //===============================================================================
        // eliminate column neq-1
        //===============================================================================
        ind=neq-1;
        {
            i_diag=Jac.diags[ind];
            el_ij[0]=Jac.A[i_diag];
            Jac.A[i_diag]=1.0;
            b[ind]/=el_ij[0];
        }

        //===============================================================================
        // back-substitution
        //===============================================================================
        double xTe=b[ind];
        x[ind]=xTe;

        ind--;
        for(int k=0; k<=ind; k++){ x[k]=b[k]-xTe*Jac.A[Jac.diags[neq-1]-(neq-1-k)]; }
        
        ind--;
        x[ind]-=x[ind+1]*Jac.A[Jac.diags[ind+1]-1];
        
        for(ind--; ind>=2; ind--)
        {
            x[ind]-=x[ind+2]*Jac.A[Jac.diags[ind+2]-2];
            x[ind]-=x[ind+1]*Jac.A[Jac.diags[ind+1]-1];
        }
        
        ind=1;
        x[ind]-=x[ind+3]*Jac.A[Jac.diags[ind+3]-3];
        x[ind]-=x[ind+2]*Jac.A[Jac.diags[ind+2]-2];
        x[ind]-=x[ind+1]*Jac.A[Jac.diags[ind+1]-1];
        
        ind=0;
        x[ind]-=x[ind+4]*Jac.A[Jac.diags[ind+4]-4];
        x[ind]-=x[ind+3]*Jac.A[Jac.diags[ind+3]-3];
        x[ind]-=x[ind+2]*Jac.A[Jac.diags[ind+2]-2];
        x[ind]-=x[ind+1]*Jac.A[Jac.diags[ind+1]-1];
        
        return 0;
    }    
    
    //===================================================================================
    // Solving the Matrix Equation A*x == b for x
    // Here it is assume that i=[0, index_nODE-1] is PDE while i=[index_nODE,index_OAE-1]
    // is a coupled ODE and i=[index_nOAE, neq-1] couples OAEs. 
    // The PDE is assumed to have 4 off-diagonals; Jacobian is stored in column format
    //===================================================================================
    void get_matrix_elements(int i_diag, int kmax, ODE_solver_matrix &Jac, double *el_ij)
    {
        for(int k=0; k<=kmax; k++)
        { 
            el_ij[k]=Jac.A[k+i_diag];
            Jac.A[k+i_diag]=0.0; // after elimination they are == 0 in any case
        }            
        
        Jac.A[i_diag]=1.0; // after elimination this matix element should be == 1
        
        return;
    }
        
    //===================================================================================
    void devide_row(int row, int ksub, int iAe, int nAe, double el0, 
                    ODE_solver_matrix &Jac, vector<double > &b)
    {
        for(int k=1; k<=ksub; k++) Jac.A[Jac.diags[row+k]-k]          /=el0;
        for(int k=0; k<nAe; k++)   Jac.A[Jac.diags[iAe+k]-(iAe+k)+row]/=el0;
        b[row]/=el0;
        
        //show_whole_matrix(Jac, b);
        return;
    }
    
    //===================================================================================
    void eliminate_subdiags(int row, int krow, int kcol, int iAe, int nAe, 
                            const double *el_ij, 
                            ODE_solver_matrix &Jac, vector<double > &b)
    {
        for(int k=1; k<=krow; k++) b[row+k]-=el_ij[k]       *b[row];
        for(int k=0; k<  nAe; k++) b[iAe+k]-=el_ij[krow+1+k]*b[row];
        
        for(int c=1; c<=kcol; c++)
        {
            int m=Jac.diags[row+c]-c;
            for(int k=1; k<=krow; k++) Jac.A[m+k]  -=el_ij[k]*Jac.A[m];
            
            //int mAe=(row+c<iAe-1 ? Jac.diags[row+c+1]-2-nAe : Jac.diags[row+c]+1); // first few rows were incorrect
            int mAe=(iAe-(row+c)>5 ? Jac.diags[row+c]+2+nAe : Jac.diags[row+c]+iAe-(row+c));
            for(int k=0; k<  nAe; k++) Jac.A[mAe+k]-=el_ij[krow+1+k]*Jac.A[m];

            // useful for checks [JC]
            //cout << row+c << " " << mAe+0 << " " << Get_element_index(row+c, b.size()-nAe, Jac) << " ";
            //for(int k=0; k<nAe; k++) Jac.A[Get_element_index(row+c, b.size()-nAe, Jac)]-=el_ij[krow+1+k]*Jac.A[m];
            //cout << scientific << Jac.A[mAe+0] << endl;
        }

        for(int c=0; c<nAe; c++)
        {
            int m=Jac.diags[iAe+c]-(iAe+c)+row;
            for(int k=1; k<=krow; k++) Jac.A[m+k]        -=el_ij[k]*Jac.A[m];
            for(int k=0; k<  nAe; k++) Jac.A[m-row+iAe+k]-=el_ij[krow+1+k]*Jac.A[m];
        }
        
        //show_whole_matrix(Jac, b);

        return;
    }    
    
    //===================================================================================
    int ODE_Solver_Solve_LA_system_PDE_ODE_OAE(ODE_solver_matrix &Jac, 
                                               vector<double > &b, 
                                               vector<double > &x, 
                                               int index_Ae, 
                                               int verbose=0)
    {
        int neq=b.size();
        int nAe=neq-index_Ae, nPDE=index_Ae;
        double *el_ij=new double[5+nAe];
        
        //show_whole_matrix(Jac, b);

        //===============================================================================
        // eliminate 0. column
        //===============================================================================
        int row=0, i_diag=Jac.diags[row];

        get_matrix_elements(i_diag, 2+nAe, Jac, el_ij);
        devide_row(row, 4, index_Ae, nAe, el_ij[0], Jac, b);
        eliminate_subdiags(row, 2, 4, index_Ae, nAe, el_ij, Jac, b);
                
        //===============================================================================
        // eliminate 1. column
        //===============================================================================
        row=1; i_diag=Jac.diags[row];
        
        get_matrix_elements(i_diag, 2+nAe, Jac, el_ij);
        devide_row(row, 3, index_Ae, nAe, el_ij[0], Jac, b);
        eliminate_subdiags(row, 2, 3, index_Ae, nAe, el_ij, Jac, b);

        //===============================================================================
        // eliminate column 2..neq-6
        //===============================================================================
        for(row=2; row<=nPDE-6; row++)
        {
            i_diag=Jac.diags[row];

            get_matrix_elements(i_diag, 2+nAe, Jac, el_ij);
            devide_row(row, 2, index_Ae, nAe, el_ij[0], Jac, b);
            eliminate_subdiags(row, 2, 2, index_Ae, nAe, el_ij, Jac, b);
        }        
        
        //===============================================================================
        // Upper left corner is easy, but lower right corner is a bit more tricky...
        //
        // x x o x x         |      nPDE - 8
        //   x x o x x       |      nPDE - 7
        //     x x o x x     |      nPDE - 6
        //       x x o x x   |      nPDE - 5
        //         x x o x x |      nPDE - 4
        //           x x o x | x    nPDE - 3
        //           x x x o | x    nPDE - 2
        //           x x x x | o    nPDE - 1
        //===============================================================================
        
        //===============================================================================
        // eliminate column nPDE-8 .. nPDE-3
        //===============================================================================
        for(row=nPDE-5; row<=nPDE-3; row++)
        {
            i_diag=Jac.diags[row];
            int kmax=4-(row-(nPDE-5));

            get_matrix_elements(i_diag, kmax+nAe, Jac, el_ij);
            devide_row(row, 2, index_Ae, nAe, el_ij[0], Jac, b);
            eliminate_subdiags(row, kmax, 2, index_Ae, nAe, el_ij, Jac, b);
        }
        
        //===============================================================================
        // eliminate column nPDE-2
        //===============================================================================
        row=nPDE-2; i_diag=Jac.diags[row];
        
        get_matrix_elements(i_diag, 1+nAe, Jac, el_ij);
        devide_row(row, 1, index_Ae, nAe, el_ij[0], Jac, b);
        eliminate_subdiags(row, 1, 1, index_Ae, nAe, el_ij, Jac, b);
        
        //===============================================================================
        // eliminate column nPDE-1
        //===============================================================================
        row=nPDE-1; i_diag=Jac.diags[row];
        
        get_matrix_elements(i_diag, nAe, Jac, el_ij);
        devide_row(row, 0, index_Ae, nAe, el_ij[0], Jac, b);
        eliminate_subdiags(row, 0, 0, index_Ae, nAe, el_ij, Jac, b);

        //===============================================================================
        // eliminate ODE + OAE part
        //===============================================================================
        for(row=index_Ae; row<neq; row++)
        {
            i_diag=Jac.diags[row];

            get_matrix_elements(i_diag, nAe-(row-index_Ae), Jac, el_ij);
            devide_row(row, 0, row, nAe-(row-index_Ae), el_ij[0], Jac, b);
            eliminate_subdiags(row, 0, 0, row, nAe-(row-index_Ae), el_ij, Jac, b);
        }
        
        //===============================================================================
        // back-substitution (ODE + OAE part)
        //===============================================================================
        for(int k=0; k<neq; k++) x[k]=b[k];
        
        for(row=neq-2; row>=index_Ae-1; row--)
            for(int k=0; k<=row; k++)
                x[k]-=x[row+1]*Jac.A[Jac.diags[row+1]-(row+1)+k]; 
        
        //===============================================================================
        // back-substitution (PDE part)
        //===============================================================================
        row=index_Ae-2;
        x[row]-=x[row+1]*Jac.A[Jac.diags[row+1]-1];
        
        for(row--; row>=2; row--)
        {
            x[row]-=x[row+2]*Jac.A[Jac.diags[row+2]-2];
            x[row]-=x[row+1]*Jac.A[Jac.diags[row+1]-1];
        }
        
        row=1;
        x[row]-=x[row+3]*Jac.A[Jac.diags[row+3]-3];
        x[row]-=x[row+2]*Jac.A[Jac.diags[row+2]-2];
        x[row]-=x[row+1]*Jac.A[Jac.diags[row+1]-1];
        
        row=0;
        x[row]-=x[row+4]*Jac.A[Jac.diags[row+4]-4];
        x[row]-=x[row+3]*Jac.A[Jac.diags[row+3]-3];
        x[row]-=x[row+2]*Jac.A[Jac.diags[row+2]-2];
        x[row]-=x[row+1]*Jac.A[Jac.diags[row+1]-1];
        
        delete [] el_ij;
        
        return 0;
    }    

    //===================================================================================
    //
    // Check users Jacobian using numerical derivatives
    //
    //===================================================================================
    void check_Jacobian_setup(int neq, int J, double z, double Dz, double *y,
                              const vector<double> &Jij, ODE_Solver_data &SD)
    {
        vector<double> JijN(neq);
        double eps=1.0e-4;

        ODE_solver_jac_2p(neq, J, z, Dz, y, &JijN[0], SD);

        for(int row=0; row<neq; row++)
        if(fabs(JijN[row]-Jij[row])>fabs(eps*Jij[row]))
        {
            cout << " Element " << row << " " << J
                 << " inconsistent at relative level ~" << eps << endl;
            cout << " User == " << Jij[row] << " || Numerical == " << JijN[row] << endl;
        }

        //wait_f_r();
        return;
    }

    //===================================================================================
    //
    // Jacobian setup routine for PDE + ODE solver; it is assumed that jacobian setup is 
    // performed by user, but here the structure is saved. 
    //
    //===================================================================================
    void ODE_solver_compute_Jacobian_Matrix_PDE_ODE_OAE_full(double z, double Dz,
                                                             int neq, double *y,
                                                             ODE_solver_matrix &Jac,
                                                             ODE_Solver_data &SD, int mess=0)
    {
        //===============================================================================
        // z is the current redshift
        // y[] should contain the solution for which the jacobian is needed
        // Jac[] should have dimension neq*neq
        //===============================================================================
        if(mess==1) cout << " entering full Jacobinan update. " << endl;
        
        Jac.A.clear();
        Jac.row.clear();
        Jac.col.clear();
        Jac.diags.clear();
        
        //===============================================================================
        // this vector will be used to store the jacobian after calls of f_i(X)
        //===============================================================================
        vector<double> Jij(neq);
        
        //===============================================================================
        // fill Matrix with values (PDE part)
        //===============================================================================
        for(int J=0; J<SD.index_ODE; J++)
        {   
            ODE_solver_jac_user(neq, J, z, Dz, y, &Jij[0], SD);
            if(SD.check_jacobian) check_Jacobian_setup(neq, J, z, Dz, y, Jij, SD);
            
            for(int row=0; row<SD.index_ODE; row++)
                if(Jij[row]!=0.0 || (row==0 && J<=4) || (row==SD.index_ODE-1 && SD.index_ODE-5<=J))
                {
                    Jac.A.push_back(Jij[row]);
                    Jac.col.push_back(J);
                    Jac.row.push_back(row);
                    
                    if(J==row) Jac.diags.push_back((int)(Jac.A.size()-1));
                }
            
            //===========================================================================
            // for ode part ALL elements are saved
            //===========================================================================
            for(int row=SD.index_ODE; row<neq; row++)
            {
                Jac.A.push_back(Jij[row]);
                Jac.col.push_back(J);
                Jac.row.push_back(row);
                
                if(J==row) Jac.diags.push_back((int)(Jac.A.size()-1));    
            }
        }
        
        //===============================================================================
        // fill Matrix with values (ODE + OAE parts)
        //===============================================================================
        for(int J=SD.index_ODE; J<neq; J++)
        {   
            ODE_solver_jac_user(neq, J, z, Dz, y, &Jij[0], SD);
            if(SD.check_jacobian) check_Jacobian_setup(neq, J, z, Dz, y, Jij, SD);
            
            //===========================================================================
            // for ode part ALL elements are saved
            //===========================================================================
            for(int row=0; row<neq; row++)
            {
                Jac.A.push_back(Jij[row]);
                Jac.col.push_back(J);
                Jac.row.push_back(row);
                
                if(J==row) Jac.diags.push_back((int)(Jac.A.size()-1));    
            }
        }

        if(mess==1) cout << " Number of non-zero elements = " << Jac.A.size() 
                         << ". Full matrix has " << neq*neq << " elements " << endl;
        
        if((int)Jac.diags.size()!=neq) cerr << " Hmmm. That should not happen... " << endl;

        return;
    }        
    
    //===================================================================================
    void ODE_solver_compute_Jacobian_Matrix_PDE_ODE_OAE(double z, double Dz,
                                                        int neq, double *y,
                                                        ODE_solver_matrix &Jac,
                                                        ODE_Solver_data &SD, int mess=0)
    {
        //if(z<=1.0e+5) Jac.A.clear();
        
        if(Jac.A.size()==0)
        {
            ODE_solver_compute_Jacobian_Matrix_PDE_ODE_OAE_full(z, Dz, neq, y, Jac, SD, mess);
            return;
        }
        
        //===============================================================================
        // z is the current redshift
        // y[] should contain the solution for which the jacobian is needed
        // Jac[] should have dimension neq*neq
        //===============================================================================
        if(mess==1) cout << " entering Jacobinan update using previous structure. " << endl;
        
        //===============================================================================
        // this vector will be used to store the jacobian after calls of f_i(X)
        //===============================================================================
        unsigned int i=0;
        vector<double> Jij(neq);
        
        //===============================================================================
        // fill Matrix with values
        //===============================================================================
        for(int J=0; J<neq; J++)
        {
            ODE_solver_jac_user(neq, J, z, Dz, y, &Jij[0], SD);
            
            for(; i<Jac.A.size() && J==Jac.col[i]; i++) Jac.A[i]=Jij[Jac.row[i]];
        }
    
        if(mess==1) cout << " Number of non-zero elements = " << Jac.A.size()
                         << ". Full matrix has " << neq*neq << " elements " << endl;
        
        if((int)Jac.diags.size()!=neq)
            cout << " Hmmm. That should not happen... " << endl;
        
        return;
    }

    //===================================================================================
    //
    // setting up the equation system for matrix solver
    //
    //===================================================================================
    int ODE_Solver_compute_new_Solution_PDE_ODE_OAE(int order, const double zout, 
                                                    const double reltol, 
                                                    ODE_Solver_data &ODE_Solver_info, 
                                                    ODE_solver_Solution * &Snew,
                                                    ODE_solver_Solution *Sn[6])
    {
        //===============================================================================
        double zin=Sn[0]->z, Dz=zout-zin, delta=0;
        int neq=Sn[0]->y.size(), converged=0;
        
        //===============================================================================
        int Jac_loops=0, Jac_loops_max=10;
        int mess=verbosity_ODE_solver-1;
        
        //===============================================================================
        // set initial values for iteration: extrapolate from z --> zout to get yguess.
        // This is only based on previous solutions of the problem & does not depend on
        // whether one is dealing with ODE, PDE or OAE.
        //===============================================================================
        ODE_Solver_info.Sguessptr->z=Snew->z=zout;
        ODE_Solver_extrapolate_ynp1(order, *ODE_Solver_info.Sguessptr, Sn);
        
        do{
            Jac_loops++;
            
            //---------------------------------------------------------------------------
            // redshift z==zn --> compute f_i(yguess, z)
            //---------------------------------------------------------------------------
            ODE_solver_f(neq, zout,
                         &ODE_Solver_info.Sguessptr->y[0], 
                         &ODE_Solver_info.Sguessptr->dy[0],
                         ODE_Solver_info);
            
            //---------------------------------------------------------------------------
            // after calling this function, ynew will contain y(yguess)
            // delta is the coefficient in front of h f(y); it depends 
            // on the order that is used to compute things.
            //---------------------------------------------------------------------------
            // This is like evaluatinf Eq. (2) of CVD 2010 (1003.4928)
            // using the initial guess. Generally Snew.y != yguess, so
            // system has to be solved next.
            //---------------------------------------------------------------------------
            delta=ODE_Solver_compute_ynp1(order, *Snew, *ODE_Solver_info.Sguessptr, Sn);
            
            //---------------------------------------------------------------------------
            // this is rhs of J_F Dx = -F for PDE + ODE parts
            //---------------------------------------------------------------------------
            for(int i=0; i<ODE_Solver_info.index_OAE; i++)
                ODE_Solver_info.F[i]=Snew->y[i]-ODE_Solver_info.Sguessptr->y[i]; 
            
            //---------------------------------------------------------------------------
            // OAE part
            //---------------------------------------------------------------------------
            for(int i=ODE_Solver_info.index_OAE; i<neq; i++)
                ODE_Solver_info.F[i]=-ODE_Solver_info.Sguessptr->dy[i]; 

            //---------------------------------------------------------------------------
            // convert the boundary conditions of the PDE
            //---------------------------------------------------------------------------
            if(ODE_Solver_info.nPDE>2)
            {
                ODE_Solver_info.F[0]=-ODE_Solver_info.Sguessptr->dy[0];
                int ui=ODE_Solver_info.index_ODE-1;
                ODE_Solver_info.F[ui]=-ODE_Solver_info.Sguessptr->dy[ui];
            }    

            //---------------------------------------------------------------------------
            // compute Jacobian
            //---------------------------------------------------------------------------
            ODE_solver_compute_Jacobian_Matrix_PDE_ODE_OAE(zout, Dz*delta, neq,
                                                           &ODE_Solver_info.Sguessptr->y[0], 
                                                           *ODE_Solver_info.Jacobian_ptr,
                                                           ODE_Solver_info); 

            for(int it=0; it<=ODE_Solver_info.max_it; it++)
            {
                //-----------------------------------------------------------------------
                // solve equation for dY
                //-----------------------------------------------------------------------
                ODE_Solver_Solve_LA_system_PDE_ODE_OAE(*ODE_Solver_info.Jacobian_ptr,
                                                       ODE_Solver_info.F,
                                                       ODE_Solver_info.dY,
                                                       ODE_Solver_info.index_ODE, mess);
/*
                ODE_Solver_Solve_LA_system(*ODE_Solver_info.Jacobian_ptr,
                                           ODE_Solver_info.F,
                                           ODE_Solver_info.dY, 1.0e-8, 1);
*/ /*
                ODE_Solver_Solve_LA_system_GSL(*ODE_Solver_info.Jacobian_ptr,
                                               ODE_Solver_info.F,
                                               ODE_Solver_info.dY, 1.0e-7, 1);
*/
//                wait_f_r();
                //-----------------------------------------------------------------------
                // update current solution
                //-----------------------------------------------------------------------
#ifdef OPENMP_ACTIVATED_ODE
#pragma omp parallel for default(shared)
#endif
                for(int i=0; i<neq; i++)
                    ODE_Solver_info.Sguessptr->y[i]+=ODE_Solver_info.dY[i];

                //-----------------------------------------------------------------------
                // add correction from full matrix and solve again
                //-----------------------------------------------------------------------
                if(it<ODE_Solver_info.max_it)
                {
                    // compute delta fb(Y^(0))
                    //ODE_Solver_info.fcn_col_ptr(&neq, &zout,
                    //                            &ODE_Solver_info.Sguessptr->y[0],
                    //                            &ODE_Solver_info.F[0], -2);
                }
            }
            
            //---------------------------------------------------------------------------
            // check convergence of iteration
            //---------------------------------------------------------------------------
            converged=1;
            
            for(int k=0; k<neq; k++) 
                if( ODE_Solver_info.rel_vector[k]!=0.0 
                   && (fabs(ODE_Solver_info.dY[k])>=
                       1.15*max(ODE_Solver_info.rel_vector[k]*fabs(ODE_Solver_info.Sguessptr->y[k]), 
                           ODE_Solver_info.abs_vector[k]) 
                       ) 
                   )
                { 
                    converged=0; 
                    if(mess>=2) 
                        cout << " " << k << " " << fabs(ODE_Solver_info.dY[k]) << " " 
                             << ODE_Solver_info.rel_vector[k]
                                        *fabs(ODE_Solver_info.Sguessptr->y[k]) << " " 
                             << ODE_Solver_info.abs_vector[k] << " " 
                             << ODE_Solver_info.order << endl;
                    
                    break; 
                }
            
            if(Jac_loops>=Jac_loops_max)
            {
                converged=1;
                if(mess>=1) cout << " Reach maximal number of iterations ( "
                                 << Jac_loops_max << " )" << endl;
            }
            
            if(mess>=1) cout << "\033[2K" << " zout= " << zout 
                             << " it= " << Jac_loops << endl << "\033[1A";
        } 
        while(converged==0);
        
        if(mess>=1) cout << " one step done " << zin-zout << " " << zin << " " << zout << endl;
        
        if(mess>=1) cout << "\n Number of Jacobian iterations " << Jac_loops 
                         << " needed for propagation from z= " << zin 
                         << " by Dz= " << zin-zout 
                         << " or Dz/z= " << zin/zout-1.0 << endl;
        
        //===============================================================================
        // swap variables, so that Snew contains new solution
        //===============================================================================
        ODE_Solver_info.Sptr=Snew;
        Snew=ODE_Solver_info.Sguessptr;
        ODE_Solver_info.Sguessptr=ODE_Solver_info.Sptr;
        ODE_Solver_info.Sptr=NULL;
        
        return 0;
    }

    //==============================================================================================
    // This routine has can only be called after the setup was done 
    // (ODE_Solver_set_up_solution_and_memory)
    //
    // After the cal ODE_solver_Solution Sz will contain the solution at time zend; initially Sz 
    // should contain the solution at zs. Previous solutions will also be stored by the routine.
    //
    // Furthermore, this routine allows to treat PDE + ODE + OAE systems. The PDE is assumed to come 
    // first while the next nODE elements are the ODEs followed by OAE ordinary equations. The 
    // boundary conditions are assumed to be given as algebraic equations in the setup routines.  
    // This is the assumes structure: bc, 1, 2, ..., nPDE-3, nPDE-2, bc, nODE, nOAE. 
    //
    //==============================================================================================
    // Apr 12th, 2019: overshooting allowed [BB & JC]
    // Oct 15th, 2013: added adaptive step-size control; Dz_in < Dz < Dz_max will be ensured;
    // to use this option make sure that NOT all relative errors are == 0
    // if this option is not required just set Dz_in==Dz_max (error control will have no effect)
    //==============================================================================================
    int ODE_Solver_Solve_history_PDE_ODE_OAE_run(double zs, double zend, double zcrit,
                                                 double Dz_in, double Dz_max,
                                                 ODE_Solver_data &ODE_Solver_info,
                                                 int nODE, int nOAE, int setmaxorder)
    {   
        //===========================================================================
        // setup
        //===========================================================================
        double zout=zs, zin=zs;
        double Dz_z=max(Dz_in/zs, ODE_Solver_info.Dz_z_last), Dz_z_max;
        int LAerror=0;
        int maxorder=setmaxorder;
        int messflag=verbosity_ODE_solver, nsteps=0;
        
        //===========================================================================
        // set initial step-size
        //===========================================================================
        if(zs!=0.0) Dz_z_max=fabs((zcrit-zs)/zs);
        else Dz_z_max=fabs(zcrit-zs);
        
        if(Dz_max!=0.0 && zs!=0.0) Dz_z_max=Dz_max/zs;
        
        if(Dz_z==0.0) cerr << " please give non-zero step-size" << endl;

        Dz_z=min(Dz_z, Dz_z_max);
        
        //===========================================================================
        // make sure that max order is not set larger
        //===========================================================================
        ODE_Solver_info.order=(int)min(ODE_Solver_info.order, maxorder);
        
        do{ 
            if(ODE_Solver_info.direction==1) 
            {
                if(zin<0.0) zout=max(zin*(1.0+Dz_z), zcrit);
                else if(zin>ODE_Solver_info.tolSol) zout=max(zin*(1.0-Dz_z), zcrit);
                else zout=max(-ODE_Solver_info.tolSol, zcrit);
            }
            else
            {
                if(zin<-ODE_Solver_info.tolSol) zout=min(zin*(1.0-Dz_z), zcrit);
                else if(zin>0.0) zout=min(zin*(1.0+Dz_z), zcrit);
                else zout=min(ODE_Solver_info.tolSol, zcrit);
            }
            // avoid taking tiny last step [JC Aug 2018]
            if(abs(zcrit-zout)<=Dz_in*1.0e-2) zout=zcrit;
            
            LAerror=ODE_Solver_compute_new_Solution_PDE_ODE_OAE(ODE_Solver_info.order, zout,
                                                                ODE_Solver_info.tolSol, 
                                                                ODE_Solver_info, 
                                                                ODE_Solver_info.Snewptr, 
                                                                ODE_Solver_info.Snptr);
            
            nsteps++;                  // count steps/attempts
            Dz_z=fabs(zout/zin-1.0);   // save actual Dz_z

            if(LAerror==1)
            {
                LAerror=0;
                Dz_z/=2.0;
                Dz_z=max(Dz_z, 1.0e-10);
                zout=zin; 
            }
            else if(LAerror==0)
            {
                //--------------------------------------------------------
                // estimate error and next possible stepsize
                //--------------------------------------------------------
                ODE_Solver_estimate_error_and_next_possible_step_size(ODE_Solver_info.order,
                                                                      zin, zout, zcrit, Dz_z,
                                                                      Dz_z_max, ODE_Solver_info,
                                                                      *ODE_Solver_info.Snewptr,
                                                                      ODE_Solver_info.Snptr,
                                                                      messflag);

                Dz_z=max(Dz_z, Dz_in/zout);
                
                //--------------------------------------------------------
                // provide some feedback
                //--------------------------------------------------------
                if(verbosity_ODE_solver>1)
                {
                    cout << " Last attempted Dz_z = " << Dz_z << endl;
                    cout << " Maximal Dz_z = " << fabs(Dz_max/zin) << endl;
                    cout << " Minimal Dz_z = " << fabs(Dz_in/zin) << endl;
                }

                //--------------------------------------------------------
                // accepting the current step
                //--------------------------------------------------------
                ODE_Solver_info.Sptr=ODE_Solver_info.Snptr[5];
                for(int k=5; k>0; k--) ODE_Solver_info.Snptr[k]=ODE_Solver_info.Snptr[k-1];
                ODE_Solver_info.Snptr[0]=ODE_Solver_info.Snewptr;
                ODE_Solver_info.Snewptr=ODE_Solver_info.Sptr;
                ODE_Solver_info.Sptr=NULL;
                ODE_Solver_info.count++;

                if(ODE_Solver_info.count>=ODE_Solver_info.order+1)
                { 
                    ODE_Solver_info.order=(int)min(ODE_Solver_info.order+1, maxorder); 
                    ODE_Solver_info.count=1; 
                }
                //
                zin=zout;
            }
            else{ cout << " Unknown error in LA-solver accurred: " << LAerror << endl; exit(1); }
        }       
        while(ODE_Solver_info.direction*zout>ODE_Solver_info.direction*zend);

        //===========================================================================
        // save last step-size
        //===========================================================================
        ODE_Solver_info.Dz_z_last=Dz_z;
        
        if(verbosity_ODE_solver>1)
        {
            cout << " Number of steps/attempts = " << nsteps << endl;
            cout << " Next starting Dz_z = " << Dz_z << endl;
            cout << " maximal Dz_z = " << Dz_max << endl;
        }

        return 0;
    }
    
    
    //==============================================================================================
    // driver routine with overshooting up to z==zcrit
    //==============================================================================================
    int ODE_Solver_Solve_history_PDE_ODE_OAE(double zs, double zend, double zcrit,
                                             double Dz_in, double Dz_max,
                                             ODE_solver_Solution &Sz,
                                             ODE_Solver_data &ODE_Solver_info,
                                             int nODE, int nOAE, int setmaxorder)
    {
        if(ODE_Solver_info.Snewptr==NULL)
            throw_error("ODE_Solver_Solve_history_PDE_ODE_OAE",
                        "please set up the memory first! Exiting.", 1);

        if(verbosity_ODE_solver>0) cout << " computing solution " << endl;

        //===========================================================================
        // storing number of PDEs
        //===========================================================================
        int neq=Sz.y.size();
        set_equation_indices(neq-nODE-nOAE, nODE, nOAE, ODE_Solver_info);

        //===========================================================================
        // define integration direction
        //===========================================================================
        double zlast=ODE_Solver_info.Snptr[0]->z;
        if(zs>zend) ODE_Solver_info.direction=1;
        else ODE_Solver_info.direction=-1;

        if(ODE_Solver_info.direction*zs<ODE_Solver_info.direction*zlast)
            throw_error("ODE_Solver_Solve_history_PDE_ODE_OAE",
                        "this should never happen", 2);
        
        //===========================================================================
        // check if run is needed [if order == 1 --> only one solution known]
        //===========================================================================
        if(ODE_Solver_info.direction*zend<ODE_Solver_info.direction*zlast ||
           ODE_Solver_info.order==1)
        {
            if(verbosity_ODE_solver>0) cout << " calling solver " << endl;

            ODE_Solver_Solve_history_PDE_ODE_OAE_run(zlast, zend, zcrit,
                                                     Dz_in, Dz_max,
                                                     ODE_Solver_info,
                                                     nODE, nOAE, setmaxorder);
        }

        //===========================================================================
        // interpolate or just copy solution
        //===========================================================================
        if(zend==ODE_Solver_info.Snptr[0]->z)
        {
            if(verbosity_ODE_solver>0) cout << " Copying solution " << endl;

            Sz.z=zend;
            Sz.y =ODE_Solver_info.Snptr[0]->y;
            Sz.dy=ODE_Solver_info.Snptr[0]->dy;
        }
        else
        {
            if(verbosity_ODE_solver>0) cout << " Interpolating solution " << endl;

            ODE_Solver_Interpolate_Solution(zend, ODE_Solver_info, Sz);
        }

        return 0;
    }

    //==============================================================================================
    // driver routine without overshooting
    //==============================================================================================
    int ODE_Solver_Solve_history_PDE_ODE_OAE(double zs, double zend,
                                             double Dz_in, double Dz_max,
                                             ODE_solver_Solution &Sz,
                                             ODE_Solver_data &ODE_Solver_info,
                                             int nODE, int nOAE, int setmaxorder)
    {
        // call overshooting routine with zcrit == zend
        return ODE_Solver_Solve_history_PDE_ODE_OAE(zs, zend, zend, Dz_in, Dz_max,
                                                    Sz, ODE_Solver_info,
                                                    nODE, nOAE, setmaxorder);
    }

    //==============================================================================================
    // to obtain interpolated solution between zs and ze. The solver should have been run before
    // and the solution at ze should be available already.
    //==============================================================================================
    int ODE_Solver_Interpolate_Solution(double z,
                                        ODE_Solver_data &ODE_Solver_info,
                                        ODE_solver_Solution &Sn)
    {
        double zend=ODE_Solver_info.Snptr[0]->z;
        double zs  =ODE_Solver_info.Snptr[ODE_Solver_info.order-1]->z;

        if(!(ODE_Solver_info.direction*zs>=ODE_Solver_info.direction*z &&
             ODE_Solver_info.direction*z>=ODE_Solver_info.direction*zend))
            
            throw_error("ODE_Solver_Interpolate_Solution", "not for extrapolation!", 1);

        Sn.z=z;
        ODE_Solver_interpolate(ODE_Solver_info.order, ODE_Solver_info.Snptr, Sn);
        
        //-----------------------------------------------------------------------
        // call ODE setup on interpolated solution to get dy instead of interpol
        //-----------------------------------------------------------------------
        //ODE_solver_f(neq, zend, &Sz.y[0], &Sz.dy[0], ODE_Solver_info);
        
        if(verbosity_ODE_solver>1)
        {
            cout << " zs= " << zs << " z= " << z << " zend= " << zend << endl;
            if(z<1.0e+4) wait_f_r();
        }
        
        return 0;
    }
}
