//==================================================================================================
// Author: Andrea Ravenni
// first implementation: Sep 2019
// last modification: Sep 2019
//==================================================================================================

#include "CUBA_wrapper.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include "Bremsstrahlung.h"
#include "Definitions.h"

#define CUBA_NUMCORES 1
#define CUBA_POINTS_PER_CORE 20000

bool use_cuhre=1, use_vegas=0, use_suave=0;

using namespace std;

//==================================================================================================
// Utilities
//==================================================================================================
void printfineline()
{
    std::cout << "----------------------------------------------------------------------\n";
}

//---------------------------------------------------------------------------------------
void printthickline()
{
    std::cout << "\n======================================================================\n";
}


//==================================================================================================
// CUBA domain splitter
//==================================================================================================
bool split_domain_3(int reg, double delreg, double &Jacobian, double &mu1, double &mu2, 
                    const double xx[])
{    
    double mu1max, mu1min;
    double mu2max, mu2min;
    
    switch(reg)
    {
        case(1):
            mu1max = 1.;
            mu1min = 1.-delreg;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = 1.;
            mu2min = mu1-delreg;            
            break;
            
        case(2):
            mu1max = 1.-delreg;
            mu1min = 1.-2.*delreg;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = 1.;
            mu2min = 1.-delreg;  
            break;
            
        case(3):
            mu1max = 1.-delreg;
            mu1min = -1.;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = min(mu1+delreg,1.-delreg);
            mu2min = max(mu1-delreg,-1.);    
            break;
            
        case(4):
            mu1max = 1.-2.*delreg;
            mu1min = -1.;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = 1.;
            mu2min = 1.-delreg;
            
            break;
            
        case(5):
            mu1max = 1.;
            mu1min = 1.-delreg;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = mu1-delreg;
            mu2min = -1.; 
            break;
            
        case(6):
            mu1max = 1.-2.*delreg;
            mu1min = -1.;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = 1.-delreg;
            mu2min = mu1+delreg;
            break;
            
        case(7):
            mu1max = 1.-delreg;
            mu1min = -1.+delreg;
            
            mu1= (mu1max-mu1min)*xx[2] +mu1min;
            
            mu2max = mu1-delreg;
            mu2min = -1.;
            break;
        
        default:
            return true;
    }

    mu2= (mu2max-mu2min)*xx[1] +mu2min;
    Jacobian *= ((mu1max-mu1min)*(mu2max-mu2min));
    return false;
}


//==================================================================================================
// CUBA integrand xi variables
//==================================================================================================
static int dsigma_dL_xi(const int *ndim, const cubareal xx[],
                        const int *ncomp, cubareal ff[], void *params)
{
    double integrand;
    double Jacobian;
    
    Integration_variables *IV=((Integration_variables *) params);
    
    if(*ndim==3)
    {        
    // Initialize mu1
        IV->V.mu1= 2.*xx[0]-1.;
        Jacobian= 2.;
      
    // Initialize Delta mu
        double negligiblestart= IV->Get_Precision_info().epsrel/10.0*IV->V.w;
        double lDmu_max;
        double lDmu_min= log(negligiblestart);
        
        if((IV->cbreg) %2)
        { // p regions
            if(1.0-IV->V.mu1 <= negligiblestart)
            {
                ff[0] = 0.;
                return 0;
            }
            else lDmu_max= log(1.0-IV->V.mu1);
        }
        else
        { // m regions
            if(1.0+IV->V.mu1 <= negligiblestart)
            {
                ff[0] = 0.;
                return 0;
            }
            else lDmu_max= log(1.0+IV->V.mu1);
        }
        
        double Dmu21=exp((lDmu_max-lDmu_min)*xx[1]+lDmu_min);
        
    // Initialize xi
        IV->V.phi=-10.0;
        
        if(IV->cbreg %2)
        { // p regions
            IV->V.Dmu21= Dmu21;
            IV->V.mu2= IV->V.mu1+Dmu21;
        }
        else
        { // m regions
            IV->V.Dmu21= -Dmu21;
            IV->V.mu2= IV->V.mu1-Dmu21;
        }
        
        IV->V.compute_derived_values();
            
        double lDxi_MIN= log(negligiblestart);
        double lDxi_MAX;
        if(IV->V.Dxitot <= negligiblestart)
            {
                ff[0] = 0.;
                return 0;
            }
        else lDxi_MAX= log(IV->V.Dxitot);
        
        double lDxi_min, lDxi_max;
        
        double firstsplit = 0.3;
        double secondsplit= 0.9; // ! HAS TO BE LARGER THAN firstsplit !
        
        switch(IV->cbreg){
            case 5: // the weird ordering follows the importance in term of integral size
            case 6:
                lDxi_min= lDxi_MIN;
                lDxi_max= lDxi_MIN*(1.-firstsplit) + lDxi_MAX*firstsplit;
                break;
                
            case 1:
            case 2:
                lDxi_min= lDxi_MIN*(1.-firstsplit) + lDxi_MAX*firstsplit;
                lDxi_max= lDxi_MIN*(1.-secondsplit) + lDxi_MAX*secondsplit;
                break;
                
            case 3:
            case 4:
                lDxi_min= lDxi_MIN*(1.-secondsplit) + lDxi_MAX*secondsplit;
                lDxi_max= lDxi_MAX;
                break;
        }
        
        
        IV->V.Dxi= exp((lDxi_max-lDxi_min)*xx[2]+lDxi_min);
        
        IV->V.compute_derived_values();
        
        Jacobian *= Dmu21*(lDmu_max-lDmu_min)*
                    IV->V.Dxi*(lDxi_max-lDxi_min);
    }
    else
    {
        cout << "integral slices not yet supported with CUBA, stay tuned\n";
        exit(EXIT_FAILURE);
    }
    
    integrand = IV->dsigma(IV->V, IV->Z);
    
    //if(integrand < 0 || integrand!=integrand)
    if(integrand!=integrand)
    {
        std::cerr << "ERROR occurred ...\n";
        return -999;//value used to abort integration in cuba
    }

    ff[0] = integrand*Jacobian;
    
    return 0;
}


//==================================================================================================
// CUBA integrand normal variables
//==================================================================================================
static int dsigma_dL_std(const int* ndim, const cubareal xx[], const int *ncomp, 
                        cubareal ff[], void *params)
{
    double integrand;
    Integration_variables *IV=((Integration_variables *) params);
    
    double Jacobian;
    
    IV->V.phi= TWOPI*xx[0];
    Jacobian = TWOPI;
    
    double tune = 1.e-8;
//     double delreg = max(min(0.1, tune*IV->V.omega/IV->V.E1), 1.e-18);
    double delreg = min(0.1, tune*max(0.01, IV->V.omega/IV->V.E1));
    
    if(*ndim==3)
    {        
        if(split_domain_3(IV->cbreg, delreg, Jacobian, IV->V.mu1, IV->V.mu2, xx))
        {
            printthickline();
            cout << "ERROR: error occurred in split_domain_3\n";
            printthickline();
            return -999;//value used to abort integration in cuba
        }
    }
    else if(*ndim==2)
    {
        double mu2max, mu2min;
        switch(IV->cbreg){
            case 1:
                mu2max = min(IV->V.mu1+.05, 1.);
                mu2min = max(IV->V.mu1-.05, -1.);
                break;
                
            case 2:
                mu2max = 1.;
                mu2min = min(IV->V.mu1+.05, 1.);
                break;
            
            case 3:
                mu2max = max(IV->V.mu1-.05, -1.);
                mu2min = -1.;
                break;
        }
        
        IV->V.mu2= (mu2max-mu2min)*xx[1] +mu2min;
        Jacobian *= (mu2max-mu2min);
    }
    
    integrand = IV->dsigma(IV->V, IV->Z);
    
    //if(integrand < 0 || integrand!=integrand)
    if(integrand!=integrand)
    {
        std::cerr << "ERROR occurred ...\n";
        return -999;//value used to abort integration in cuba
    }

    ff[0] = integrand*Jacobian;
    
    return 0;
}


//==================================================================================================
// CUBA integrator
//==================================================================================================
double cubaint(int ndim, double epsrel, double epsabs, void *params, string mode)
//ndim select the number of dimension of the integral
//p is a pointer to the integral parameters
//I see ndim being used to select what to leave differential
{
//Set up CUBA
    cubacores(CUBA_NUMCORES, CUBA_POINTS_PER_CORE);
    
//reinterpretcast variables to get precision
    Integration_variables *IV=((Integration_variables *) params);
    
//Extra CUBA-specific variables
    int ncomp = 1;//number of components of the integrand. 1 since it's a scalar
    double integral[1], err[1];
    double prob[1];//probability of getting wrong result
    int fail=0;//if CUBA incounters and error or exceeds the maximum number of evaluations
    
    int nregions, neval;//CUBA statistics

    int nvec = 1;//number of points to be evaluated in a single go by the integrand function.
                 //Used to vectorize the code. Not used at the moment, we will see if it might help
    int verbose = 0; //[QUESTION] Do you have a verbosity level to set? [TODO] Introduce a verbosity level
    int seed = 0;
    int flags = 0;//too complicate, read CUBA documentation. I think 0 is fine in most cases.
    int mineval = 0;//minimum and maximum amount of evaluations
    int maxeval = 100000000;
    int key = 0;//cuhre cubature rule set to default.
    int nstart = 1000;
    int nincrease = 500;
    int nbatch = 1000;
    int gridno = 0;
    int nnew = 1000;
    int nmin = 10;
    double flatness = 10.;
    char statefile[] = "";//don't store internal state
    void *spin = NULL;//CUBA takes the trash out like a big boy.

    static int (*dsigma_dL)(const int *ndim, const cubareal xx[],
                     const int *ncomp, cubareal ff[], void *params);
    
    if(mode=="std") dsigma_dL=dsigma_dL_std;
    else if(mode=="xi") dsigma_dL=dsigma_dL_xi;
    else
    {
        cout<<"ERROR: mode not accepted\n";
        exit(EXIT_FAILURE);
    }
    
    if(use_cuhre)
        Cuhre(ndim, ncomp, dsigma_dL, params, nvec,
              epsrel, epsabs, verbose | flags,
              mineval, maxeval, key,
              statefile, spin,
              &nregions, &neval, &fail, integral, err, prob);
    else if(use_vegas)
        Vegas(ndim, ncomp, dsigma_dL, params, nvec,
              epsrel, epsabs, verbose | flags, seed,
              mineval, maxeval, nstart, nincrease, nbatch, gridno,
              statefile, spin, &neval, &fail, integral, err, prob);
    else if(use_suave)
        Suave(ndim, ncomp, dsigma_dL, params, nvec,
              epsrel, epsabs, verbose | flags, seed,
              mineval, maxeval, nnew, nmin, flatness,
              statefile, spin,
              &nregions, &neval, &fail, integral, err, prob);
    else
    {
        printthickline();
        cout << "ERROR: The code is not supposed to reach this point\n";
        printthickline();
    }
        
    return integral[0];
}


//==================================================================================================
// CUBA specific functions
//==================================================================================================
double dsigma_dk_cuba(void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P = IV->Get_Precision_info();

    double epsrel = relative_precision_estimate(P.epsrel, IV->V, IV->Z);
    double epsabs = absolute_precision_estimate(  epsrel, IV->V, IV->Z)*2.0*FOURPI;

    double r =0.;

    for(int j=1; j<=7; j++)
    {
        IV->cbreg=j;
        void *params=IV;
        r += cubaint(3, epsrel, epsabs, params, "std");
        epsabs= max(P.epsabs, abs(r*epsrel/5.0));
    }
    return r;
}

//--------------------------------------------------------------------------------------------------
double dsigma_dkdmu1_cuba(double mu1, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P = IV->Get_Precision_info();
    IV->V.mu1=mu1;
    
    void *params=IV;
    return cubaint(2, P.epsrel, P.epsabs, params, "std");
}

//--------------------------------------------------------------------------------------------------
double dsigma_dkdOmega1dmu2_cuba(double mu2, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P = IV->Get_Precision_info();
    IV->V.mu2=mu2;
    
    void *params=IV;
    return cubaint(1, P.epsrel, P.epsabs, params, "std");
}

//--------------------------------------------------------------------------------------------------
double dsigma_dk_Dxi_I0_cuba(void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P = IV->Get_Precision_info();
    
    double epsrel = relative_precision_estimate(P.epsrel, IV->V, IV->Z);
    double epsabs = absolute_precision_estimate(  epsrel, IV->V, IV->Z)*2.0*FOURPI;

    double r= 0.;
    
    for(int j=1; j<=6; j++)
    {
        IV->cbreg= j;
        void *params=IV;
        r += cubaint(3, epsrel, epsabs, params, "xi");
        epsabs= max(P.epsabs, abs(r*epsrel/5.0));
    }
    
    return TWOPI*r;
}
