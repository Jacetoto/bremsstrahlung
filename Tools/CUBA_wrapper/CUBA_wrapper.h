//==================================================================================================
// Author: Andrea Ravenni
// first implementation: Sep 2019
// last modification: Sep 2019
//==================================================================================================

#ifndef CUBA_WRAPPER_H
#define CUBA_WRAPPER_H

#if REALSIZE == 16
#include "cubaq.h"
#elif REALSIZE == 10
#include "cubal.h"
#else
#include "cuba.h"
#endif

#include <iostream>
#include <string>

using namespace std;

//==================================================================================================
// integral wrappers for CUBA
//==================================================================================================
double dsigma_dk_cuba(void *p);
double dsigma_dkdmu1_cuba(double mu1, void *p);
double dsigma_dkdOmega1dmu2_cuba(double mu2, void *p);

double dsigma_dk_Dxi_I0_cuba(void *p);

#endif

//==================================================================================================
//==================================================================================================
