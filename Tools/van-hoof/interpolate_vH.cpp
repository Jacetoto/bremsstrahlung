/*
Copyright (c) 2014, Peter A.M. van Hoof.

This program is provided 'as-is', without any express or implied warranty. In
no event will the author be held liable for any damages arising from the use
of this program.

Permission is granted to anyone to use this program for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject
to the following restrictions:

1. The origin of this program must not be misrepresented; you must not claim
that you created the original program. If you use this program in a product,
an acknowledgment in the product documentation would be appreciated but
is not required.
2. Altered program versions must be plainly marked as such, and must not be
misrepresented as being the original program.
3. This notice may not be removed or altered from any further distribution.

Peter A.M. van Hoof
Royal Observatory of Belgium
Ringlaan 3
B-1180 Brussels
Belgium
p.vanhoof@oma.be
*/

/*
If you use any of these data in a scientific publication, please refer to

van Hoof P.A.M., Williams R.J.R., Volk K., Chatzikos M., Ferland G.J., Lykins M., Porter R.L., Wang Y.
Accurate determination of the free-free Gaunt factor, I -- non-relativistic Gaunt factors
2014, MNRAS, 444, 420
*/

#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>
#include <complex>
#include <time.h>

#include "Definitions.h"
#include "physical_consts.h"
#include "Bremsstrahlung-simple-functions.h"
#include "interpolate_vH.h"

using namespace std;
#define _LINE_LENGTH_MAX_ 10024 /**< size of the string read in each line of the file (extra characters not taken into account) */
#define _ARGUMENT_LENGTH_MAX_ 10024 /**< maximum size of each argument (name or value), including the final null character */

static const long gaunt_magic = 20140210L;
static const long gaunt_magic2 = 20140510L;

string LOC_vH_path=BREMSSTRAHLUNGPATH;

/* uncomment the following line to interpolate on the non-thermally averaged Gaunt factors */
#define USE_NONAV_GAUNT 1

#ifdef USE_NONAV_GAUNT

string gauntff_file = LOC_vH_path + "./Tools/van-hoof/gauntff_nonav.dat";

#define NP_GAM2 151
#define NP_U 276

#else

string gauntff_file = LOC_vH_path + "./Tools/van-hoof/gauntff.dat";

#define NP_GAM2 81
#define NP_U 146

#endif

double gff[NP_U][NP_GAM2];

double lg_gam2_min, lg_u_min, step, lg_gam2_max, lg_u_max;

size_t min(size_t x, size_t y)
{
    return ( x < y ) ? x : y;
}

size_t max(size_t x, size_t y)
{
    return ( x > y ) ? x : y;
}

void read_table(const char* fnam)
{
    char* ptr;
    long magic;
    size_t np_gam2, np_u, i, ipu, ipg2, len;
    FILE* io = fopen( fnam, "r" );
    if( io == NULL )
    {
        fprintf( stderr, "failed to open file %s\n", fnam );
        exit(1);
    }
    ptr = NULL;
    /* skip copyright statement */
    while( 1 )
    {
        getline( &ptr, &len, io );
        if( ptr[0] != '#' )
            break;
        free( ptr );
        ptr = NULL;
    }
    /* read magic number */
    sscanf( ptr, "%ld", &magic );
    free( ptr );
    ptr = NULL;
    if( magic != gaunt_magic && magic != gaunt_magic2 )
    {
        fprintf( stderr, "read_table() found wrong magic number in file %s.\n", fnam );
        exit(1);
    }
    /* read dimensions of the table */
    getline( &ptr, &len, io );
    sscanf( ptr, "%ld %ld",  &np_gam2, &np_u );
    free( ptr );
    ptr = NULL;
    assert( np_gam2 == NP_GAM2 && np_u == NP_U );
    /* read start value for log(gamma^2) */
    getline( &ptr, &len, io );
    sscanf( ptr, "%le",  &lg_gam2_min );
    free( ptr );
    ptr = NULL;
    /* read start value for log(u) */
    getline( &ptr, &len, io );
    sscanf( ptr, "%le",  &lg_u_min );
    free( ptr );
    ptr = NULL;
    /* read step size in dex */
    getline( &ptr, &len, io );
    sscanf( ptr, "%le",  &step );
    free( ptr );
    ptr = NULL;
    lg_gam2_max = lg_gam2_min + (double)(np_gam2-1)*step;
    lg_u_max = lg_u_min + (double)(np_u-1)*step;
    /* read atomic number when present */
    if( magic == gaunt_magic2 )
    {
        getline( &ptr, &len, io );
        free( ptr );
        ptr = NULL;
    }
    
    /* next lines are comments */
    for( i=0; i < 9; ++i )
    {
        getline( &ptr, &len, io );
        free( ptr );
        ptr = NULL;
    }
    
    for( ipu=0; ipu < np_u; ++ipu )
    {
        for( ipg2=0; ipg2 < np_gam2; ++ipg2 )
            fscanf( io, "%le", &gff[ipu][ipg2] );
    }
    
    /* the remainder of the file may contain the uncertainties of the gff values
     * we will not read those here... */
    
    fclose(io);
}

double lagrange(const double x[], /* x[n] */
                const double y[], /* y[n] */
                long n,
                double xval)
{
    long i, j;
    double yval = 0.;
    
    for( i=0; i < n; i++ )
    {
        double l = 1.;
        for( j=0; j < n; j++ )
        {
            if( i != j )
                l *= (xval-x[j])/(x[i]-x[j]);
        }
        yval += y[i]*l;
    }
    return yval;
}

double gauntff(double gam2, double u)
{
    /* use 3rd-order interpolation */
    static const size_t NINTERP = 4;
    
    size_t i;
    long ipg2, ipu;
    double lg_gam2, lg_u;
    double x[NINTERP], interp[NINTERP];
    
    assert( gam2 > 0. && u > 0. );
    
    lg_gam2 = log10(gam2);
    lg_u = log10(u);
    
    assert( lg_gam2_min <= lg_gam2 && lg_gam2 <= lg_gam2_max );
    assert( lg_u_min <= lg_u && lg_u <= lg_u_max );
    
    ipg2 = min(max((size_t)((lg_gam2-lg_gam2_min)/step),1)-1,NP_GAM2-NINTERP);
    ipu = min(max((size_t)((lg_u-lg_u_min)/step),1)-1,NP_U-NINTERP);
    
    for( i=0; i < NINTERP; ++i )
        x[i] = lg_gam2_min + (double)(ipg2+i)*step;
    
    for( i=0; i < NINTERP; ++i )
        interp[i] = lagrange(x, &gff[ipu+i][ipg2], NINTERP, lg_gam2);
    
    for( i=0; i < NINTERP; ++i )
        x[i] = lg_u_min + (double)(ipu+i)*step;
    
    return lagrange(x, interp, NINTERP, lg_u);
}

//==================================================================================================
// interpolation function vH
//==================================================================================================
bool vH_table_is_read=0;

// emission cross section
double Gaunt_factor_vH(double p1, double omega, int Z)
{
    if(!vH_table_is_read){ read_table( gauntff_file.c_str() ); vH_table_is_read=1; }
    
    //#################################################################################
    // TODO: add Z dependence and use constants from "physical_consts.h" for conversion
    //#################################################################################
    double KB_eV_per_Kelvin= 8.617333262145e-5; //in eV⋅K−1.
    double Z2Ry_over_mec2 = KB_eV_per_Kelvin*1.579e5/0.511e6;
    double epsilon_1 =  Emax_func(p1)/Z2Ry_over_mec2;
    double w_vH=omega/Z2Ry_over_mec2;
    double eps_i = epsilon_1 - w_vH;

    if(eps_i <= 0) return 0.0;
    
    double gaunt_vh = gauntff(eps_i, w_vH);

    return gaunt_vh;
}

//==================================================================================================
//==================================================================================================
