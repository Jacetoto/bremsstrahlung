//==================================================================================================
//
// This code returns the thermally-averaged Gaunt-factor functions
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Oct 2019
// last modification   : Oct 2019
//
//==================================================================================================
#ifndef GAUNT_FACTOR_TH_INTERPOL_H
#define GAUNT_FACTOR_TH_INTERPOL_H

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

double Gaunt_th_interpolated(double The, double xe, int Z);

#endif

//==================================================================================================
//==================================================================================================
