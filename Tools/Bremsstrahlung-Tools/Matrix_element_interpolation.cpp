//==================================================================================================
//
// Log pre-computed matrix elements A2, W2 and Im[V*W]-|W|^2 assuming w<<1
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// required libs
//==================================================================================================
#include "physical_consts.h"
#include "routines.h"
#include "parser.h"
#include "Bremsstrahlung.h"
#include "Bremsstrahlung-simple-functions.h"
#include "Matrix_element_interpolation.h"

string path_Mdatabase=string(BREMSSTRAHLUNGPATH)+"./Database/";

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// structures for Matrix Element storage
//==================================================================================================
struct Matrix_Element_table
{
    string label, filename;
    vector<double> p1arr;
    vector<double> xiarr;
    vector<vector<double> > Yarr;
    
    // information to accelerate interpolation using spline
    // when p1 is reset a 1D table in xi is created and then interpolated using spline
    double lp1;
    int spline_memindex;
    vector<double> Gxi;
    
    // information to accelerate interpolation
    unsigned long search_ind_p1;
    unsigned long search_ind_xi;
    
    Matrix_Element_table()
    {
        lp1=0.0;
        spline_memindex=-100;
        search_ind_p1=search_ind_xi=0;
        label=filename=" ";
    }
};

//==================================================================================================
// read Matrix Element table
//==================================================================================================
void read_matrix_element_table(string fname, Matrix_Element_table &M, bool logy=1)
{
    file_content Gfc;
    parser_read_file(fname, Gfc);
    double val;
    vector<double> Mvals;
    
    istringstream iss(Gfc.lines[0]);
    
    //cout << "\n read_matrix_element_table:: loading file " << fname << endl;
    
    //----------------------------------------------------------------------------------------------
    // p-values
    //----------------------------------------------------------------------------------------------
    do {
        iss >> val;
        M.xiarr.push_back(log(val)); // immediately store as log
    }
    while(iss);
    
    M.xiarr.pop_back(); // remove last double entry
    
    //----------------------------------------------------------------------------------------------
    // remaining entries p1, M1, M2, M3, ... MN
    //----------------------------------------------------------------------------------------------
    for(int k=1; k<(int)Gfc.lines.size(); k++)
    {
        istringstream iss_I(Gfc.lines[k]);

        iss_I >> val;
        M.p1arr.push_back(log(val)); // immediately store as log
        
        Mvals.clear();
        for(int l=0; l<(int)M.xiarr.size(); l++)
        {
            iss_I >> val;
            if(val<=0.0 && logy)
                throw_error("read_matrix_element_table", "Matrix element negative...", 1);
            if(logy) Mvals.push_back(log(val)); // immediately store as log
            else Mvals.push_back(val); // immediately store as log
        }
        
        //----------------------------------------------------------------------------------------------
        // copy to structure flipping p1 and xi
        //----------------------------------------------------------------------------------------------
        if(M.Yarr.size()==0) M.Yarr.resize(Mvals.size()); // lenght of xi array

        for(int ix=0; ix<(int) M.Yarr.size(); ix++)
            M.Yarr[ix].push_back(Mvals[ix]);
    }

    //----------------------------------------------------------------------------------------------
    // tidy up
    //----------------------------------------------------------------------------------------------
    parser_free(Gfc);
}

//==================================================================================================
// interpolation function for all cases
//==================================================================================================
double compute_Matrix_element_interpolated(double xi, double p1,
                                           Matrix_Element_table &MT, bool logy=1)
{
    //----------------------------------------------------------------------------------------------
    // treat simple cases
    //----------------------------------------------------------------------------------------------
    double logp1=log(p1);
    double logxi=log(xi);

    // error when p1 or xi too small or too large
    if(logp1<MT.p1arr[0] || logp1>MT.p1arr.back() || logxi<MT.xiarr[0] || logxi>MT.xiarr.back())
    {
        cout << " p1-val= " << p1 << " xi-val= " << xi << endl;
        throw_error("compute_Matrix_element_interpolated", "table needs to be extended", 1);
    }
    
    //----------------------------------------------------------------------------------------------
    // do interpolation
    //----------------------------------------------------------------------------------------------
    int nxi=MT.xiarr.size();
    int np1=MT.p1arr.size();
    int nint=4;
    
    // locate closes w and p1 indices
    locate_JC(&MT.xiarr[0], nxi, logxi, &MT.search_ind_xi);
    locate_JC(&MT.p1arr[0], np1, logp1, &MT.search_ind_p1);
    
    // avoid boundary extrapolation
    if(MT.search_ind_xi+nint-1>=nxi) MT.search_ind_xi-=MT.search_ind_xi+nint-nxi;
    if(MT.search_ind_p1+nint-1>=np1) MT.search_ind_p1-=MT.search_ind_p1+nint-np1;
    
    // interpolate along p1 direction first
    double y, dy;
    vector<double> Gxi(nint);
    int p1_index=MT.search_ind_p1;

    for(int s=0; s<nint; s++)
    {
        int xi_index=MT.search_ind_xi+s;
        
        polint_JC(&MT.p1arr[p1_index], &MT.Yarr[xi_index][p1_index], nint, logp1, nint-1, &Gxi[s], &dy);
    }
    
    // now do a simple interpolation along xi direction
    polint_JC(&MT.xiarr[MT.search_ind_xi], &Gxi[0], nint, logxi, nint-1, &y, &dy);
    
    return logy ? exp(y) : y;
}

//==================================================================================================
// interpolation function for all cases using polynomial interpolation in p1 and spline in xi.
// Every time p1 changes a new spline setup is computed. This reduces the number of calls to
// interpolation parts and stores all the data at constant p1.
//==================================================================================================
double compute_Matrix_element_interpolated_spline(double xi, double p1,
                                                  Matrix_Element_table &MT, bool logy=1)
{
    //----------------------------------------------------------------------------------------------
    // treat simple cases
    //----------------------------------------------------------------------------------------------
    double logp1=log(p1);
    double logxi=log(xi);
    double y, dy;

    if(MT.lp1!=logp1 || MT.spline_memindex==-100)
    {
        // error when p1 or xi too small or too large
        if(logp1<MT.p1arr[0] || logp1>MT.p1arr.back() || logxi<MT.xiarr[0] || logxi>MT.xiarr.back())
        {
            cout << " p1-val= " << p1 << " xi-val= " << xi << endl;
            throw_error("compute_Matrix_element_interpolated_spline", "table needs to be extended", 1);
        }
        
        MT.lp1=logp1;

        //----------------------------------------------------------------------------------------------
        // prepare interpolation
        //----------------------------------------------------------------------------------------------
        int nxi=MT.xiarr.size();
        int np1=MT.p1arr.size();
        int nint=4;
        
        // locate closest p1 index
        locate_JC(&MT.p1arr[0], np1, logp1, &MT.search_ind_p1);
        
        // avoid boundary extrapolation
        if(MT.search_ind_p1+nint-1>=np1) MT.search_ind_p1-=MT.search_ind_p1+nint-np1;
        
        // interpolate along p1 direction at every value of xi
        if(MT.spline_memindex==-100) MT.Gxi.resize(nxi);
        int p1i=MT.search_ind_p1;

        for(int k=0; k<nxi; k++)
            polint_JC(&MT.p1arr[p1i], &MT.Yarr[k][p1i], nint, logp1, nint-1, &MT.Gxi[k], &dy);

        if(MT.spline_memindex==-100)
            MT.spline_memindex=calc_spline_coeffies_JC(nxi, &MT.xiarr[0], &MT.Gxi[0], MT.label);
        else update_spline_coeffies_JC(MT.spline_memindex, nxi, &MT.xiarr[0], &MT.Gxi[0], MT.label);
    }

    y=calc_spline_JC(logxi, MT.spline_memindex, MT.label);
    
    return logy ? exp(y) : y;
}

//==================================================================================================
// variable mapping for varying Z
//==================================================================================================
double p1_eff(double p1, int Z)
{
    if(Z==1) return p1;
    double p1s=p1/Z;
    return p1s/sqrt( 1.0+p1s*p1s*(Z*Z-1) );
}

//==================================================================================================
Matrix_Element_table M_int_A2, M_int_W2;
Matrix_Element_table M_int_A2_hi, M_int_W2_hi;

vector<double> Get_p1_values_W2(){ return M_int_W2.p1arr; }
vector<double> Get_p1_values_A2(){ return M_int_A2.p1arr; }
vector<double> Get_p1_values_W2_hi(){ return M_int_W2_hi.p1arr; }
vector<double> Get_p1_values_A2_hi(){ return M_int_A2_hi.p1arr; }

double W2_interpolated(double xi, double p1, int Z)
{
    double ximax;
    Matrix_Element_table &Gl=M_int_W2;

    //----------------------------------------------------------------------------------------------
    // remap variables as for NR case
    //----------------------------------------------------------------------------------------------
    double p1Z=p1_eff(p1, Z);
    
    if(p1Z<5.0)
    {
        ximax=1.0e+6;
        Gl.label="Matrix-W2";
        Gl.filename="Matrix-W2-data.dat";
    }
    else if(p1Z<1.0e+3)
    {
        ximax=1.0e+3;
        Gl=M_int_W2_hi;
        Gl.label="Matrix-W2-hi";
        Gl.filename="Matrix-W2-data-highp.dat";
    }

    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.p1arr.size()==0)
        read_matrix_element_table(path_Mdatabase+Gl.filename, Gl);

    //----------------------------------------------------------------------------------------------
    // use expansion for very large xi
    //----------------------------------------------------------------------------------------------
    if(xi>=ximax) return W2_approx_high(xi, p1, Z);
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor
    //----------------------------------------------------------------------------------------------
    //double M=compute_Matrix_element_interpolated(xi, p1Z, Gl);
    double M=compute_Matrix_element_interpolated_spline(xi, p1Z, Gl);
    
    return M;
}

double A2_interpolated(double xi, double p1, int Z)
{
    double ximax;
    Matrix_Element_table &Gl=M_int_A2;

    //----------------------------------------------------------------------------------------------
    // remap variables as for NR case
    //----------------------------------------------------------------------------------------------
    double p1Z=p1_eff(p1, Z);
    
    if(p1Z<5.0)
    {
        ximax=1.0e+6;
        Gl.label="Matrix-A2";
        Gl.filename="Matrix-A2-data.dat";
    }
    else if(p1Z<1.0e+3)
    {
        ximax=1.0e+3;
        Gl=M_int_A2_hi;
        Gl.label="Matrix-A2-hi";
        Gl.filename="Matrix-A2-data-highp.dat";
    }
    
    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.p1arr.size()==0)
        read_matrix_element_table(path_Mdatabase+Gl.filename, Gl);
    
    //----------------------------------------------------------------------------------------------
    // use expansion for very large xi
    //----------------------------------------------------------------------------------------------
    if(xi>=ximax) return A2_approx_high(xi, p1, Z);
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor
    //----------------------------------------------------------------------------------------------
    //double M=compute_Matrix_element_interpolated(xi, p1Z, Gl);
    double M=compute_Matrix_element_interpolated_spline(xi, p1Z, Gl);

    return M;
}

//==================================================================================================
double W2_approx_high(double xi, double p1, int Z)
{
    double eta1=const_alpha*Z*sqrt(1.0+p1*p1)/p1;
    double F=pow(one_minus_exp_mx(TWOPI*eta1)/(TWOPI*eta1), 2);
    double phi=log(xi)-2.0*Harmonic_number_Re(eta1);
    
    return F*pow(eta1, 2)*( phi*phi*(1.0+(2.0+3.0/xi)/xi)
                           +pow(eta1, 2) * ( phi*( 2.0*(2.0+phi) + (17.0+9.0*phi)/2.0/xi )/xi )
                           +pow(eta1, 4) * ( 4.0+phi*(11.0+3.0*phi)/2.0 )/xi/xi );
}

double A2_approx_high(double xi, double p1, int Z)
{
    double eta1=const_alpha*Z*sqrt(1.0+p1*p1)/p1;
    double F=pow(one_minus_exp_mx(TWOPI*eta1)/(TWOPI*eta1), 2);
    double phi=log(xi)-2.0*Harmonic_number_Re(eta1);
    
    return F*(1.0-2.0*pow(eta1, 2)/xi *
              ( (1.0+phi)
               +             (1.0+2.0*phi*(1.0-phi))/(4.0*xi)
               +pow(eta1, 2)*(3.0-2.0*phi*(1.0+phi))/(4.0*xi) ) );
}

//==================================================================================================
//==================================================================================================
