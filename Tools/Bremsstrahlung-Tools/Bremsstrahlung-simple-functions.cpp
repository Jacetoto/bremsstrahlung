//==================================================================================================
//
// Simple functions used in Bremsstrahlung calculations
//
//==================================================================================================
//
// Author: Jens Chluba
// first implementation: Oct 2019
// last modification   : Oct 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>
#include <complex>

//==================================================================================================
// required libs
//==================================================================================================
#include "routines.h"
#include "Definitions.h"
#include "physical_consts.h"
#include "Bremsstrahlung.h"
#include "Bremsstrahlung-simple-functions.h"

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// simple functions
//==================================================================================================
double gamma_func(double p){ return sqrt(1.0+p*p); }
double Emax_func(double p)
{
    if(p<1.0e-3) return p*p/2.0*(1.0-p*p/4.0*(1.0-p*p/2.0*(1.0-5.0*p*p/8.0)));
    return sqrt(1.0+p*p)-1.0;
}

double one_minus_exp_mx(double x) // == 1 - exp(-x)
{
    //==============================================================================================
    // for small x use series expansion for 1-exp(-x)
    //==============================================================================================
    if(x<=0.001) return x*(1.0+(-0.5+(1.0/6+(-1.0/24+(1.0/120-1.0/720*x)*x)*x)*x)*x);
    else return 1.0-exp(-x);
}

//==================================================================================================
double Deta1eta2_nr(double p1, double p2, double omega, int Z)
{
    if(omega*2.0/(p1*p1)<1.0e-6)
    {
        double p12=p1*p1, xi=omega/p12;
        
        return -const_alpha*Z*xi/p1*
        (1.0+(1.5+(2.5+2.0*p12+5.0/8.0*(7.0+4.0*p12)*xi)*xi)*xi);
    }
    
    return const_alpha*Z*(1.0/p1-1.0/p2);
}

double Deta1eta2(Momenta_and_Angles &V, int Z)
{
    if(V.w<1.0e-6)
    {
        double p12=V.p1*V.p1, xi=V.omega/p12;
        
        return -const_alpha*Z*xi/V.p1*
                 (1.0+(1.5*V.gamma1+(2.5+2.0*p12+5.0/8.0*V.gamma1*(7.0+4.0*p12)*xi)*xi)*xi);
    }
    
    return const_alpha*Z*(V.gamma1/V.p1-V.gamma2/V.p2);
}

//==================================================================================================
double Dp2p1(Momenta_and_Angles &V)
{
    if(V.w<=1.0e-6)
    {
        double wp=V.omega/pow(V.p1, 2);
        return -V.gamma1*V.p1*wp*(1.0+0.5*wp/V.gamma1*(1.0+V.gamma1*wp+(0.25+pow(V.gamma1, 2))*wp*wp ) );
    }
    
    return V.p2-V.p1;
}

//==================================================================================================
double DS21_func(double mu1, double mu2, double Dmu21)
{
    double S1=sqrt(1.0-mu1*mu1);
    
    if(abs(Dmu21)<=1.0e-6 && S1>0.0)
    {
        double DmuS12=Dmu21/S1/S1;
        return -Dmu21/S1*(mu1+0.5*DmuS12*(1.0+mu1*DmuS12)
                     + (1.0+4.0*mu1*mu1+ mu1*(3.0+4.0*mu1*mu1)*DmuS12)/8.0*pow(DmuS12, 3)
                     +     (1.0+(12.0+8.0*mu1*mu1)*mu1*mu1
                     + mu1*(5.0+(20.0+8.0*mu1*mu1)*mu1*mu1)*DmuS12)/16.0*pow(DmuS12, 5) );
    }
    
    return sqrt(1.0-mu2*mu2)-S1;
}

//==================================================================================================
double Del_Daa1(double Da_a1)
{
    // Da_a1=1-a2/a2
    double Dr=-Da_a1;
    
    if(fabs(Dr)<=1.0e-6)
        return -pow(Dr, 2)/4.0*(1.0-Dr/2.0*( 1.0-5.0/8.0*Dr*(1.0-7.0/10.0*Dr) ) );

    return 2.0*sqrt(1.0+Dr)-(2.0+Dr);
}

//==================================================================================================
double Del2_Daa1(double Da_a1)
{
    // Da_a1=1-a2/a2
    double Dr=-Da_a1;
    
    if(fabs(Dr)<=1.0e-6)
        return Dr/2.0*(1.0-Dr/4.0*(1.0-Dr/2.0*( 1.0-5.0/8.0*Dr*(1.0-7.0/10.0*Dr) ) ) );
    
    return sqrt(1.0+Dr)-1.0;
}

//==================================================================================================
// Elwert factor
//==================================================================================================
double F_EWF(double a1, double a2)
{
    return a2/a1*one_minus_exp_mx(TWOPI*a1)/one_minus_exp_mx(TWOPI*a2);
}

double F_EH(double a1, double a2)
{
    return 4.0*PI2*a1*a2*exp(-TWOPI*a1)/one_minus_exp_mx(TWOPI*a1)/one_minus_exp_mx(TWOPI*a2);
}

double F_NR(double a1, double a2)
{
    return 4.0*PI2*a1*a2/one_minus_exp_mx(TWOPI*a1)/one_minus_exp_mx(TWOPI*a2);
}

//==================================================================================================
//==================================================================================================
