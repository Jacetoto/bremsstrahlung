//==================================================================================================
//
// Simple functions used in Bremsstrahlung calculations
//
//==================================================================================================
//
// Author: Jens Chluba
// first implementation: Oct 2019
// last modification   : Oct 2019
//
//==================================================================================================

#ifndef BREMSSTRAHLUNG_SIMPLE_FUNCS_H
#define BREMSSTRAHLUNG_SIMPLE_FUNCS_H

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>
#include <complex>

#include "Bremsstrahlung.h"
#include "Definitions.h"

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

double gamma_func(double p);
double Emax_func(double p);        // == gamma1-1
double one_minus_exp_mx(double x); // == 1 - exp(-x)

//==================================================================================================
double Deta1eta2(Momenta_and_Angles &V, int Z);                 // == eta1-eta2
double Deta1eta2_nr(double p1, double p2, double omega, int Z); // == eta1-eta2
double Dp2p1(Momenta_and_Angles &V);                            // == p2-p1
double DS21_func(double mu1, double mu2, double Dmu21);         // == S2-S1
double Del_Daa1(double Da_a1);
double Del2_Daa1(double Da_a1);

//==================================================================================================
// Elwert factors
//==================================================================================================
double F_EWF(double a1, double a2);
double F_EH(double a1, double a2);
double F_NR(double a1, double a2);

#endif

//==================================================================================================
//==================================================================================================
