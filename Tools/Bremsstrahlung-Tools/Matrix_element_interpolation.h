//==================================================================================================
//
// Log pre-computed matrix elements A2, W2 and Im[V*W]-|W|^2 assuming w<<1
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================
#ifndef MATRIX_EL_INTERPOL_H
#define MATRIX_EL_INTERPOL_H

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

vector<double> Get_p1_values_W2();
vector<double> Get_p1_values_A2();
vector<double> Get_p1_values_W2_hi();
vector<double> Get_p1_values_A2_hi();

double W2_interpolated(double xi, double p1, int Z);
double A2_interpolated(double xi, double p1, int Z);

double W2_approx_high(double xi, double p1, int Z);
double A2_approx_high(double xi, double p1, int Z);

#endif

//==================================================================================================
//==================================================================================================
