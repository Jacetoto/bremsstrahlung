//==================================================================================================
//
// This code is compute the thermal average of Gaunt-factor functions
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// required libs
//==================================================================================================
#include "physical_consts.h"
#include "routines.h"
#include "parser.h"
#include "Bremsstrahlung.h"
#include "Bremsstrahlung-simple-functions.h"
#include "Gaunt_factor_interpolation.h"

string path_database=string(BREMSSTRAHLUNGPATH)+"./Database/";

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// structures for Gaunt-factor storage
//==================================================================================================
struct Gaunt_factor_table
{
    string label, filename;
    double p1min, p1max, wmin, wmax;
    vector<double> lp1arr;
    vector<double> lwarr;
    vector<vector<double> > Garr;
    
    // information to accelerate interpolation
    unsigned long search_ind_p1;
    unsigned long search_ind_w;
    
    Gaunt_factor_table()
    {
        search_ind_p1=search_ind_w=0;
        label=filename=" ";
    }
};

//==================================================================================================
// read Gaunt factor table
//==================================================================================================
void read_gaunt_factor_table(string fname, Gaunt_factor_table &G)
{
    file_content Gfc;
    parser_read_file(fname, Gfc);
    double val;
    vector<double> Gvals;
    
    istringstream iss(Gfc.lines[0]);
    
    //cout << "\n read_gaunt_factor_table:: loading file " << fname << endl;

    //----------------------------------------------------------------------------------------------
    // p-values
    //----------------------------------------------------------------------------------------------
    do {
        iss >> val;
        G.lp1arr.push_back(log(val)); // immediately store as log
    }
    while(iss);
    
    G.lp1arr.pop_back(); // remove last double entry

    //----------------------------------------------------------------------------------------------
    // remaining entries w, G1, G2, G3, ... GN
    //----------------------------------------------------------------------------------------------
    for(int k=1; k<(int)Gfc.lines.size(); k++)
    {
        istringstream iss_I(Gfc.lines[k]);

        iss_I >> val;
        G.lwarr.push_back(log(val)); // immediately store as log
        
        Gvals.clear();
        for(int l=0; l<(int)G.lp1arr.size(); l++)
        {
            iss_I >> val;
            Gvals.push_back(log(val)); // immediately store as log
        }
        
        G.Garr.push_back(Gvals);
    }

    //----------------------------------------------------------------------------------------------
    // set minimal and maximal values
    //----------------------------------------------------------------------------------------------
    G.p1min=exp(G.lp1arr[0]);
    G.p1max=exp(G.lp1arr.back());
    G.wmin=exp(G.lwarr[0]);
    G.wmax=exp(G.lwarr.back());

    //----------------------------------------------------------------------------------------------
    // tidy up
    //----------------------------------------------------------------------------------------------
    parser_free(Gfc);
}

//==================================================================================================
// read Gaunt factor table with switched roles of p1 and w
//==================================================================================================
void read_gaunt_factor_table_wp(string fname, Gaunt_factor_table &G)
{
    file_content Gfc;
    parser_read_file(fname, Gfc);
    double val;
    vector<double> Gvals;
    
    istringstream iss(Gfc.lines[0]);
    
    //cout << "\n read_gaunt_factor_table_wp:: loading file " << fname << endl;
    
    //----------------------------------------------------------------------------------------------
    // w-values
    //----------------------------------------------------------------------------------------------
    do {
        iss >> val;
        G.lwarr.push_back(log(val)); // immediately store as log
    }
    while(iss);
    
    G.lwarr.pop_back(); // remove last double entry
    
    //----------------------------------------------------------------------------------------------
    // remaining entries w, G1, G2, G3, ... GN
    //----------------------------------------------------------------------------------------------
    for(int k=1; k<(int)Gfc.lines.size(); k++)
    {
        istringstream iss_I(Gfc.lines[k]);
        
        iss_I >> val;
        G.lp1arr.push_back(log(val)); // immediately store as log
        
        Gvals.clear();
        for(int l=0; l<(int)G.lwarr.size(); l++)
        {
            iss_I >> val;
            Gvals.push_back(log(val)); // immediately store as log
        }
        
        //----------------------------------------------------------------------------------------------
        // copy to structure flipping p1 and w
        //----------------------------------------------------------------------------------------------
        if(G.Garr.size()==0) G.Garr.resize(Gvals.size());
        for(int iw=0; iw<(int) G.Garr.size(); iw++)
            G.Garr[iw].push_back(Gvals[iw]);
    }
    
    //----------------------------------------------------------------------------------------------
    // set minimal and maximal values
    //----------------------------------------------------------------------------------------------
    G.p1min=exp(G.lp1arr[0]);
    G.p1max=exp(G.lp1arr.back());
    G.wmin=exp(G.lwarr[0]);
    G.wmax=exp(G.lwarr.back());
    
    //----------------------------------------------------------------------------------------------
    // tidy up
    //----------------------------------------------------------------------------------------------
    parser_free(Gfc);
}

//==================================================================================================
// interpolation function for all cases
//==================================================================================================
double compute_Gaunt_interpolated(double p1, double w, Gaunt_factor_table &GT)
{
    //----------------------------------------------------------------------------------------------
    // treat simple cases
    //----------------------------------------------------------------------------------------------
    if(w>1.0) throw_error("compute_Gaunt_interpolated", "omega energetically not allowed!", 1);

    // error when p1 too small or too large
    if(p1<GT.p1min)
    {
        return 0.0;
        cout << " p1-val= " << p1 << " p1_min= " << GT.p1min << endl;
        throw_error("compute_Gaunt_interpolated", "table needs to be extended in p1", 1);
    }
    if(p1>GT.p1max)
    {
        cout << " p1-val= " << p1 << " p1_max= " << GT.p1max << endl;
        throw_error("compute_Gaunt_interpolated", "table needs to be extended in p1", 1);
    }

    // error when w too small
    if(w<GT.wmin)
    {
        cout << " w-val= " << w << endl;
        throw_error("compute_Gaunt_interpolated", "table needs to be extended in w", 1);
    }

    // for w>wmax just interpolate at wmax
    if(w>GT.wmax) w=GT.wmax;

    //----------------------------------------------------------------------------------------------
    // do interpolation
    //----------------------------------------------------------------------------------------------
    double logp1=log(p1);
    double logw=log(w);

    int nw =GT.lwarr .size();
    int np1=GT.lp1arr.size();
    int nint=4;
    
    // locate closes w and p1 indices
    locate_JC(&GT.lwarr [0], nw , logw , &GT.search_ind_w );
    locate_JC(&GT.lp1arr[0], np1, logp1, &GT.search_ind_p1);
    
    // avoid boundary extrapolation
    if(GT.search_ind_w +nint-1>=nw ) GT.search_ind_w -=GT.search_ind_w +nint-nw ;
    if(GT.search_ind_p1+nint-1>=np1) GT.search_ind_p1-=GT.search_ind_p1+nint-np1;
    
    // interpolate along p1 direction first
    double y, dy;
    vector<double> Gw(nint);
    int p_index=GT.search_ind_p1;

    for(int s=0; s<nint; s++)
    {
        int w_index=GT.search_ind_w+s;
        
        polint_JC(&GT.lp1arr[p_index], &GT.Garr[w_index][p_index], nint, logp1, nint-1, &Gw[s], &dy);
    }
    
    // now do a simple interpolation along w direction
    polint_JC(&GT.lwarr[GT.search_ind_w], &Gw[0], nint, logw, nint-1, &y, &dy);

    return exp(y);
}

//==================================================================================================
// variable mapping for varying Z
//==================================================================================================
double p1_eff_Gaunt(double p1, int Z)
{
    if(Z==1) return p1;
    double p1s=p1/Z;
    return p1s/sqrt( 1.0+p1s*p1s*(Z*Z-1) );
}

double omega_eff_Gaunt(double omega, double p1, int Z)
{
    if(Z==1) return omega;
    
    double gamma1=sqrt(1.0+p1*p1);
    double p2=sqrt(p1*p1+omega*(omega-2.0*gamma1));
    double p1eff=p1_eff_Gaunt(p1, Z);
    double p2eff=p1_eff_Gaunt(p2, Z);
    double gamma1eff=sqrt(1.0+p1eff*p1eff);
    double gamma2eff=sqrt(1.0+p2eff*p2eff);
    
    return (p1eff*p1eff-p2eff*p2eff)/(gamma1eff+gamma2eff);
}

//==================================================================================================
Gaunt_factor_table G_int_G_NR, G_int_G_NR_high;

double Gaunt_interpolated_NR(double p1, double omega, int Z)
{
    Gaunt_factor_table &Gl=G_int_G_NR;
    Gl.label="NR";
    Gl.filename="Gaunt_NR.dat";

    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.lp1arr.size()==0)
        read_gaunt_factor_table(path_database+Gl.filename, Gl);
    
    //----------------------------------------------------------------------------------------------
    // handle simplest cases
    //----------------------------------------------------------------------------------------------
    // for very small values of w (those outside of the table) use soft approximation
    //if(omega/Emax_func(p1)<=Gl.wmin) return Gaunt_NR_soft(p1, omega, Z);
    if(omega/Emax_func(p1)<=1.0e-8) return Gaunt_NR_soft(p1, omega, Z);

    // for large p1 the recursions should work
    //if(p1>=Gl.p1max) return Gaunt_NR(p1, omega, Z);
    if(p1>=1.0e-2) return Gaunt_NR(p1, omega, Z);

    //----------------------------------------------------------------------------------------------
    // remap variables for NR case
    //----------------------------------------------------------------------------------------------
    double p1Z=p1_eff_Gaunt(p1, Z), omegaZ=omega_eff_Gaunt(omega, p1, Z);
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor
    //----------------------------------------------------------------------------------------------
    double G=compute_Gaunt_interpolated(p1Z, omegaZ/Emax_func(p1Z), Gl);
    
    return G;
}

double Gaunt_interpolated_NR_rel_corr(double p1, double omega, int Z)
{ return Gaunt_interpolated_NR(p1, omega, Z)*(1.0+p1*p1); }

double Gaunt_interpolated_NR_KL(double p1, double omega, int Z) // explicitly setting gamma~1
{
    Gaunt_factor_table &Gl=G_int_G_NR;
    Gl.label="NR";
    Gl.filename="Gaunt_NR.dat";
    
    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.lp1arr.size()==0)
        read_gaunt_factor_table(path_database+Gl.filename, Gl);
    
    //----------------------------------------------------------------------------------------------
    // handle simplest cases
    //----------------------------------------------------------------------------------------------
    // for very small values of w (those outside of the table) use soft approximation
    if(omega*2.0/(p1*p1)<=1.0e-8) return Gaunt_NR_KL_soft(p1, omega, Z);
    
    // for large p1 the recursions should work
    if(p1>=1.0e-2) return Gaunt_NR_KL(p1, omega, Z);
    
    //----------------------------------------------------------------------------------------------
    // remap variables for NR case
    //----------------------------------------------------------------------------------------------
    double p1Z=p1/Z, omegaZ=omega/Z/Z;
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor
    //----------------------------------------------------------------------------------------------
    //double p2Z=sqrt(p1Z*p1Z+omegaZ*(omegaZ-2.0*sqrt(1.0+p1Z*p1Z)) );
    double p2Z=sqrt(p1Z*p1Z-2.0*omegaZ);
    double p1s=p1Z/sqrt(1.0+p1Z*p1Z), p2s=p2Z/sqrt(1.0+p2Z*p2Z);
    double omegas=(p1s-p2s)*(p1s+p2s)/2.0;
    
    double G=compute_Gaunt_interpolated(p1s, omegas*2.0/(p1s*p1s), Gl);
    
    return G;
}

//==================================================================================================
Gaunt_factor_table G_int_G_EH[100];

double Gaunt_interpolated_EH(double p1, double omega, int Z)
{
    if(Z>20) throw_error("Gaunt_interpolated_EH",
                        "database for Z>10 still needs to be added", 1);

    Gaunt_factor_table &Gl=G_int_G_EH[Z];
    Gl.label="EH_Z"+int_to_string(Z);
    //Gl.filename="Gaunt-EH-BH-Z"+int_to_string(Z)+"-new.dat";
    //Gl.filename="Gaunt-EH-BH-Z"+int_to_string(Z)+"-B.dat";
    Gl.filename="Gaunt-EH-BH-Z"+int_to_string(Z)+"-C.dat";

    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.lp1arr.size()==0)
        read_gaunt_factor_table_wp(path_database+Gl.filename, Gl);
    
    if(omega/Emax_func(p1)< 1.0e-10) return Gaunt_extrapolated_EH_soft(p1, omega, Z);
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor (no remapping of variables here)
    //----------------------------------------------------------------------------------------------
    double G=Gaunt_BH_ana(p1, omega, Z)*compute_Gaunt_interpolated(p1, omega/Emax_func(p1), Gl);
    
    return G;
}

//==================================================================================================
double Gaunt_interpolated_general(double p1, double omega, int Z)
{
    if(Z>20) throw_error("Gaunt_interpolated_general",
                         "database for Z>10 still needs to be added", 1);
    
    // make sure the data is loaded
    if(G_int_G_EH[Z].lp1arr.size()==0) Gaunt_interpolated_EH(0.01, 0.01*Emax_func(0.01), Z);
    
    if(p1<=G_int_G_EH[Z].p1min) return Gaunt_interpolated_NR(p1, omega, Z);
    else if(p1>=G_int_G_EH[Z].p1max) return Gaunt_BH_ana(p1, omega, Z);
    
    return Gaunt_interpolated_EH(p1, omega, Z);
}

//==================================================================================================
//==================================================================================================
