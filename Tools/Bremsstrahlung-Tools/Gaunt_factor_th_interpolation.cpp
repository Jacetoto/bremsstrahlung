//==================================================================================================
//
// This code is compute the thermal average of Gaunt-factor functions
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// required libs
//==================================================================================================
#include "physical_consts.h"
#include "routines.h"
#include "parser.h"
#include "Gaunt_factor_th_interpolation.h"

string path_database_th=string(BREMSSTRAHLUNGPATH)+"./Database-th/";

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// structures for Gaunt-factor storage
//==================================================================================================
struct Gaunt_factor_th_table
{
    string label, filename;
    double The_min, The_max, xmin, xmax;
    vector<double> lThearr;
    vector<double> lxarr;
    vector<vector<double> > Garr;
    
    // information to accelerate interpolation
    unsigned long search_ind_The;
    unsigned long search_ind_x;
    
    Gaunt_factor_th_table()
    {
        search_ind_The=search_ind_x=0;
        label=filename=" ";
    }
};

//==================================================================================================
// read Gaunt factor table
//==================================================================================================
void read_gaunt_factor_table_th(string fname, Gaunt_factor_th_table &G)
{
    file_content Gfc;
    parser_read_file(fname, Gfc);
    double val;
    vector<double> Gvals;
    
    istringstream iss(Gfc.lines[0]);
    
    //cout << "\n read_gaunt_factor_table:: loading file " << fname << endl;

    //----------------------------------------------------------------------------------------------
    // The-values
    //----------------------------------------------------------------------------------------------
    do {
        iss >> val;
        G.lThearr.push_back(log(val)); // immediately store as log
    }
    while(iss);
    
    G.lThearr.pop_back(); // remove last double entry

    //----------------------------------------------------------------------------------------------
    // remaining entries x, G1, G2, G3, ... GN
    //----------------------------------------------------------------------------------------------
    for(int k=1; k<(int)Gfc.lines.size(); k++)
    {
        istringstream iss_I(Gfc.lines[k]);

        iss_I >> val;
        G.lxarr.push_back(log(val)); // immediately store as log
        
        Gvals.clear();
        for(int l=0; l<(int)G.lThearr.size(); l++)
        {
            iss_I >> val;
            Gvals.push_back(log(val)); // immediately store as log
        }
        
        G.Garr.push_back(Gvals);
    }

    //----------------------------------------------------------------------------------------------
    // set minimal and maximal values
    //----------------------------------------------------------------------------------------------
    G.The_min=exp(G.lThearr[0]);
    G.The_max=exp(G.lThearr.back());
    G.xmin=exp(G.lxarr[0]);
    G.xmax=exp(G.lxarr.back());

    //----------------------------------------------------------------------------------------------
    // tidy up
    //----------------------------------------------------------------------------------------------
    parser_free(Gfc);
}

//==================================================================================================
// interpolation function for all cases
//==================================================================================================
double compute_Gaunt_th_interpolated(double The, double x, Gaunt_factor_th_table &GT)
{
    //----------------------------------------------------------------------------------------------
    // treat simple cases
    //----------------------------------------------------------------------------------------------
    // error when p1 too small or too large
    if(The<GT.The_min)
    {
        return 0.0;
        cout << " The-val= " << The << " The_min= " << GT.The_min << endl;
        throw_error("compute_Gaunt_th_interpolated", "table needs to be extended in The", 1);
    }
    if(The>GT.The_max)
    {
        cout << " The-val= " << The << " The_max= " << GT.The_max << endl;
        throw_error("compute_Gaunt_th_interpolated", "table needs to be extended in The", 1);
    }

    // error when w too small
    if(x<GT.xmin)
    {
        cout << " x-val= " << x << endl;
        throw_error("compute_Gaunt_th_interpolated", "table needs to be extended in x", 1);
    }

    // for x>xmax just interpolate at xmax
    if(x>GT.xmax) x=GT.xmax;

    //----------------------------------------------------------------------------------------------
    // do interpolation
    //----------------------------------------------------------------------------------------------
    double logThe=log(The);
    double logx  =log(x  );

    int nx  =GT.lxarr .size();
    int nThe=GT.lThearr.size();
    int nint=4;
    
    // locate closes w and p1 indices
    locate_JC(&GT.lxarr [0] , nx  , logx  , &GT.search_ind_x  );
    locate_JC(&GT.lThearr[0], nThe, logThe, &GT.search_ind_The);
    
    // avoid boundary extrapolation
    if(GT.search_ind_x +nint-1 >=nx  ) GT.search_ind_x  -=GT.search_ind_x  +nint-nx  ;
    if(GT.search_ind_The+nint-1>=nThe) GT.search_ind_The-=GT.search_ind_The+nint-nThe;
    
    // interpolate along p1 direction first
    double y, dy;
    vector<double> Gw(nint);
    int The_index=GT.search_ind_The;

    for(int s=0; s<nint; s++)
    {
        int x_index=GT.search_ind_x+s;
        
        polint_JC(&GT.lThearr[The_index], &GT.Garr[x_index][The_index], nint, logThe, nint-1, &Gw[s], &dy);
    }
    
    // now do a simple interpolation along w direction
    polint_JC(&GT.lxarr[GT.search_ind_x], &Gw[0], nint, logx, nint-1, &y, &dy);

    return exp(y);
}

//==================================================================================================
Gaunt_factor_th_table G_int_G_th[100];

double R_The(double The)
{
    if(The<0.001) return
        1.0 +
            The*(-1.875000000000000 +
               The*(2.695312500000000 +
                    The*(-3.208007812500000 +
                         The*(2.910003662109375 +
                              The*(-0.8852577209472656 +
                                   The*(-4.663342237472534 +
                                        The*(17.96749457716942
                                             -51.59003870794550*The)))))));
    
    return sqrt(PI*The/2.0)/scaled_BesselK2(1.0/The);
}

double Gaunt_th_interpolated(double The, double xe, int Z)
{
    if(Z>2) throw_error("Gaunt_th_interpolated",
                        "database for Z>2 still needs to be added", 1);

    Gaunt_factor_th_table &Gl=G_int_G_th[Z];
    Gl.label="G_th_Z"+int_to_string(Z);
    Gl.filename="Gaunt_th_Z"+int_to_string(Z, 2)+"_Gen.dat";

    //----------------------------------------------------------------------------------------------
    // load table as needed
    //----------------------------------------------------------------------------------------------
    if(Gl.lThearr.size()==0)
        read_gaunt_factor_table_th(path_database_th+Gl.filename, Gl);
    
    //----------------------------------------------------------------------------------------------
    // compute Gaunt-factor
    //----------------------------------------------------------------------------------------------
    double om=xe*The;
    double norm=( pow(1.0+om, 2)+2.0*The*(1.0+om+The) )*R_The(The);
    
    double G=compute_Gaunt_th_interpolated(The, xe, Gl);
    
    return G*norm;
}

//==================================================================================================
//==================================================================================================
