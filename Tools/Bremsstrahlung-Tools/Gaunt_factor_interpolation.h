//==================================================================================================
//
// This code is compute the thermal average of Gaunt-factor functions
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================
#ifndef GAUNT_FACTOR_INTERPOL_H
#define GAUNT_FACTOR_INTERPOL_H

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

double Gaunt_interpolated_NR_KL(double p1, double omega, int Z); // explicitly setting gamma~1
double Gaunt_interpolated_NR(double p1, double omega, int Z);
double Gaunt_interpolated_NR_rel_corr(double p1, double omega, int Z);
double Gaunt_interpolated_EH(double p1, double omega, int Z);

double Gaunt_interpolated_general(double p1, double omega, int Z); // merges all regimes

#endif

//==================================================================================================
//==================================================================================================
