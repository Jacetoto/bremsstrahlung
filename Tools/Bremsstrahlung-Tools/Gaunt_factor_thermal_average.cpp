//==================================================================================================
//
// This code is compute the thermal average of Gaunt-factor functions
//
//==================================================================================================
//
// Author: Boris Bolliet and Jens Chluba
// first implementation: Aug 2019
// last modification   : Sept 2019
//
//==================================================================================================

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>

//==================================================================================================
// required libs
//==================================================================================================
#include "routines.h"
#include "Definitions.h"
#include "physical_consts.h"
#include "Patterson.h"
#include "Bremsstrahlung.h"
#include "Bremsstrahlung-simple-functions.h"
#include "Gaunt_factor_thermal_average.h"

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// structures with variables
//==================================================================================================
struct Parameters_for_thermal_average
{
    int Z;
    double omega, Th_e;
    double (*gaunt_factor)(double omega, double p1, int Z);

    double p1min;
    string av_limit, fe; // decides about the averaging limit and electron distribution
    
    Parameters_for_thermal_average()
    {
        gaunt_factor=NULL;
        omega=0.0;
        Z=1;
        av_limit="exact";
        fe="rMB";
    }
    
    void set(double omegav, double Th_ev, int Zv,
             double (*g)(double, double, int),
             string limitv, string fev)
    {
        gaunt_factor=g;
        omega=omegav;
        Th_e=Th_ev;
        Z=Zv;
        av_limit=limitv;
        fe=fev;
        
        if(av_limit=="NR-pmin") p1min=sqrt(2.0*omega);
        else if(av_limit=="Itoh") p1min=0.0;
        else p1min=sqrt(omega*(2.0+omega));
        
        p1min*=(1.0+1.0e-10); // add a very small value to threshold

        return;
    }
    
    Parameters_for_thermal_average(double omegav, double Th_ev, int Zv,
                                   double (*g)(double, double, int),
                                   string limitv, string fev)
    {
        this->set(omegav, Th_ev, Zv, g, limitv, fev);
    }
};

//==================================================================================================
// electron distribution functions mappings
//==================================================================================================

//--------------------------------------------------------------------------------------------------
// f ~ exp(-p^2/2Th_e)
//--------------------------------------------------------------------------------------------------
double p1_xi_nr(double p1min, double xi, double Th_e)
{
    // xi = (p^2-pmin^2)/(2Th_e)
    return sqrt(p1min*p1min + 2.0*Th_e*xi);
}

double dp1_dlnxi_nr(double p1min, double p1, double xi, double Th_e)
{
    return Th_e * xi/p1;
}

//--------------------------------------------------------------------------------------------------
// f ~ exp(-gamma/Th_e)
//--------------------------------------------------------------------------------------------------
double p1_xi_rel(double p1min, double xi, double Th_e)
{
    // xi = (gamma-gamma_min)/ Th_e
    double gamma_min=sqrt(1.0+p1min*p1min);
    //return sqrt(p1min*p1min + 4.0*Th_e*xi*(gamma_min+Th_e*xi)); ## BUG ##
    return sqrt(p1min*p1min + Th_e*xi*(2.0*gamma_min+Th_e*xi));
}

double dp1_dlnxi_rel(double p1min, double p1, double xi, double Th_e)
{
    // == xi * The * gamma1 / p1
    double gamma_min=sqrt(1.0+p1min*p1min);
    return Th_e*(gamma_min+Th_e*xi) * xi/p1;
}

//==================================================================================================
// set these pointers to choice the thermal averaging case
//==================================================================================================
double (*p1_xi_f)(double p1min, double xi, double Th_e);
double (*dp1_dlnxi_f)(double p1min, double p1, double xi, double Th_e);

//==================================================================================================
// normal Kramers Gaunt-factor
double gaunt_factor_K    (double p1, double omega, int Z){ return 1.0; }

// relativistically-corrected Kramers
double gaunt_factor_K_rel(double p1, double omega, int Z){ return 1.0+p1*p1; }

//==================================================================================================
double integrand_gaunt_factor(double lxi, void *p)
{
    Parameters_for_thermal_average *V=((Parameters_for_thermal_average *) p);
    double xi=exp(lxi);
    double p1=p1_xi_f(V->p1min, xi, V->Th_e), gamma1=sqrt(1.0+p1*p1);
    double dp1_dlnxi=dp1_dlnxi_f(V->p1min, p1, xi, V->Th_e);
    
    // renormalize such that integral of denominator is == 1
    dp1_dlnxi/=V->Th_e;
    
    if(V->av_limit=="exact")
        return dp1_dlnxi * (p1/gamma1) * exp(-xi) * V->gaunt_factor(p1, V->omega, V->Z);

    else if(V->av_limit=="NR" || V->av_limit=="NR-pmin")
        return dp1_dlnxi * p1 * exp(-xi) * V->gaunt_factor(p1, V->omega, V->Z);

    // called with Gaunt = 1 this gives == K2(1/The) exp(1/The) / sqrt(Pi The / 2) ~ 1 at The << 1
    else if(V->av_limit=="Itoh")
        return dp1_dlnxi * p1*p1 * exp(-xi) * V->gaunt_factor(p1, V->omega, V->Z) / sqrt(PI/2.0*V->Th_e);

    throw_error("integrand_gaunt_factor", "averaging limit not found", 1);

    return 0;
}

double integrate_gaunt_factor(Parameters_for_thermal_average &V)
{
    void *params=&V;
    
    double epsrel=1.0e-6;
    double epsabs=1.0e-60;

    if(V.fe!="nrMB" && V.fe!="rMB")
        throw_error("integrate_gaunt_factor", "chose fe== 'nrMB' or 'rMB'", 1);
    
    if(V.fe=="nrMB")
    {
        p1_xi_f=p1_xi_nr;
        dp1_dlnxi_f=dp1_dlnxi_nr;
    }
    else if(V.fe=="rMB")
    {
        p1_xi_f=p1_xi_rel;
        dp1_dlnxi_f=dp1_dlnxi_rel;
    }

    double r=Integrate_using_Patterson_adaptive(log(2.0e-7), log(100.0),
                                                epsrel, epsabs,
                                                integrand_gaunt_factor,
                                                params);

    return r;
}

//==================================================================================================
// main routine to manage thermal averages
//==================================================================================================
double Thermally_averaged_gaunt_factor(double omega, double Th_e, int Z,
                                       double (*gaunt_factor)(double omega, double p1, int Z),
                                       string mode)
{
    Parameters_for_thermal_average VK, V;
    
    if(mode=="exact")
    {
        // Kramers' case
        VK.set(omega, Th_e, Z, gaunt_factor_K, "exact", "rMB");
        V .set(omega, Th_e, Z, gaunt_factor  , "exact", "rMB");
    }
    else if(mode=="exact-hf")
    {
        // Kramers' case with relativistic boosting
        VK.set(omega, Th_e, Z, gaunt_factor_K_rel, "exact", "rMB");
        V .set(omega, Th_e, Z, gaunt_factor      , "exact", "rMB");
    }
    else if(mode=="Karzas")
    {
        // Kramers' case assuming all non-relativistic
        VK.set(omega, Th_e, Z, gaunt_factor_K, "NR-pmin", "nrMB");
        V .set(omega, Th_e, Z, gaunt_factor  , "NR-pmin", "nrMB");
    }
    else if(mode=="Itoh")
    {
        // Kramers' case assuming all non-relativistic
        VK.set(omega, Th_e, Z, gaunt_factor_K, "Itoh", "rMB");
        V .set(omega, Th_e, Z, gaunt_factor  , "exact", "rMB");
    }
    else throw_error("Thermally_averaged_gaunt_factor", "averaging limit not found", 1);

    double denominator = integrate_gaunt_factor(VK);
    double numerator   = integrate_gaunt_factor(V);
    
    return numerator/denominator;
}

//==================================================================================================
//==================================================================================================
