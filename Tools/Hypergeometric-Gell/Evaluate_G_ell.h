//==================================================================================================================
// Evaluate matrix elements G_ell needed for NR and EH cross sections. Recursion relations are used to avoid
// numerical cancelation issues.
//==================================================================================================================

#ifndef EVALUATE_G_ELL_H
#define EVALUATE_G_ELL_H

#include <string>
#include <vector>
#include <complex>

using namespace std;

//==================================================================================================================
void set_precision_G_ell(double eps);

//==================================================================================================================
struct work_space_G_ell
{
    double eta1, eta2, Deta12;
    int ell;
    
    // precomputed coefficients
    vector<double> CIa, CIb;
    vector<double> CII, CIII;
    
    work_space_G_ell()
    {
        eta1=eta2=Deta12=0.0; ell=0;
    }

    work_space_G_ell(double eta1v, double eta2v, double Deta12v, int ellv)
    {
        eta1=eta1v; eta2=eta2v; Deta12=Deta12v;
        ell=ellv;
    }
    
    void clear()
    {
        CIa.clear(); CIb.clear();
        CII.clear(); CIII.clear();
        
        return;
    }
    
    void show()
    {
        cout << "G-ell-workspace::" << eta1 << " " << eta2 << " " << Deta12 << " " << ell << endl;
        cout << " stored terms: x<-2.0 " << CIa.size() << " " << CIb.size() << endl;
        cout << " stored terms: -2<=x<-0.5 " << CII.size() << endl;
        cout << " stored terms: -0.5<=x<0.5 " << CIII.size() << endl;
        return;
    }
};

//==================================================================================================================
int Re_Im_J_GSL(double eta1, double eta2, double Deta12, int ell, double &ReJ, double &ImJ);
int G_ell_function(double eta1, double eta2, double Deta12, int ell, double x, double &G_ell);
int G_ell_function(double eta1, double eta2, double Deta12, int ell, double x, double &G_ell, work_space_G_ell &W);

void test_G_ell();

//==================================================================================================================
// ode solve
//==================================================================================================================
int G0x_function(double eta1, double eta2, double Deta12,
                 const vector<double> &x, vector<double> &G0x, vector<double> &DG0x);

int G0x_function(double eta1, double eta2, double Deta12, double x, double &G0x, double &DG0x);

#endif

//==================================================================================================================
//==================================================================================================================
