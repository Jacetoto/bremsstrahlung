//==================================================================================================================
// compute hypergeometric functions 2F1(a, b, c, x). These routines only work for real x <=0.5.
// The zones x<-1 and -1 <= x < 0 are mapped into the interval 0<=x<0.5. The coefficients
// for given (a, b, c) are stored to accelerate repeated call of function.
//==================================================================================================================

//==================================================================================================================
// Standards
//==================================================================================================================
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <complex>
#include <fstream>
#include <stdlib.h>

#include "routines.h"
#include "Definitions.h"
#include "Hyper_Geometric_2F1.h"
#include "gsl/gsl_sf_gamma.h"

using namespace std;

//==================================================================================================
// precision settings for computations
//==================================================================================================
double Hyper_epsrel=1.0e-12;
int Hyper_Nmax=20000;

void set_precision_2F1(double eps){ Hyper_epsrel=eps; return; }

//==================================================================================================
// Gamma-factors for relations
//==================================================================================================
int Gamma_factors_relation_I_GSL(complex<double> a, complex<double> b, complex<double> c, double lnf,
                                 complex<double> &f1, complex<double> &f2)
{
    f1=LnLambda_func(c, b-a, b, c-a);
    f2=LnLambda_func(c, a-b, a, c-b);
    f2=exp(f2-f1);
    f1=exp(f1+lnf);

    return 0;
}

int Gamma_factors_relation_II_GSL(complex<double> a, complex<double> b, complex<double> c, double lnf,
                                  complex<double> &g1, complex<double> &g2)
{
    g1=LnLambda_func(c, a+b-c, a, b);
    g2=LnLambda_func(c, c-a-b, c-a, c-b);
    g2=exp(g2-g1);
    g1=exp(g1+lnf);

    return 0;
}

//==================================================================================================
// compute hypergeometric functions 2F1. These routines only work for real x<1.0
//==================================================================================================
int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x,
                  complex<double> &F, vector<complex<double> > &C)
{
    int Nmax=Hyper_Nmax;
    double eps=Hyper_epsrel;
    double xn=x;
    
    // first two terms in Gauss series
    if((int)C.size()==0) C.push_back(a*b/c);
    F=C[0]*x;
    
    int count=0;
    for(int n=1; n<=Nmax; n++)
    {
        // add more coefficients if needed
        if(n>=(int)C.size()) C.push_back(C[n-1] * (a+1.0*n)*(b+1.0*n)/((c+1.0*n)*(1.0+n)));
        
        xn*=x;
        F+=C[n]*xn;
        
        //cout << n << " " << F << " " << C[n] << " " << C[n]*xn << endl;
        
        // stop when better than expected 5 times [--> adds some safety]
        if(abs(real(C[n]*xn))/(abs(real(F))+1.0e-300)<eps &&
           abs(imag(C[n]*xn))/(abs(imag(F))+1.0e-300)<eps)
            count++;
        else count=0;
        
        if(count==5){ F+=1.0; return 0; }
    }
    
    cout.precision(16);
    cout << " Hyper_Geo_2F1:: not converged to eps= " << eps
         << " in Nmax= " << Nmax << " evaluations." << endl;
    cout << " (a, b, c, x)= " << a << " " << b << " " << c << " " << x << " " << F << endl;
    
    return 1;
}

//==================================================================================================================
// compute exp(f) * 2F1(a, b, c, x) for real x
//==================================================================================================================
int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x, double lnf,
                  complex<double> &F, work_space_Hyper_Geo_2F1 &W)
{
    if(a!=W.a || b!=W.b || c!=W.c || W.lnf!=lnf)
    {
        W.clear();
        W.a=a; W.b=b; W.c=c; W.lnf=lnf;
    }
    
    int err=0;
    
    //if(x<-1.0) // map into [0..0.5)
    if(x<-2.0) // map into [0..1/3)
    //if(x<-0.5) // map into [0..2/3)
    //if(x<-5000.0) // map into [0..2/3)
    {
        if(!W.coefficient_I_set)
        {
            Gamma_factors_relation_I_GSL(a, b, c, W.lnf, W.f1, W.f2);
            W.coefficient_I_set=1;
        }
        
        complex<double> Fe;
        err+=Hyper_Geo_2F1(a, c-b, a-b+1.0, 1.0/(1.0-x), Fe, W.CIa);
        F=Fe;
        err+=Hyper_Geo_2F1(b, c-a, b-a+1.0, 1.0/(1.0-x), Fe, W.CIb);
        F+=Fe*W.f2/pow(1.0-x, b-a);
        F*=W.f1/pow(1.0-x, a);
        
        return err;
    }
    //else if(-0.5<=x && x<0.5) // map back into [-0.5,0.0)
    //else if(-2.0<=x && x<=-0.5) // map back into [1/3,2/3]
    else if(-2.0<=x && x<0.5) // map back into [1/3,2/3]
    {
        if(abs(a)>abs(b))
        {
            err=Hyper_Geo_2F1(c-a, b, c, x/(x-1.0), F, W.CIIa);
            F/=pow(1.0-x, b) / exp(W.lnf);
        }
        else
        {
            err=Hyper_Geo_2F1(a, c-b, c, x/(x-1.0), F, W.CIIb);
            F/=pow(1.0-x, a) / exp(W.lnf);
        }
        
        return err;
    }
    else if(0.5<=x && x<=1.5) // map back into [-0.5..0.5]
    {
        if(!W.coefficient_II_set)
        {
            Gamma_factors_relation_II_GSL(a, b, c, W.lnf, W.g1, W.g2);
            W.coefficient_II_set=1;
        }
        
        complex<double> Fe;
        err+=Hyper_Geo_2F1(c-a, c-b, c-a-b+1.0, 1.0-x, Fe, W.CIVa);
        F=Fe*pow(1.0-x, c-a-b);
        err+=Hyper_Geo_2F1(a, b, a+b-c+1.0, 1.0-x, Fe, W.CIVb);
        F+=Fe*W.g2;
        F*=W.g1;
        
        return err;
    }
    else if(1.5<x) // map back into [0..2/3]
    {
        if(!W.coefficient_III_set)
        {
            Gamma_factors_relation_I_GSL(a, b, c, W.lnf, W.h1, W.h2);
            W.coefficient_III_set=1;
        }

        complex<double> Fe;
        err+=Hyper_Geo_2F1(a, a-c+1.0, a-b+1.0, 1.0/x, Fe, W.CVa);
        F=Fe;
        err+=Hyper_Geo_2F1(b, b-c+1.0, b-a+1.0, 1.0/x, Fe, W.CVb);
        F+=Fe*W.h2/pow(-x, b-a);
        F*=W.h1/pow(-x, a);

        return err;
    }

    return Hyper_Geo_2F1(a, b, c, x, F, W.CIII); // x in [0..0.5)
}

int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x, double lnf,
                  complex<double> &F)
{
    work_space_Hyper_Geo_2F1 W;
    return Hyper_Geo_2F1(a, b, c, x, lnf, F, W);
}

//==================================================================================================================
// compute 2F1(a, b, c, x) for real x
//==================================================================================================================
int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x,
                  complex<double> &F)
{
    work_space_Hyper_Geo_2F1 W;
    return Hyper_Geo_2F1(a, b, c, x, 0.0, F, W);
}

int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x,
                  complex<double> &F, work_space_Hyper_Geo_2F1 &W)
{
    return Hyper_Geo_2F1(a, b, c, x, 0.0, F, W);
}

//==================================================================================================================
// for testing the functions
//==================================================================================================================
void test_Hyper_Geo_2F1()
{
    complex<double> a1(0.0, 1.0e-2), a2(0.0, 4.0e-2);
    
    complex<double> F;
    complex<double> dF;
    work_space_Hyper_Geo_2F1 W, dW;

    //----------------------------------------------------
    double xv=0.1;
    Hyper_Geo_2F1(a1, a2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=-10.0;
    Hyper_Geo_2F1(a1, a2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=-0.5;
    Hyper_Geo_2F1(a1, a2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    //----------------------------------------------------
    complex<double> c1(0.0, 10.0), c2(0.0, 4.0);
    W.clear();
    xv=0.1;
    Hyper_Geo_2F1(c1, c2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=0.5;
    Hyper_Geo_2F1(c1, c2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=1.0;
    Hyper_Geo_2F1(c1, c2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=1.5;
    Hyper_Geo_2F1(c1, c2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    xv=15.0;
    Hyper_Geo_2F1(c1, c2, 1, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;

    //----------------------------------------------------
    xv=0.0125695;
    complex<double> b1(2,-72.9735), b2(2,23076.3);
    Hyper_Geo_2F1(b1, b2, 4, xv, F, W);
    W.show();
    cout << xv << " " << F << endl;
    
    return;
}

//==================================================================================================================
//==================================================================================================================
