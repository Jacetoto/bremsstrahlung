//==================================================================================================================
// Evaluate matrix elements G_ell needed for NR and EH cross sections. Recursion relations are used to avoid
// numerical cancelation issues.
//==================================================================================================================

//==================================================================================================================
// Standards
//==================================================================================================================
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <complex>
#include <fstream>
#include <stdlib.h>
#include <cmath>

#include "routines.h"
#include "Definitions.h"
#include "Hyper_Geometric_2F1.h"
#include "ODE_solver_Rec.h"
#include "Evaluate_G_ell.h"

using namespace std;
using namespace ODE_solver_Rec;

//==================================================================================================================
// precision settings for computations
//==================================================================================================================
double G_ell_epsrel=1.0e-8;
int G_ell_Nmax=50000;

void set_precision_G_ell(double eps){ G_ell_epsrel=eps; return; }

//==================================================================================================================
// Gamma-factors for relations.
// Compute Gamma[2(l+1)] Gamma[-i(eta1-eta2)] / (Gamma[1+ell-i eta1] Gamma[1+ell+i eta2])
//==================================================================================================================
int Re_Im_J_GSL(double eta1, double eta2, double Deta12, int ell, double &ReJ, double &ImJ)
{
    if(abs(Deta12/eta1)<1.0e-6)
    {
        if(ell>1){ cerr << " check function Re_Im_J_GSL " << endl; exit(0); }
        
        double fac=(1.0-exp(-2.0*PI*eta1))/(2.0*PI*eta1);
        ReJ=-fac*Harmonic_number_Re(eta1);
        ImJ= fac*(1.0/Deta12-Harmonic_number_Im(eta1));
        
        if(ell==1)
        {
            double g=(1.0+eta1*eta1);
            ReJ+=-fac/g;
            ImJ+= fac*eta1/g;
            ReJ*=6.0/g;
            ImJ*=6.0/g;
        }
        
        //cout << " App " << ell << " " << ReJ << " " << ImJ << endl;
        return 0;
    }

    complex<double> J, I(0,1);
    complex<double> a(1.0+ell, -eta1), b(1.0+ell, eta2), c(2.0*ell+2.0, 0), d(0.0, -Deta12);
    
    J =LnGamma_func(c);
    J-=LnGamma_func(a);
    J-=LnGamma_func(b);
    J+=LnGamma_func(d);
    //cout << J-PI*eta1 << " " << a << " " << b << " " << d << endl;
    
    J=exp(J-PI*eta1);
    ReJ=real(J);
    ImJ=imag(J);
    //cout << " Ex " << ReJ << " " << ImJ << endl;

    return 0;
}

//==================================================================================================================
// compute G_ell for |x| < 1.0
//==================================================================================================================
int compute_G_ell_recursion_I(double eta1, double eta2, double Deta12, int ell,
                              double x, double &Gell, vector<double> &C)
{
    int Nmax=G_ell_Nmax;
    double eps=G_ell_epsrel;
    double llp1=ell*(ell+1.0), eta12=eta1*eta2, etam=Deta12/2.0;
    
    // first two terms in power series
    if((int)C.size()==0) C.push_back(1.0);

    double xn=1.0;
    Gell=C[0];
    
    int count=0;
    for(int n=1; n<=Nmax; n++)
    {
        // add more coefficients if needed
        if(n>=(int)C.size())
        {
            double d=n*(2*ell+1+n);
            double f1=(llp1+2.0*(n-1)*(2*ell+n)-eta12)/d;
            double f2=(pow(2.0*(ell+n)-3, 2)/4.0+etam*etam)/d;
            if(n>1) C.push_back( f1*C[n-1]-f2*C[n-2] );
            else C.push_back( f1*C[n-1] );
        }

        xn*=x;
        Gell+=C[n]*xn;

        //cout << " low " << eta12 << " " << n << " " << Gell << " " << C[n] << " " << C[n]*xn << endl;
        
        // stop when better than expected 5 times [--> adds some safety]
        if(abs(C[n]*xn)/(abs(Gell)+1.0e-300)<eps) count++;
        else count=0;
        
        if(count==5){ Gell*=pow(-x, ell+1)/sqrt(1.0-x); return 0; }
    }
    
    cout << " compute_G_ell_recursion_I:: not converged to eps= " << eps
         << " in Nmax= " << Nmax << " evaluations." << endl;
    cout << " (eta1, eta2, ell, x)= " << eta1 << " " << eta2 << " " << ell << " " << x << endl;
    
    return 1;
}

//==================================================================================================================
// compute G_ell for |x| > 1.0
//==================================================================================================================
int compute_G_ell_recursion_II(double eta1, double eta2, double Deta12, int ell, double x, double &Gell,
                               vector<double> &Ca, vector<double> &Cb)
{
    int Nmax=G_ell_Nmax;
    double eps=G_ell_epsrel;
    double llp1=ell*(ell+1.0), eta12=eta1*eta2;
    double etam=Deta12/2.0;
    double A, B, z=1.0/x;
    
    // first term in power series
    if((int)Ca.size()==0)
    {
        double ReJ, ImJ;
        Re_Im_J_GSL(eta1, eta2, Deta12, ell, ReJ, ImJ);

        //cout << x << " " << ReJ << " " << ImJ << endl;
        
        Ca.push_back( 2.0*ReJ);
        Cb.push_back(-2.0*ImJ);
    }

    double scale=eta12/etam/etam/4.0;
    double zn=1.0;
    A=Ca[0];
    B=Cb[0];

    int count=0;
    for(int n=1; n<=Nmax; n++)
    {
        // add more coefficients if needed
        if(n>=(int)Ca.size())
        {
            double d=(4.0*etam*etam+n*n)*scale;
            double cm1=(1.0+n*(2*n-3)-llp1-eta12+6.0*etam*etam*(1.0-1.0/n))/d;
            double cm2=(1.0*(n+ell-1)*(n-ell-2)+3.0*etam*etam*(1.0-2.0/n))/d/scale;
            double dm1=etam/n*(2.0*llp1+3*n-2+eta1*eta1+eta2*eta2)/d;
            double dm2=etam/n*(2.0*llp1+3*n-4+2.0*etam*etam)/d/scale;
            
            //cout << n << " " << cm1 << " " << cm2 << " " << dm1 << " " << dm2 << endl;
            
            if(n>1) {
                Ca.push_back( cm1*Ca[n-1]-cm2*Ca[n-2] + dm1*Cb[n-1]-dm2*Cb[n-2]);
                Cb.push_back( cm1*Cb[n-1]-cm2*Cb[n-2] - dm1*Ca[n-1]+dm2*Ca[n-2]);
            }
            else {
                Ca.push_back( cm1*Ca[n-1] + dm1*Cb[n-1]);
                Cb.push_back( cm1*Cb[n-1] - dm1*Ca[n-1]);
            }
        }
        
        zn*=z*scale;
        A+=Ca[n]*zn;
        B+=Cb[n]*zn;

        //cout << " high " << Deta12 << " " << 4.0*etam*etam+n*n << endl;
        //cout << " high " << eta12 << " " << n << " " << A << " " << Ca[n] << " " << Ca[n]*zn << endl;
        //cout << " high " << eta12 << " " << n << " " << B << " " << Cb[n] << " " << Cb[n]*zn << endl;
        //cout << " high " << (A*cos(etam*log(-z))+B*sin(etam*log(-z)))*exp(-PI*eta1) << endl;
        //cout << " high " << (A*cos(etam*log(-z))+B*sin(etam*log(-z))) << endl;
        //wait_f_r();
        
        // stop when better than expected 5 times [--> adds some safety]
        if(abs(Ca[n]*zn)/(abs(A)+1.0e-300)<eps && abs(Cb[n]*zn)/(abs(B)+1.0e-300)<eps) count++;
        else count=0;
        
        if(count==5) {
            Gell=( A*cos(etam*log(-z))+B*sin(etam*log(-z)) );
            return 0;
        }
    }

    cout << " compute_G_ell_recursion_II:: not converged to eps= " << eps
         << " in Nmax= " << Nmax << " evaluations." << endl;
    cout << " (eta1, eta2, ell, x)= " << eta1 << " " << eta2 << " " << ell << " " << x << endl;
    
    return 1;
}

int compute_G_ell_recursion_III(double eta1, double eta2, double Deta12, int ell, double x, double &Gell,
                               vector<double> &Ca, vector<double> &Cb)
{
    int Nmax=G_ell_Nmax;
    double eps=G_ell_epsrel;
    double llp1=ell*(ell+1.0), eta12=eta1*eta1;
    double etam=Deta12/2.0, etap=(eta1+eta2)/2.0;
    double A, B, z=1.0/x;
    
    // first term in power series
    if((int)Ca.size()==0)
    {
        double ReJ, ImJ;
        Re_Im_J_GSL(eta1, eta2, Deta12, ell, ReJ, ImJ);
        
        Ca.push_back( 2.0*ReJ);
        Cb.push_back(-2.0*ImJ);
    }
    
    double scale=eta12/etam/etam/4.0;
    double zn=1.0;
    A=Ca[0];
    B=Cb[0];
    
    int count=0;
    for(int n=1; n<=Nmax; n++)
    {
        // add more coefficients if needed
        if(n>=(int)Ca.size())
        {
            double d=(4.0*etam*etam+n*n)*scale;
            double cm1=(1.0+n*(2*n-3)-llp1-eta12+2.0*eta1*etam*(2.0-1.0/n)+4.0*etam*etam*(1.0-1.0/n))/d;
            double cm2=(2.0+n*(  n-3)-llp1-eta12+2.0*eta1*etam*(2.0-3.0/n))/d/scale;
            double dm1=((2*n-1)*eta1+2.0*etam/n*(llp1-(n-1)*(n-1)+eta12))/d;
            double dm2=((2*n-3)*eta1+2.0*etam/n*(llp1-2-n*(n-3)  +eta12))/d/scale;
            
            //cout << n << " " << cm1 << " " << cm2 << " " << dm1 << " " << dm2 << endl;
            
            if(n>1) {
                Ca.push_back( cm1*Ca[n-1]-cm2*Ca[n-2] + dm1*Cb[n-1]-dm2*Cb[n-2]);
                Cb.push_back( cm1*Cb[n-1]-cm2*Cb[n-2] - dm1*Ca[n-1]+dm2*Ca[n-2]);
            }
            else {
                Ca.push_back( cm1*Ca[n-1] + dm1*Cb[n-1]);
                Cb.push_back( cm1*Cb[n-1] - dm1*Ca[n-1]);
            }
        }
        
        zn*=z*scale;
        A+=Ca[n]*zn;
        B+=Cb[n]*zn;
        
        // stop when better than expected 5 times [--> adds some safety]
        if(abs(Ca[n]*zn)/(abs(A)+1.0e-300)<eps && abs(Cb[n]*zn)/(abs(B)+1.0e-300)<eps) count++;
        else count=0;
        
        if(count==5) {
            Gell=( A*cos(etap*log(1.0-z)+etam*log(-z))+B*sin(etap*log(1.0-z)+etam*log(-z)) );
            return 0;
        }
    }
    
    cout << " compute_G_ell_recursion_III:: not converged to eps= " << eps
         << " in Nmax= " << Nmax << " evaluations." << endl;
    cout << " (eta1, eta2, ell, x)= " << eta1 << " " << eta2 << " " << ell << " " << x << endl;
    
    return 1;
}

//==================================================================================================================
int G_ell_function(double eta1, double eta2, double Deta12, int ell, double x, double &G_ell, work_space_G_ell &W)
{
    if(eta1!=W.eta1 || eta2!=W.eta2 || Deta12!=W.Deta12 || ell!=W.ell)
    {
        W.clear();
        W.eta1=eta1; W.eta2=eta2; W.Deta12=Deta12; W.ell=ell;
    }
    
    int err=0;
    
    if(x<-2.0) // maps into [-0.5..0)
    {
        err=compute_G_ell_recursion_II(eta1, eta2, Deta12, ell, x, G_ell, W.CIa, W.CIb);
        //cout << " HH0 " << x << " " << G_ell << endl;
        return err;
    }
    else if(-2.0<x && x<=-0.5) // maps into [1/3 .. 2/3)
    {
        err=compute_G_ell_recursion_I(eta1, -eta2, eta1+eta2, ell, x/(x-1.0), G_ell, W.CII);
        G_ell*=pow(-1.0, ell+1)*exp(-PI*eta1);
        //cout << " HH1 " << x << " " << G_ell << endl;
        return err;
    }
    else if(-0.5<x && x<=0.5)
    {
        err=compute_G_ell_recursion_I(eta1, eta2, Deta12, ell, x, G_ell, W.CIII);
        G_ell*=exp(-PI*eta1);
        //cout << " HH2 " << x << " " << G_ell << endl;
        return err;
    }

    cerr << " G_ell_function:: not implemented yet x= " << x << endl;

    return 1;
}

int G_ell_function(double eta1, double eta2, double Deta12, int ell, double x, double &G_ell)
{
    work_space_G_ell W;
    return G_ell_function(eta1, eta2, Deta12, ell, x, G_ell, W);
}

//==================================================================================================================
// for testing the functions
//==================================================================================================================
void test_G_ell()
{
    double eta1=0.8, eta2=0.7, x=-4.0*eta1*eta2/pow(eta1-eta2, 2), G_ell;
    work_space_G_ell WG;
    int ell=0;
    
    G_ell_function(eta1, eta2, eta1-eta2, ell, x, G_ell, WG);
    cout << x << " " << G_ell << endl;

    complex<double> F;
    work_space_Hyper_Geo_2F1 W;
    complex<double> a1(1.0+ell, eta1), a2(1.0+ell, eta2), cv(2*(ell+1), 0.0);
    complex<double> etap(0.0, (eta1+eta2)/2.0);
    
    Hyper_Geo_2F1(a1, a2, cv, x, -PI*eta1, F, W);
    F*=pow(-x, ell+1) * pow(1.0-x, etap);
    cout << x << " " << F << endl;

    return;
}

//==================================================================================================================
// ode solve
//==================================================================================================================
struct ODE_parameters
{
    double alpha_x;
    double eta12;
    double gamma;
};

ODE_parameters glob_G_pars;

void fcn_jac_energy(int *neq, double *z, double *y, double *f, int col)
{
    double x=*z;

    if(col==-1)
    {
        f[0]=glob_G_pars.alpha_x*y[0] + y[1];
        f[1]=glob_G_pars.alpha_x*y[1] + y[1]/(1.0-x)
           -(glob_G_pars.eta12/x+glob_G_pars.gamma)*y[0]/pow(1.0-x, 2);
    }
    else if(col==0)
    {
        f[0]=  glob_G_pars.alpha_x;
        f[1]=-(glob_G_pars.eta12/x+glob_G_pars.gamma)/pow(1.0-x, 2);
    }
    else if(col==1)
    {
        f[0]=1.0;
        f[1]=glob_G_pars.alpha_x + 1.0/(1.0-x);
    }
    else throw_error("??", "???", 1);
    
    return;
}

int G0x_function(double eta1, double eta2, double Deta12,
                 const vector<double> &xfa,
                 vector<double> &G0xa, vector<double> &DG0xa)
{
    // rescale by exp(pi eta1 (1-xi/xf)), which gives G(xi) at xi=xif
    bool rescale=0, verbose=0;
    
    // ODE parameters
    glob_G_pars.alpha_x=(rescale ? -PI*eta1/xfa.back() : 0.0);
    glob_G_pars.eta12=eta1*eta2;
    glob_G_pars.gamma=Deta12*Deta12/4.0;

    // ODE solver variables
    set_verbosity(0);
    ODE_solver_Solution Sz;
    ODE_solver_accuracies tols;
    ODE_Solver_data ODE_G0;
    
    // memory and errors
    Sz.y.resize(2, 0.0);
    Sz.dy.resize(2, 0.0);
    tols.rel.resize(2, 1.0e-8);
    tols.abs.resize(2, 1.0e-300);

    // initial condition
    double xin=min(-1.0e-20, 1.0e-6*xfa[0]);
    Sz.z=xin;
    Sz.y[0]= -xin*(1.0 + xin/2.0*(1.0-glob_G_pars.eta12)); // series around xi~0
    Sz.y[1]= -    (1.0 - xin    *(1.0-glob_G_pars.eta12)); // series around xi~0
    if(!rescale){ Sz.y[0]*=exp(-PI*eta1); Sz.y[1]*=exp(-PI*eta1); }

    // arm solver
    ODE_Solver_set_up_solution_and_memory(Sz, tols, ODE_G0, fcn_jac_energy, 0);

    //==============================================================================================================
    // save intermediate steps
    //==============================================================================================================
    for(int k=0; k<(int)DG0xa.size(); k++)
    {
        double x=xfa[k];
        double dx=x-xin;
        double del=(k<=10 ? 0.0001 : 0.1);
        
        ODE_Solver_Solve_history(xin, x, 1.0e-8*dx, dx*del, Sz, ODE_G0);
        
        G0xa[k] =Sz.y[0]; DG0xa[k]=Sz.y[1];
        
        if(verbose)
            cout << x << " G0= " << Sz.y[0] << " DG0= " << Sz.y[1]
                      << " -G0 * DG0 = " << -Sz.y[0]*Sz.y[1] << endl;
        
        xin=x;
    }

    clear_SolverData(ODE_G0);
    
    return 0;
}

//==================================================================================================================
// define some reasonable amount of intermediate steps
//==================================================================================================================
int nsteps=1000;
vector<double> G0x_xarr(nsteps), glob_G0xa(nsteps), glob_DG0xa(nsteps);

int G0x_function(double eta1, double eta2, double Deta12, double xf, double &G0x, double &DG0x)
{
    // assumption is xf < 0
    init_xarr(min(-1.0e-10, 1.0e-10*xf), xf, &G0x_xarr[0], nsteps, 1, 0);

    int err=G0x_function(eta1, eta2, Deta12, G0x_xarr, glob_G0xa, glob_DG0xa);
    
    // return only final values
    G0x=glob_G0xa.back();
    DG0x=glob_DG0xa.back();

    return err;
}
//==================================================================================================================
//==================================================================================================================
