//==================================================================================================================
// compute hypergeometric functions 2F1(a, b, c, x). These routines only work for real x <=0.5.
// The zones x<-1 and -1 <= x < 0 are mapped into the interval 0<=x<0.5. The coefficients
// for given (a, b, c) are stored to accelerate repeated call of function.
//==================================================================================================================

#ifndef HYPER_GEO_H
#define HYPER_GEO_H

#include <string>
#include <vector>
#include <complex>

using namespace std;

//==================================================================================================================
void set_precision_2F1(double eps);

//==================================================================================================================
struct work_space_Hyper_Geo_2F1
{
    double lnf;
    complex<double> a, b, c;
    
    // coefficients for relation
    bool coefficient_I_set, coefficient_II_set, coefficient_III_set;
    complex<double> f1, f2;
    complex<double> g1, g2;
    complex<double> h1, h2;
    
    // precomputed coefficients
    vector<complex<double> > CIa, CIb;
    vector<complex<double> > CIIa, CIIb, CIII;
    vector<complex<double> > CIVa, CIVb;
    vector<complex<double> > CVa, CVb;

    void clear()
    {
        coefficient_I_set=0;
        CIa.clear(); CIb.clear();
        CIIa.clear(); CIIb.clear(); CIII.clear();
        coefficient_II_set=0;
        CIVa.clear(); CIVb.clear();
        coefficient_III_set=0;
        CVa.clear(); CVb.clear();
        
        return;
    }

    work_space_Hyper_Geo_2F1()
    {
        this->clear();
        lnf=0.0;
        f1=f2=g1=g2=h1=h2=0.0;
        a=b=c=0.0;
    }

    work_space_Hyper_Geo_2F1(complex<double> av, complex<double> bv, complex<double> cv)
    {
        this->clear();
        lnf=0.0;
        f1=f2=g1=g2=h1=h2=0.0;
        a=av; b=bv; c=cv;
    }
    
    void show()
    {
        cout << "Hyper_Geo_2F1-workspace::" << a << " " << b << " " << c << " lnf = " << lnf << endl;
        cout << " stored terms: x<-2.0 " << CIa.size() << " "  << CIb.size() << endl;
        cout << " stored terms: -2<=x<0.5 " << CIIa.size() << " " << CIIb.size() << endl;
        cout << " stored terms: (not used) " << CIII.size() << endl;
        cout << " stored terms: 0.5<=x<=1.5 " << CIVa.size() << " " << CIVb.size() << endl;
        cout << " stored terms: 1.5<x " << CVa.size() << " " << CVb.size() << endl;
        return;
    }
};

//==================================================================================================================
// compute 2F1(a, b, c, x) for real x
//==================================================================================================================
int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x, complex<double> &F);

int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x,
                  complex<double> &F, work_space_Hyper_Geo_2F1 &W);

//==================================================================================================================
// compute exp(f) * 2F1(a, b, c, x) for real x
//==================================================================================================================
int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x, double lnf, complex<double> &F);

int Hyper_Geo_2F1(complex<double> a, complex<double> b, complex<double> c, double x, double lnf,
                  complex<double> &F, work_space_Hyper_Geo_2F1 &W);

void test_Hyper_Geo_2F1();

#endif

//==================================================================================================================
//==================================================================================================================
