//==================================================================================================
// Bethe-Heitler case, explicitly carried out and slightly rewritten
//==================================================================================================
double dsigma_dkdOmega1dOmega2_BH(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_BH:: angles not correct" << endl;
    
    double a   =const_alpha*Z;
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=a*V.gamma1/V.p1, a2=a*V.gamma2/V.p2;
    
    V.Dxi=-1.0;
    V.Dmu21=V.mu2-V.mu1;
    V.compute_derived_values();

    double mu12=V.mu1*V.mu2+cos(V.phi)*V.S1*V.S2;
    double p1p2=V.p1*V.p2*mu12;
    
    double q2=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(V.p2*V.mu2-V.p1*V.mu1)-p1p2);
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    //----------------------------------------------------------------------------------------------
    // cross section evaluation
    //----------------------------------------------------------------------------------------------
    double norm=F_EWF(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    
    double h1=V.p1*V.S1, h2=V.p2*V.S2, h1h2=V.p1*V.p2*cos(V.phi)*V.S1*V.S2;
    double eps12=V.gamma1*V.gamma1, eps22=V.gamma2*V.gamma2;
    double M2=pow(q2, -2)*( pow(h1/V.kap1, 2)*(4.0*eps22-q2)
                           +pow(h2/V.kap2, 2)*(4.0*eps12-q2)
                           +2.0/V.kap1/V.kap2*( V.o2*(h1*h1+h2*h2)-(4.0*V.gamma1*V.gamma2+2.0*V.o2-q2)*h1h2 )
                           );

    return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
// differential in xi, mu1 and mu2
//==================================================================================================
double dsigma_dkdOmega1dOmega2_BH_xi(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_BH_xi:: angles not correct" << endl;
    
    V.compute_derived_values();
    
    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;
    
    double Delta=V.Dxi*(V.Dxitot-V.Dxi);
    if(Delta<=0) return 0.0;
    
    double q2=V.xi*V.kap12/V.mut;
    if(q2<=0.0) return 0.0;
    
    //----------------------------------------------------------------------------------------------
    // cross section evaluation
    //----------------------------------------------------------------------------------------------
    double norm=F_EWF(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;

    double chi1=V.p1*V.S1, chi2=V.p2*V.S2;
    double f0=chi1/V.kap1-chi2/V.kap2, f1=V.gamma2*chi1/V.kap1-V.gamma1*chi2/V.kap2;
    double tau12=4.0*V.gamma1*V.gamma2-q2, chi12=chi1*chi2*(1.0-2.0*V.Dxi/V.Dxitot);
    double zeta1=chi1*chi1-chi12, zeta2=chi2*chi2-chi12;
    double JBH=(zeta1+zeta2)*2.0*V.o2/V.kap12;
    JBH+= 4.0*pow(f1, 2)-pow(f0, 2)*q2;
    JBH+= V.Dxi/V.mut*tau12;
/*
    double chi1=V.p1*V.S1, chi2=V.p2*V.S2;
    double f0=chi1/V.kap1-chi2/V.kap2, f1=V.gamma2*chi1/V.kap1-V.gamma1*chi2/V.kap2;
    double JBH=2.0*pow(chi1-chi2, 2)*V.o2 / V.kap12;
    JBH+= 4.0*pow(f1, 2)-pow(f0, 2)*q2;
    JBH+= V.Dxi/V.mut*( 4.0*V.gamma1*V.gamma2+2.0*V.o2-q2 );
*/
    double M2=JBH/pow(q2, 2);
    M2*= 2.0/sqrt(Delta); // 2*dphi2/dxi: remove when comparing cross sections

    return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
// integrated cross section
//==================================================================================================
double dsigma_dk_BH_ana(Momenta_and_Angles &V, int Z)
{
    // Heitler-cross section integrated over all angles (see Heitler book, Nozawa et al. 1998 or J&R)
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    
    double a1=const_alpha*Z*V.gamma1/V.p1, a2=const_alpha*Z*V.gamma2/V.p2;
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double norm=F_EWF(a1, a2) * sigT * const_alpha*Z*Z * 3.0/(8.0*PI) * V.p2/V.p1 / V.omega;
    
    // similar to Jauch & Rohrlich after adding factor of 2 in L-term
    double l1=2.0*log(V.gamma1+V.p1)/V.p1, l2=2.0*log(V.gamma2+V.p2)/V.p2;
    double g1g2=V.gamma1*V.gamma2, p1p2=V.p1*V.p2;
    double L=2.0*log((g1g2+p1p2-1.0)/V.omega)/p1p2;
    
    double M2=4.0/3.0-2.0*g1g2*(pow(V.p1, 2)+pow(V.p2, 2))/pow(p1p2, 2)
              +l1*V.gamma2/pow(V.p1, 2)+l2*V.gamma1/pow(V.p2, 2)-l1*l2
              +L*( 8.0/3.0*g1g2
                  +0.5*V.omega*( (1.0+g1g2/pow(V.p1, 2))*l1 - (1.0+g1g2/pow(V.p2, 2))*l2 )
                  +V.o2*( 1.0+g1g2*(1.0+g1g2)/pow(p1p2, 2) ) );
    
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
// Gaunt-factors
//==================================================================================================
double Gaunt_BH_ana(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);

    return dsigma_dk_BH_ana(V, Z)/dsigma_dk_K(V, Z);
}

//==================================================================================================
//==================================================================================================
