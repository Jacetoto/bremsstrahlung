//==================================================================================================
void create_scripts_Gaunt_factor_tables(double p1min, double p1max, int np,
                                        double wmin, int nw, int Z,
                                        string low_freq_integrator,
                                        string high_freq_integrator,
                                        string batchmode,
                                        string sel,
                                        int nthreads, string path)
{
    cout << " create_scripts_Gaunt_factor_tables:: creating runfiles for "
    << nthreads << " threads." << endl;
    
    ofstream ofile, call_file;
    int np_per_thread=np/(nthreads);
    int index=0;
    
    string fileroot=path+"/job_Z"+int_to_string(Z);
    call_file.open((fileroot+".batch").c_str());
    call_file << "cd .." << endl;
    
    if(np%nthreads>0) np_per_thread+=1;
    
    for(int k=0; k<nthreads; k++)
    {
        string add="_"+int_to_string(k+1, 4);
        string fname=fileroot+add+".ini";
        ofile.open(fname.c_str());
        
        ofile << "cross section limit = " << sel << endl;
        ofile << "low frequency integrator = " << low_freq_integrator << endl;
        ofile << "high frequency integrator = " << high_freq_integrator << endl << endl;
        //
        ofile << "output directory = " << path << "-outputs" << endl;
        ofile << "addition to name = " << "_Z"+int_to_string(Z)+add+".dat" << endl << endl;
        ofile << "Gaunt factor batch mode = " << batchmode << endl;
        //
        ofile << "Nuclear charge Z = " << Z << endl << endl;
        ofile << "minimal w value wmin = " << wmin << endl;
        ofile << "number of points in w = " << nw << endl << endl;
        if(batchmode=="plot") ofile << "minimal electron momentum p1min = " << p1min << endl;
        else ofile << "minimal electron momentum p1min = " << p1min*Z << endl;
        //ofile << "maximal electron momentum p1max = " << p1max*Z << endl;
        ofile << "maximal electron momentum p1max = " << p1max << endl;
        ofile << "number of points in p1 = " << np << endl << endl;
        // decides which p1 values to use in the full array between [p1min, p1max]
        ofile << "p1 start index = " << index << endl;
        index+=np_per_thread;
        ofile << "p1 end index = " << (int)min(index, np) << endl;
        ofile.close();
        
        call_file << "./Bremsstrahlung Gaunt-split " + fname << " >> " +fname+"-log &" << endl;
        
        if(index>=np) break;
    }
    
    call_file.close();
    
    // make runfile executable
    change_to_executable(fileroot+".batch");
    
    return;
}

//==================================================================================================
void setup_w_grid(double wmin, vector<double> &warr, string sel="lin_high")
{
    int nw=warr.size();
    double wsplit=0.2, whigh=0.9999;
    double log_den=(wmin<wsplit ? log(wsplit/wmin)/(log(wsplit/wmin)+10*(wsplit/0.1)*log(whigh/wsplit)) : 1);
    
    vector<double> tarr(nw);
    
    if(sel=="reverse_log")
    {
        int nwlow = (wmin<wsplit ? (int)(log_den*nw) : 1);
        
        if(wmin<wsplit) init_xarr(wmin, wsplit, &warr[0], nwlow, 1, 0);
        init_xarr(1.0-whigh, 1.0-wsplit, &tarr[0], nw+1-nwlow, 1, 0);
        // reverse high frequency part
        for(int k=0; k<nw+1-nwlow; k++) warr[nwlow-1+k]=1.0-tarr[nw-nwlow-k];
    }
    else if(sel=="lin_high")
    {
        int nwlow = (wmin<wsplit ? (int)(log_den*nw) : 1);
        
        if(wmin<wsplit) init_xarr(wmin, wsplit, &warr[0], nwlow, 1, 0);
        init_xarr(wsplit, whigh, &warr[nwlow-1], nw+1-nwlow, 0, 0);
    }
    
    //for(int k=0; k<nw; k++){ cout << warr[k] << endl; if(warr[k]>=wsplit) wait_f_r(); }
    
    return;
}

//==================================================================================================
void output_Gaunt_factors_batch(string name, int p1start, int p1end,
                                double p1min, double p1max, int np,
                                double wmin, int nw, int Z,
                                string low_freq_integrator,
                                string high_freq_integrator,
                                string batchmode,
                                string sel, int verbose=0)
{
    if(np<p1end || p1start<0) throw_error("output_Gaunt_factors_batch", "check requested p1 indices", 1);

    // output array in w
    vector<double> warr(nw);
    //setup_w_grid(wmin, warr, "lin_high");
    setup_w_grid(wmin, warr, "reverse_log");

    //warr.clear();
    //warr.push_back(0.01);
    //warr.push_back(0.1);
    //warr.push_back(0.9999);

    // output array in p1
    vector<double> p1arr(np);
    init_xarr(p1min, p1max, &p1arr[0], np, 1, 0);
    
    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    bool switch_integrator=1;               // for w>w_switch can switch to Patterson/CUBA
    double w_switch=1.0e-2;

    set_integration_scheme(low_freq_integrator, 0);
    
    cout << " output_Gaunt_factors_batch:: computing case = " << sel;
    cout << " using " << selected_variables << " variables for Z = " << Z << endl;
    cout << " np = " << np << " p1-start = " << p1start << " p1-end = " << p1end << endl;
    
    if(p1start==0)
    {
        // write header info
        ofile << "#########################################################################" << endl;
        ofile << "# cross section limit     = " << sel << endl;
        ofile << "# epsrel_precision_DEH    = " << scientific << epsrel_precision_DEH << endl;
        ofile << "# epsrel_precision_DEH_xi = " << scientific << epsrel_precision_DEH_xi << endl;
        ofile << "# switch_integrator       = " << switch_integrator << endl;
        ofile << "# integrator_sel          = " << low_freq_integrator << " / " << high_freq_integrator << endl;
        ofile << "# w_switch                = " << scientific << w_switch << endl;
        ofile << "#########################################################################" << endl;

        // write info of w
        if(batchmode=="plot") ofile << "#";
        if(batchmode=="plot-2D") ofile << scientific << 0.0 << " ";
        for(int k=0; k<(int)warr.size(); k++) ofile << scientific << warr[k] << " ";
        ofile << endl;
    }
    
    // main output block. Each row is for a given p1
    for(int m=p1start; m<p1end; m++)
    {
        double p1=p1arr[m];
        if(switch_integrator) set_integration_scheme(low_freq_integrator, 0);

        if(verbose>0) cout << "\33[2K" << " p1 = " << p1 << " " << flush;
        ofile << scientific << p1 << " ";
        
        for(int k=0; k<(int)warr.size(); k++)
        {
            double w=warr[k], y;
            
            if(w>w_switch && switch_integrator) set_integration_scheme(high_freq_integrator, 0);

            if(verbose>0) cout << "w = " << w << " " << flush;
            
            if(batchmode=="plot") y=Gaunt_sel(p1, w*Emax_func(p1), Z, sel);
            //else y=Gaunt_sel_rel(p1, w*Emax_func(p1), Z, sel);
            else y=Gaunt_sel_BH(p1, w*Emax_func(p1), Z, sel);

            if(verbose>0) cout << "G = " << y << " " << flush;
            ofile << scientific << y << " ";
        }
        
        if(verbose>0) cout << endl << "\x1b[A";
        ofile << endl;
    }

    if(verbose>0) cout << "\33[2K";
    cout << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_Gaunt_factors_batch_th(string name,
                                   double The_min, double The_max, int nThe,
                                   double xmin, double xmax, int nx, int Z,
                                   string sel, string mode, int verbose=0)
{
    // output array in x
    vector<double> xarr(nx);
    init_xarr(xmin, xmax, &xarr[0], nx, 1, 0);

    // output array in The
    vector<double> The_arr(nThe);
    init_xarr(The_min, The_max, &The_arr[0], nThe, 1, 0);
    
    ofstream ofile(name.c_str());
    ofile.precision(10);
    
    cout << " output_Gaunt_factors_batch_th:: computing case = " << sel;
    cout << " using " << selected_variables << " variables for Z = " << Z << endl;
    cout << " nThe = " << nThe << endl;
    
    // write header info
    ofile << "#########################################################################" << endl;
    ofile << "# cross section limit = " << sel << " Z= " << Z << " mode= " << mode << endl;
    ofile << "# xmin= " << xmin << " xmax= " << xmax << " nx= " << nx << endl;
    ofile << "# The_min= " << The_min << " The_max= " << The_max << " nThe= " << nThe << endl;
    ofile << "#########################################################################" << endl;
    
    // write info of The
    if(mode=="plot") ofile << "#";
    for(int k=0; k<(int)The_arr.size(); k++) ofile << scientific << The_arr[k] << " ";
    ofile << endl;
    
    // main output block. Each row is for a given p1
    vector<double> y(nThe, 0.0), ypre(nThe, 0.0);
    vector<bool> eval(nThe, 1);
    for(int k=0; k<(int)xarr.size(); k++)
    {
        double x=xarr[k];
        
        if(verbose>0) cout << "\33[2K" << " x = " << x << " " << flush;
        ofile << scientific << x << " ";
        
        for(int m=0; m<(int)The_arr.size(); m++)
        {
            double The=The_arr[m];
            
            if(verbose>2) cout << "The = " << The << " " << flush;
            
            if(eval[m]) y[m]=Gaunt_th_sel_rel(The, x, Z, sel);
            
            if(verbose>2) cout << "G = " << y[m] << " " << flush;
            ofile << scientific << y[m] << " ";

            // estimate derivative to stop at high frequencies
            if(x>=2.0e+1/sqrt(The) && eval[m])
            {
                double dgdx=(y[m]-ypre[m])/(x-xarr[k-1]);
                if(abs(x/y[m]*dgdx)<5.0e-5) eval[m]=0;
            }
            else if(!eval[m] && verbose>0) cout << "\n extrapolation " << x << " " << y[m];
            
            ypre[m]=y[m];
        }

        if(verbose>0) cout << endl << "\x1b[A";
        ofile << endl;
    }
    
    if(verbose>1) cout << "\33[2K";
    cout << " done " << endl << endl;
    
    return;
}

//==================================================================================================

