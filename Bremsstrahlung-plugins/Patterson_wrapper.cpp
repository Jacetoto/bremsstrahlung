//==================================================================================================
// integrals over cross section functions
//==================================================================================================
int insert_interval(double xl, double xh, vector<double> &xarr, double dx_min, bool merge=true)
{
    // assumes that the total range should not exceed xarr[0] .. xarr[n-1]
    if(xh<=xarr[0] || xl>=xarr.back()) return -1;
        
    int ll=0, lh=1;
    // lower point
    for(int l=0; l<(int)xarr.size()-1; l++)
    {
        if(xl==xarr[l]){ ll=l; break; }
        
        if(xl>xarr[l] && xl<xarr[l+1]) {
            //cout << l << " " << xl << " " << xarr[l] << endl;
            if(xl-xarr[l]<dx_min && merge){ xarr[l]=xl; ll=l; }
            else{ xarr.insert(xarr.begin()+l+1, xl); ll=l+1; }
            break;
        }
    }

    // upper point
    for(int l=ll; l<(int)xarr.size()-1; l++)
    {
        if(xh==xarr[l]){ lh=l; break; }
        
        if(xh>xarr[l] && xh<xarr[l+1]) {
            //cout << ll << " " << l << " " << xh << " " << xarr[l] << endl;
            if(xh-xarr[l]<dx_min && merge){ xarr[l]=xh; lh=l; }
            else{ xarr.insert(xarr.begin()+l+1, xh); lh=l+1; }
            break;
        }
    }

    // erase points in the middle if wanted
    //cout << " A " << ll << " " << xarr[ll] << " " << lh << " " << xarr[lh] << endl;
    if(ll+1<lh && merge) xarr.erase(xarr.begin()+ll+1, xarr.begin()+lh);
    
    return ll;
}

int insert_point(double x, vector<double> &xarr, double dx_min)
{
    return insert_interval(xarr[0]-1.0, x, xarr, dx_min, false);
}

void generate_intervals(double mu1, Momenta_and_Angles &V, vector<vector<double> > &ints)
{
    ints.clear();
    double dmu2=min(0.1, 2.0e-4*V.omega/V.E1);
    double dmub=min(0.1, 2.0e-4/V.gamma1);
    vector<double> xa, dum(2);
    xa.push_back(-1.0); xa.push_back(1.0);
    
    // make it simple for high frequencies
    if(V.omega/V.E1>0.1){ ints.push_back(xa); return; }
    
    insert_point(-1.0+dmub, xa, dmub);
    insert_point(mu1-10*dmu2, xa, dmub);
    insert_point(mu1+10*dmu2, xa, dmub);
    insert_point( 1.0-dmub, xa, dmub);
    int s=insert_interval(mu1-dmu2, mu1+dmu2, xa, dmu2);
    
    //for(int l=0; l<(int)xa.size(); l++) cout << l << " " << xa[l] << endl;
    
    int len=xa.size();
    if(s==-1) cerr << " interval not inside range " << endl;
    else {
        dum[0]=xa[s]; dum[1]=xa[s+1]; ints.push_back(dum);
        if(s>0){ dum[0]=xa[0]; dum[1]=xa[1]; ints.push_back(dum); }
        if(s<len-1){ dum[0]=xa[len-2]; dum[1]=xa[len-1]; ints.push_back(dum); }
        for(int l=1; l<s; l++){ dum[0]=xa[l]; dum[1]=xa[l+1]; ints.push_back(dum); }
        for(int l=s+1; l<len-2; l++){ dum[0]=xa[l]; dum[1]=xa[l+1]; ints.push_back(dum); }
    }

    //for(int l=0; l<(int)ints.size(); l++) cout << ints[l][0] << " " << ints[l][1] << endl;
    //wait_f_r();
    
    return;
}

//==================================================================================================
void determine_phi_intervals(Momenta_and_Angles &V, vector<vector<double> > &ints)
{
    ints.clear();

    // estimate structure in phi using BH cross section --> phi~0 and phi~2*PI similar
    double S1=sqrt(1.0-V.mu1*V.mu1), S2=sqrt(1.0-V.mu2*V.mu2);
    double mu12b=V.mu1*V.mu2+S1*S2;
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2, pi12b=V.p1*V.p2*mu12b;
    double q2b=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-pi12b);
   
    // estimated dmu where dsigma ~ dsigma_bound / 4
    double dmu=(S1>0 && S2>0 ? q2b/(2.0*V.p1*V.p2*S1*S2) : 1.0);
    
    vector<double> dum(2);
    
    if(V.omega/V.E1>0.1){ dum[0]=0.0; dum[1]=PI; ints.push_back(dum); return; }
    
    if(dmu>0.2){ dum[0]=0.0; dum[1]=PI; ints.push_back(dum); } // one interval as broad
    else                                                       // three intervals
    {
        double dphi=acos(1.0-dmu);
        //dum[0]=TWOPI-dphi; dum[1]=TWOPI; ints.push_back(dum);
        dum[0]=0.0; dum[1]=0.0+dphi; ints.push_back(dum);
        //dum[0]=0.0+dphi; dum[1]=TWOPI-dphi; ints.push_back(dum);
        dum[0]=0.0+dphi; dum[1]=PI; ints.push_back(dum);
    }
    
    return;
}

//==================================================================================================
// integrals over cross section functions
//==================================================================================================
double dsigma_dkdOmega1dOmega2(double phi, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    IV->V.phi=phi;

    return IV->dsigma(IV->V, IV->Z);
}

//==================================================================================================
// Patterson scheme specific functions
//==================================================================================================
double dsigma_dkdOmega1dmu2(double mu2, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.mu2=mu2;
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z);

    vector<vector<double> > ints;
    determine_phi_intervals(IV->V, ints);

    for(int v=0; v<(int)ints.size(); v++)
    {
        double f=PI/(ints[v][1]-ints[v][0]);
        //cout << ints[v][0] << " " << ints[v][1] << endl;
        r+=Integrate_using_Patterson_adaptive(ints[v][0], ints[v][1],
                                              epsrel, epsabs*f,
                                              dsigma_dkdOmega1dOmega2, params);

        epsabs=max(P.epsabs, abs(r*epsrel/TWOPI));
    }

    return 2.0*r;
}

double dsigma_dkdmu1(double mu1, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.mu1=mu1;
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int2_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI;

    vector<vector<double> > ints;
    generate_intervals(mu1, IV->V, ints);

    //cout << endl << mu1 << endl;
    for(int v=0; v<(int)ints.size(); v++)
    {
        double f=2.0/(ints[v][1]-ints[v][0]);
        //cout << ints[v][0] << " " << ints[v][1] << endl;
        r+=Integrate_using_Patterson_adaptive(ints[v][0], ints[v][1],
                                              epsrel, epsabs*f,
                                              dsigma_dkdOmega1dmu2, params);

        epsabs=max(P.epsabs, abs(r*epsrel/2.0));
    }

    return r;
}

double dsigma_dk(void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();

    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z), r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*FOURPI;

    vector<vector<double> > ints;
    vector<double> dum(2);
    //dum[0]=-1.0; dum[1]=1.0; ints.push_back(dum);
    double dmub=min(0.1, 2.0/IV->V.gamma1);
    dum[0]=1.0-dmub; dum[1]=1.0; ints.push_back(dum);
    dum[0]=-1.0; dum[1]=1.0-dmub;  ints.push_back(dum);

    for(int v=0; v<(int)ints.size(); v++)
    {
        double f=2.0/(ints[v][1]-ints[v][0]);
        //cout << ints[v][0] << " " << ints[v][1] << endl;
        r+=Integrate_using_Patterson_adaptive(ints[v][0], ints[v][1],
                                              epsrel, epsabs*f,
                                              dsigma_dkdmu1, params);

        epsabs=max(P.epsabs, abs(r*epsrel/2.0));
    }

    return TWOPI*r;
}

//==================================================================================================
//
// xi-variables
//
//==================================================================================================
double dsigma_dk_Dxi_I3(double logDxi, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    double Dxi=exp(logDxi);
    IV->V.Dxi=Dxi;
    
//**************************************************************
// HOTFIX!!! AR: I call compute_derived_values again,
//               but it would be better to have 
//               two separate routines 
//**************************************************************
    IV->V.compute_derived_values();
//**************************************************************
// HOTFIX ENDS
//**************************************************************
    
    return Dxi*IV->dsigma(IV->V, IV->Z);
}

//--------------------------------------------------------------------------------------------------
double dsigma_dk_Dxi_I2(double mu2, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.phi=-10.0;
    IV->V.Dmu21=mu2-IV->V.mu1;
    IV->V.mu2=mu2;
    IV->V.compute_derived_values();
    
    double Dximin=P.epsrel/10.0*IV->V.w;
    double lDximin=log(Dximin), lDximax=log(max(IV->V.Dxitot, Dximin));
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI/(1.0e-300+lDximax-lDximin);

    r=Integrate_using_Patterson_adaptive(lDximin, lDximax, epsrel, epsabs, dsigma_dk_Dxi_I3, params);

    return r;
}

//--------------------------------------------------------------------------------------------------
double dsigma_dk_Dxi_I2_p(double lDmu21, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.phi=-10.0;
    double Dmu21=exp(lDmu21);
    IV->V.Dmu21=Dmu21;
    IV->V.mu2=IV->V.mu1+Dmu21;
    IV->V.compute_derived_values();
    
    double Dximin=P.epsrel/10.0*IV->V.w;
    double lDximin=log(Dximin), lDximax=log(max(IV->V.Dxitot, Dximin));

    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI/(1.0e-300+lDximax-lDximin);
    //double epsabs=fabs(epsrel/20.0 * dsigma_dk_Dxi_I3(log(2.0), params))*TWOPI/(1.0e-300+lDximax-lDximin);

    r=Integrate_using_Patterson_adaptive(lDximin, lDximax, epsrel, epsabs, dsigma_dk_Dxi_I3, params);
    
    return Dmu21*r;
}

double dsigma_dk_Dxi_I2_m(double lDmu21, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.phi=-10.0;
    double Dmu21=exp(lDmu21);
    IV->V.Dmu21=-Dmu21;
    IV->V.mu2=IV->V.mu1-Dmu21;
    IV->V.compute_derived_values();

    double Dximin=P.epsrel/10.0*IV->V.w;
    double lDximin=log(Dximin), lDximax=log(max(IV->V.Dxitot, Dximin));
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI/(1.0e-300+lDximax-lDximin);
    //double epsabs=fabs(epsrel/20.0 * dsigma_dk_Dxi_I3(log(2.0), params))*TWOPI/(1.0e-300+lDximax-lDximin);

    r=Integrate_using_Patterson_adaptive(lDximin, lDximax, epsrel, epsabs, dsigma_dk_Dxi_I3, params);

    return Dmu21*r;
}

//--------------------------------------------------------------------------------------------------
void generate_intervals_xi(double lDximin, double lDximax, vector<vector<double> > &ints)
{
    ints.clear();
    
    vector<double> xa;
    xa.push_back(lDximin); xa.push_back(lDximax);
    insert_point(log(1.0e-4), xa, 1.0e-3);
    insert_point(log(1.0e-2), xa, 1.0e-3);
    insert_point(log(1.0e+2), xa, 1.0e-3);
    insert_point(log(1.0e+4), xa, 1.0e-3);
    insert_point(log(1.0e+8), xa, 1.0e-3);
    int s=insert_interval(log(0.5), log(10.0), xa, 1.0e-3);

    int len=xa.size();
    vector<double> dum(2);

    if(s==-1) for(int l=0; l<len-1; l++){ dum[0]=xa[l]; dum[1]=xa[l+1]; ints.push_back(dum); }
    else
    {
        dum[0]=xa[s]; dum[1]=xa[s+1]; ints.push_back(dum);
        if(s>0){ dum[0]=xa[0]; dum[1]=xa[1]; ints.push_back(dum); }
        if(s<len-1){ dum[0]=xa[len-2]; dum[1]=xa[len-1]; ints.push_back(dum); }
        for(int l=1; l<s; l++){ dum[0]=xa[l]; dum[1]=xa[l+1]; ints.push_back(dum); }
        for(int l=s+1; l<len-2; l++){ dum[0]=xa[l]; dum[1]=xa[l+1]; ints.push_back(dum); }
    }
    
    //for(int l=0; l<(int)ints.size(); l++) cout << ints[l][0] << " " << ints[l][1] << endl;
    //wait_f_r();
    
    return;
}

double dsigma_dk_Dxi_I2_p_regions(double lDmu21, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.phi=-10.0;
    double Dmu21=exp(lDmu21);
    IV->V.Dmu21=Dmu21;
    IV->V.mu2=IV->V.mu1+Dmu21;
    IV->V.compute_derived_values();
    
    double Dximin=P.epsrel/10.0*IV->V.w;
    double lDximin=log(Dximin), lDximax=log(max(IV->V.Dxitot, Dximin));
    
    vector<vector<double> > ints;
    generate_intervals_xi(lDximin, lDximax, ints);
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI;
    
    for(int k=0; k<(int)ints.size(); k++)
    {
        lDximin=ints[k][0]; lDximax=ints[k][1];
        double f=(1.0e-300+lDximax-lDximin);
        r+=Integrate_using_Patterson_adaptive(lDximin, lDximax, epsrel, epsabs/f, dsigma_dk_Dxi_I3, params);

        //cout << exp(lDximin) << " " << exp(lDximax) << " " << r << " " << epsabs << endl;
        epsabs=max(epsabs, epsrel*fabs(r)/10.0);
    }
    
    return Dmu21*r;
}

double dsigma_dk_Dxi_I2_m_regions(double lDmu21, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.phi=-10.0;
    double Dmu21=exp(lDmu21);
    IV->V.Dmu21=-Dmu21;
    IV->V.mu2=IV->V.mu1-Dmu21;
    IV->V.compute_derived_values();
    
    double Dximin=P.epsrel/10.0*IV->V.w;
    double lDximin=log(Dximin), lDximax=log(max(IV->V.Dxitot, Dximin));
    
    vector<vector<double> > ints;
    generate_intervals_xi(lDximin, lDximax, ints);
    
    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int1_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*TWOPI;
    
    for(int k=0; k<(int)ints.size(); k++)
    {
        lDximin=ints[k][0]; lDximax=ints[k][1];
        double f=(1.0e-300+lDximax-lDximin);
        r+=Integrate_using_Patterson_adaptive(lDximin, lDximax, epsrel, epsabs/f, dsigma_dk_Dxi_I3, params);
        
        //cout << exp(lDximin) << " " << exp(lDximax) << " " << r << " " << epsabs << endl;
        epsabs=max(P.epsabs, epsrel*fabs(r)/10.0);
    }
    
    return Dmu21*r;
}

//--------------------------------------------------------------------------------------------------
double dsigma_dk_Dxi_I1(double mu1, void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();
    IV->V.mu1=mu1;

    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z)/P.Int2_fac, r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*FOURPI;

    double Dmu21min=P.epsrel/10.0*IV->V.w, f;
    
    f=(1.0e-300+log(max(1.0-mu1, Dmu21min))-log(Dmu21min));
    r+=Integrate_using_Patterson_adaptive(log(Dmu21min), log(max(1.0-mu1, Dmu21min)),
                                          //epsrel, epsabs/f, dsigma_dk_Dxi_I2_p, params);
                                          epsrel, epsabs/f, dsigma_dk_Dxi_I2_p_regions, params);

    f=(1.0e-300+log(max(1.0+mu1, Dmu21min))-log(Dmu21min));
    r+=Integrate_using_Patterson_adaptive(log(Dmu21min), log(max(1.0+mu1, Dmu21min)),
                                          //epsrel, epsabs/f, dsigma_dk_Dxi_I2_m, params);
                                          epsrel, epsabs/f, dsigma_dk_Dxi_I2_m_regions, params);

    return r;
}

//--------------------------------------------------------------------------------------------------
double dsigma_dk_Dxi_I0(void *p)
{
    Integration_variables *IV=((Integration_variables *) p);
    Precision_info P=IV->Get_Precision_info();

    void *params=IV;
    double epsrel=relative_precision_estimate(P.epsrel, IV->V, IV->Z), r=0.0;
    double epsabs=absolute_precision_estimate(epsrel, IV->V, IV->Z)*2.0*FOURPI;
    
    r+=Integrate_using_Patterson_adaptive(-1.0, 1.0, epsrel, epsabs, dsigma_dk_Dxi_I1, params);

    return TWOPI*r;
}

//==================================================================================================
//==================================================================================================
