//==================================================================================================
void test_functions()
{
    Momenta_and_Angles V(0.5, 0.01, 0.0);
    V.mu2=0.2;
    V.phi=1.5;
    cout << dsigma_dkdOmega1dOmega2(V, 1, "BH") << " " << dsigma_dkdOmega1dOmega2(V, 1, "EH") << endl;

    cout << dsigma_dkdOmega1(0.5, 0.01, 0.0, 1, "EH") << endl;
    cout << dsigma_dkdOmega1(0.5, 0.01, 0.0, 1, "BH") << endl;
    cout << dsigma_dk(0.5, 0.001, 1, "EH") << endl;
    cout << dsigma_dk(0.5, 0.001, 1, "BH") << endl;
    
    cout << dsigma_dk(0.5, 0.01, 1, "BH") << endl;
    cout << dsigma_dk_sel(0.5, 0.01, 1, "BH") << endl;
    
    exit(0);
}

void test_Gaunt_factor_interpolation()
{
    //==============================================================================================
    // testing the interpolation
    //==============================================================================================
    cout << Gaunt_interpolated_NR(0.01, 0.00001, 1) << endl;
    cout << Gaunt_interpolated_NR(0.001, 0.0000001, 1) << endl;
    cout << Gaunt_interpolated_NR(0.0001, 0.000000001, 1) << endl;
    cout << Gaunt_interpolated_NR(1, 0.0001, 1) << endl;
    cout << endl;
    
    output_interpolated_Gaunt_factors("./outputs/Gaunt_NR_interpol.dat", 1.0e-10, "NR", 200);
    
    double Th_e=0.00001;
    output_thermally_averaged_Gaunts("./outputs/NR-th_av_Z1.dat",
                                     Th_e, 1, 1.0e-10, 1.0e+10, "NR", "Itoh", 200);
    
    //==============================================================================================
    // thermally-averaged Gaunt factors like in van Hoof
    //==============================================================================================
    for(int k=0; k<=7; k++)
    {
        int Zsel=1+k*5;
        double Itoh_gamma2=100.0;
        double Th_e=Zsel*Zsel*const_EH_inf/(1.0e+3*const_me)/Itoh_gamma2;
        
        output_thermally_averaged_Gaunts("./outputs/NR-th_av_Z_"+int_to_string(Zsel)+".dat",
                                         Th_e, Zsel, 1.0e-10, 1.0e+10, "NR", "Itoh", 100);
    }
    
    exit(0);
}

void test_Gaunt_factor_Z_scaling()
{
    cout.precision(10);
    double p=0.05, w=1.0e-5;
    cout << " Z = 1: " << dsigma_dk_sel(p, w*Emax_func(p), 1, "NR") << " "
         << dsigma_dk_sel(p, w*Emax_func(p), 1, "NR-interpol") << endl;
    cout << " Z = 2: " << dsigma_dk_sel(p, w*Emax_func(p), 2, "NR") << " "
         << dsigma_dk_sel(p, w*Emax_func(p), 2, "NR-interpol") << endl;
    cout << " Z = 4: " << dsigma_dk_sel(p, w*Emax_func(p), 4, "NR") << " "
         << dsigma_dk_sel(p, w*Emax_func(p), 4, "NR-interpol") << endl;
    cout << " Z = 10: " << dsigma_dk_sel(p, w*Emax_func(p), 10, "NR") << " "
         << dsigma_dk_sel(p, w*Emax_func(p), 10, "NR-interpol") << endl;
    exit(0);
}

void test_G0_ODE_solver()
{
    double p1v=1.0e-4, omegav=9.999e-1*Emax_func(p1v), Zv=1.0;
    double gamma1v=gamma_func(p1v);
    double p2v=sqrt(p1v*p1v+omegav*(omegav-2.0*gamma1v));
    double ainf=const_alpha*Zv;
    double a=ainf*gamma1v/p1v, b=ainf*gamma_func(p2v)/p2v, xf=-4.0*a*b/pow(a-b, 2);
    double G0x, DG0x;
    
    G0x_function(a, b, a-b, xf, G0x, DG0x);
    
    cout << -G0x*DG0x * sqrt(3.0)/TWOPI * F_NR(a, b) * pow((a+b)/(a-b), 2) << endl;
    cout << Gaunt_sel(p1v, omegav, Zv, "NR-interpol") << endl;
    
    exit(1);
}

void test_cross_sections()
{
    double p1v=0.1, p2v=0.00009, mu1=0.1, mu2=-0.5, phi=3.0;
    double omegav=gamma_func(p1v)-gamma_func(p2v);
    
    cout << " Testing cross sections. Remember to remove factors of 2 dphi/dxi for xi variable cases " << endl;
    
    cout.precision(16);
    cout << "\n EH-I \t" << dsigma_dkdOmega1dOmega2(p1v, omegav, mu1, mu2, phi, 1, "EH-I")
             /dsigma_dk_K(p1v, omegav, 1) << endl;
    
    cout << " EH \t" << dsigma_dkdOmega1dOmega2(p1v, omegav, mu1, mu2, phi, 1, "EH")
             /dsigma_dk_K(p1v, omegav, 1) << endl;
    
    Momenta_and_Angles VVB;
    VVB.set_values(p1v, omegav, mu1, mu2, phi);
    double Dxi=VVB.compute_Dxi(phi);
    selected_variables="xi";
    cout << " EH-xi \t" << dsigma_dkdOmega1dOmega2_xi(p1v, omegav, mu1, mu2-mu1, Dxi, 1, "EH")
             /dsigma_dk_K(p1v, omegav, 1) << endl;

    cout << "\n BH-xi \t" << dsigma_dkdOmega1dOmega2_xi(p1v, omegav, mu1, mu2-mu1, Dxi, 1, "BH")
             /dsigma_dk_K(p1v, omegav, 1) << endl;

    selected_variables="default";
    cout << " BH \t" << dsigma_dkdOmega1dOmega2(p1v, omegav, mu1, mu2, phi, 1, "BH")
             /dsigma_dk_K(p1v, omegav, 1) << endl;
    
    exit(1);
}

//==================================================================================================
//==================================================================================================
