//==================================================================================================
//
// cross section functions (very close to original functions)
//
//==================================================================================================
struct EF_vars
{
    double E1, E2, E3;
    double F1, F2, F3;
    double J1, J2, J3;
    double Delta1, Delta2;
    double D1, D2;

    EF_vars()
    {
        E1=E2=E3=0.0;
        F1=F2=F3=0.0;
        J1=J2=J3=0.0;
        Delta1=Delta2=0.0;
        D1=D2=0.0;
    }
};

double E12_func(double tau2, double omega, double kap1, double kap2,
                double zeta1, double h12, double h22)
{
    return tau2*h12 + (zeta1 + 2.0*(1.0+h22)*omega/kap2)*kap1*omega;
}

double E3_func(double tau12, double omega, double kap1, double kap2,
               double zeta1, double zeta2, double h1h2)
{
    return tau12*h1h2 + 2.0*(1.0+h1h2)*omega*omega + ( kap2*zeta1 - kap1*zeta2 )*omega/2.0;
}

//==================================================================================================
double F12_func(double omega, double pi1, double pi2, double pi12,
                double zeta1, double p1, double p2, double rho, double kappa)
{
    return rho*zeta1
           + kappa*(pi1*(pi12 + p2*p2) + (2.0-pi1*pi2)*omega)
           - (kappa*p1*p2 - 2.0*omega/p1)*(pi1 + pi2 - omega);
}

double F3_func(double gamma1, double gamma2, double pi1, double pi2,
               double p1, double p2, double rho, double mu)
{
    double g1pg2=gamma1+gamma2, p1tp2=p1*p2;
    return mu*( 1.0-pi1*pi2/p1tp2 + g1pg2/p1tp2*(g1pg2+pi1+pi2)/p1tp2 ) -2.0*rho*rho;
}

void update_EF_vars(double p1, double p2, double gamma1, double gamma2, double omega,
                    double kap1, double kap2,
                    double mu1, double mu2, double phi,
                    double pi1, double pi2, double pi12, double q2, double mu,
                    EF_vars &EFV)
{
    double kappa=gamma1/p1+gamma2/p2, rho=1.0/p1+1.0/p2;
    double S1=sqrt(1.0-mu1*mu1), S2=sqrt(1.0-mu2*mu2);
    double chi1=p1*S1, chi2=p2*S2, chi12=p1*p2*cos(phi)*S1*S2;
    double zeta1=chi1*chi1-chi12, zeta2=chi2*chi2-chi12;
    double tau1=4.0*gamma1*gamma1-q2, tau2=4.0*gamma2*gamma2-q2, tau12=4.0*gamma1*gamma2-q2;
    
    EFV.E1=E12_func(tau2, omega, kap1, kap2, zeta1, chi1*chi1, chi2*chi2);
    EFV.E2=E12_func(tau1, -omega, kap2, kap1, zeta2, chi2*chi2, chi1*chi1);
    EFV.E3=E3_func (tau12, omega, kap1, kap2, zeta1, zeta2, chi12);
    
    EFV.F1=F12_func( omega, pi1, pi2, pi12, zeta1, p1, p2, rho, kappa);
    EFV.F2=F12_func(-omega, pi2, pi1, pi12, zeta2, p2, p1, rho, kappa);
    EFV.F3=F3_func ( gamma1, gamma2, pi1, pi2, p1, p2, rho, mu);
    
    return;
}

//==================================================================================================
work_space_Hyper_Geo_2F1 WV, WW;

int Eval_A1A2B_func(double a1, double a2, double ainf, double xi,
                    double &A12, double &A22, double &B2,
                    double &A1cA2, double &A1cB, double &A2cB)
{
    complex<double> V, W, I(0.0, 1.0);
    int errV=Hyper_Geo_2F1(I*a1, I*a2, 1, 1.0-xi, -PI*a1, V, WV);
    int errW=Hyper_Geo_2F1(1.0+I*a1, 1.0+I*a2, 2, 1.0-xi, -PI*a1, W, WW);
    
    // return A1, A2 and B
    complex<double> A1=V - I*a1*xi*W;
    complex<double> A2=V - I*a2*xi*W;
    complex<double> B =    I*ainf *W;
    
    A12=norm(A1); A22=norm(A2); B2=norm(B);
    A1cA2=real(conj(A1)*A2);
    A1cB =real(conj(A1)*B );
    A2cB =real(conj(A2)*B );

    return errV+errW;
}

//==================================================================================================
// Elwert-Haug soft photon approximation (omega/Emax << 1)
//==================================================================================================
double dsigma_dkdOmega1dOmega2_EH_soft(Momenta_and_Angles &V, double y, double a1, int Z)
{
    double dsigma_BH=dsigma_dkdOmega1dOmega2_BH(V, Z);
    // phi(a1) in Haug is directly srelated to 2*Re[H(i a1)]
    double corr=1.0-2.0*a1*a1/y * (1.0 + log(y) - 2.0*Harmonic_number_Re(a1));
    
    return dsigma_BH*corr;
}

//==================================================================================================
// exact Elwert-Haug differential cross section
//==================================================================================================
double dsigma_dkdOmega1dOmega2_I(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_I:: angles not correct" << endl;

    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;
    double mu=pow(V.p1+V.p2, 2)-pow(V.omega, 2);
    
    double mu12=V.mu1*V.mu2+cos(V.phi)*sqrt(1.0-V.mu1*V.mu1)*sqrt(1.0-V.mu2*V.mu2);
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2, pi12=V.p1*V.p2*mu12;
    
    double q2=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-pi12);
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    double kap1=2.0*(V.gamma1-pi1), kap2=2.0*(V.gamma2-pi2);
    double xi=mu*q2/(kap1*kap2*V.omega*V.omega);

    //----------------------------------------------------------------------------------------------
    // general cross section evaluation
    //----------------------------------------------------------------------------------------------
    EF_vars EFV;
    update_EF_vars(V.p1, V.p2, V.gamma1, V.gamma2, V.omega, kap1, kap2,
                   V.mu1, V.mu2, V.phi, pi1, pi2, pi12, q2, mu, EFV);
    
    //----------------------------------------------------------------------------------------------
    // Imaginary functions
    //----------------------------------------------------------------------------------------------
    double A12, A22, B2, A1cA2, A1cB, A2cB;
    Eval_A1A2B_func(a1, a2, ainf, xi, A12, A22, B2, A1cA2, A1cB, A2cB);
    
    //----------------------------------------------------------------------------------------------
    // cross section evaluation
    //----------------------------------------------------------------------------------------------
    double norm=F_NR(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    double M2=(EFV.E1*A12/pow(kap1, 2)+EFV.E2*A22/pow(kap2, 2)-2.0*EFV.E3*A1cA2/(kap1*kap2)
               +( EFV.F3*B2*q2-2.0*(EFV.F1*A1cB*kap2+EFV.F2*A2cB*kap1) )*q2/pow(kap1*kap2, 2)
               )/(q2*q2);
    
    return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
//
// exact Elwert-Haug differential cross section rewritten
//
//==================================================================================================
void update_J_vars(double ainf, double a1, double a2, double Da,
                   double p1, double p2, double gamma1, double gamma2, double omega,
                   double kap1, double kap2,
                   double mu1, double mu2, double phi,
                   double pi1, double pi2, double pi12, double q2, double mu,
                   EF_vars &EFV)
{
    double kappa=gamma1/p1+gamma2/p2, rho=1.0/p1+1.0/p2;
    double S1=sqrt(1.0-mu1*mu1), S2=sqrt(1.0-mu2*mu2);
    double chi1=p1*S1, chi2=p2*S2, chi12=p1*p2*cos(phi)*S1*S2;
    double zeta1=chi1*chi1-chi12, zeta2=chi2*chi2-chi12;
    double tau1=4.0*gamma1*gamma1-q2, tau2=4.0*gamma2*gamma2-q2, tau12=4.0*gamma1*gamma2-q2;
    double o2=omega*omega, kap12=kap1*kap2;
    double fac=ainf*o2/mu;

    EFV.J1=tau1*pow(chi2/kap2,2)+tau2*pow(chi1/kap1,2)-2.0*tau12*chi12/kap12
              +2.0*(zeta1+zeta2)*o2/kap12;

    EFV.J2=tau1*a2*pow(chi2/kap2,2)+tau2*a1*pow(chi1/kap1,2)-tau12*(a1+a2)*chi12/kap12
              +2.0*(a1*zeta2+a2*zeta1)*o2/kap12;

    EFV.J3=tau1*pow(a2*chi2/kap2,2)+tau2*pow(a1*chi1/kap1,2)-2.0*tau12*a1*a2*chi12/kap12
              +2.0*(a2*a2*zeta1+a1*a1*zeta2+Da*Da*(1.0+chi12))*o2/kap12;

    EFV.F1=F12_func( omega, pi1, pi2, pi12, zeta1, p1, p2, 0, kappa); // canceled rho terms
    EFV.F2=F12_func(-omega, pi2, pi1, pi12, zeta2, p2, p1, 0, kappa); // canceled rho terms
    EFV.F3=F3_func ( gamma1, gamma2, pi1, pi2, p1, p2, rho, mu);

    EFV.J2+=    fac*(     EFV.F1/kap1+   EFV.F2/kap2); // JC: there was a factor of 2 too many...
    EFV.J3+=2.0*fac*( (a1*EFV.F1/kap1+a2*EFV.F2/kap2) + fac*EFV.F3/2.0 );

    return;
}

//==================================================================================================
int Eval_V2W2ImVsW_func(double a1, double a2, double ainf, double xi,
                        double &V2, double &W2, double &ImVsW)
{
    complex<double> V, W, I(0.0, 1.0);
    int errV=Hyper_Geo_2F1(I*a1, I*a2, 1, 1.0-xi, -PI*a1, V, WV);
    int errW=Hyper_Geo_2F1(1.0+I*a1, 1.0+I*a2, 2, 1.0-xi, -PI*a1, W, WW);

    V2=norm(V);
    W2=norm(W)*xi*xi;
    ImVsW=-imag(conj(V)*W)*xi;

    return errV+errW;
}

int Eval_V2W2ImVsW_func_G0(double a1, double a2, double ainf, double Da1a2, double xi,
                           double &V2, double &W2, double &ImVsW)
{
    complex<double> V, W, I(0.0, 1.0);
    double G0, etapb=(a1+a2)/2.0;
    int errV=Hyper_Geo_2F1(I*a1, I*a2, 1, 1.0-xi, -PI*a1, V, WV);
    int errG=G_ell_function(a1, a2, Da1a2, 0, 1.0-xi, G0);
    
    V2=norm(V);
    W2=pow(G0 * xi/(xi-1.0), 2);
    ImVsW=( imag(V)*cos(etapb*log(xi))+real(V)*sin(etapb*log(xi)) ) * G0 * xi/(xi-1.0);
    
    return errV+errG;
}

#include "Bremsstrahlung-EH-interpol.cpp"

//==================================================================================================
double dsigma_dkdOmega1dOmega2(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2:: angles not correct" << endl;
    
    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;
    double mu=pow(V.p1+V.p2, 2)-pow(V.omega, 2);
    
    double mu12=V.mu1*V.mu2+cos(V.phi)*sqrt(1.0-V.mu1*V.mu1)*sqrt(1.0-V.mu2*V.mu2);
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2, pi12=V.p1*V.p2*mu12;
    
    double q2=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-pi12);
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    double kap1=2.0*(V.gamma1-pi1), kap2=2.0*(V.gamma2-pi2);
    double xi=mu*q2/(kap1*kap2*V.omega*V.omega);
    double Da=Deta1eta2(V, Z);
    
    //----------------------------------------------------------------------------------------------
    // general cross section evaluation
    //----------------------------------------------------------------------------------------------
    EF_vars EFV;
    update_J_vars(ainf, a1, a2, Da, V.p1, V.p2, V.gamma1, V.gamma2, V.omega, kap1, kap2,
                  V.mu1, V.mu2, V.phi, pi1, pi2, pi12, q2, mu, EFV);

    //----------------------------------------------------------------------------------------------
    // Imaginary functions
    //----------------------------------------------------------------------------------------------
    double V2, W2, ImVsW;
    if(V.info=="EH-interpol" && V.omega/V.E1<0.1)
    {
        arm_V_W_ImVsW_interpolation(V, Z);
        Eval_V2W2ImVsW_interpol(xi, V2, W2, ImVsW);
    }
    else Eval_V2W2ImVsW_func(a1, a2, ainf, xi, V2, W2, ImVsW);
    //else Eval_V2W2ImVsW_func_G0(a1, a2, ainf, Da, xi, V2, W2, ImVsW);

    //----------------------------------------------------------------------------------------------
    // cross section evaluation
    //----------------------------------------------------------------------------------------------
    double norm=F_NR(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    double M2=-2.0*EFV.J2*ImVsW;
    M2+= EFV.J3*W2;
    if(V.info=="DEH") M2+=EFV.J1*(V2 - F_EWF(a1, a2)/F_NR(a1, a2));
    else M2+=EFV.J1*V2;
    M2*= 1.0/(q2*q2);
    
    //return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

double dsigma_dkdOmega1dOmega2_DEH(Momenta_and_Angles &V, int Z)
{
    V.info="DEH";
    return dsigma_dkdOmega1dOmega2(V, Z);
}

double dsigma_dkdOmega1dOmega2_int(Momenta_and_Angles &V, int Z)
{
    V.info="EH-interpol";
    return dsigma_dkdOmega1dOmega2(V, Z);
}

//==================================================================================================
double dsigma_dkdOmega1dOmega2_soft(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_soft:: angles not correct" << endl;
    
    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;
    double kappa=V.gamma1/V.p1+V.gamma2/V.p2, rho=1.0/V.p1+1.0/V.p2;
    double mu=pow(V.p1+V.p2, 2)-pow(V.omega, 2);
    
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2;
    double S1=sqrt(1.0-V.mu1*V.mu1), S2=sqrt(1.0-V.mu2*V.mu2);
    double mu12=V.mu1*V.mu2+cos(V.phi)*S1*S2;
    double pi12=V.p1*V.p2*mu12;
    
    //double q2=V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-pi12);
    double q2=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-pi12);
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    double o2=V.omega*V.omega, h1=V.p1*S1, h2=V.p2*S2, h1h2=V.p1*V.p2*cos(V.phi)*S1*S2;
    double kap1=2.0*(V.gamma1-pi1), kap2=2.0*(V.gamma2-pi2), kap12=kap1*kap2;
    //double xi=mu*q2/(kap1*kap1*o2);
    double xi=mu*q2/(kap1*kap2*o2);

    double tau1=4.0*V.gamma1*V.gamma1-q2, tau2=4.0*V.gamma2*V.gamma2-q2, tau12=4.0*V.gamma2*V.gamma1-q2;
    double zeta1=h1*h1-h1h2, zeta2=h2*h2-h1h2;
    double Da=Deta1eta2(V, Z);
    
    //----------------------------------------------------------------------------------------------
    // general cross section evaluation
    //----------------------------------------------------------------------------------------------
    // quasi-exact still
    //double J1=tau1*pow(h2/kap2,2)+tau2*pow(h1/kap1,2)-2.0*tau12*h1h2/kap12;
    //double DJ2=-Da*(tau1*pow(h2/kap2,2)-tau12*h1h2/kap12);
    //double DJ3=-2.0*a1*Da*(tau1*pow(h2/kap2,2)-tau12*h1h2/kap12) + Da*Da*tau1*pow(h2/kap2,2);
    // more approximations
    double J1=(pow(h2,2)+pow(h1,2)-2.0*h1h2)*tau1/pow(kap1,2);
    double DJ2=-Da*(pow(h2,2)-h1h2)*tau1/pow(kap1,2);
    double DJ3=( -2.0*a1*Da*(pow(h2,2)-h1h2) + Da*Da*pow(h2,2) )*tau1/pow(kap1,2);

    //----------------------------------------------------------------------------------------------
    // Imaginary functions
    //----------------------------------------------------------------------------------------------
    double V2, W2, ImVsW;
    //double q2eval=V.omega*V.omega+pow(V.p1-V.p2, 2)+2.0*V.omega*V.mu1*(V.p2-V.p1);
    double q2eval=V.omega*V.omega+V.p1*V.p1+V.p2*V.p2+2.0*(V.omega*(pi2-pi1)-(V.mu1*V.mu2+S1*S2)*V.p1*V.p2);
    double xieval=mu*q2eval/(kap1*kap1*o2);
    if(Intpol_mem_index.size()==0 || V.omega/V.E1>0.1)
        Eval_V2W2ImVsW_func(a1, a2, ainf, xieval, V2, W2, ImVsW);
    //    Eval_V2W2ImVsW_func_G0(a1, a2, ainf, Da, xi, V2, W2, ImVsW);
    else Eval_V2W2ImVsW_interpol(xieval, V2, W2, ImVsW);

    //W2=exp(-TWOPI*a1)*(1.0-(1.0+a1*a2)*(1.0-xi)); ImVsW=exp(-TWOPI*a1)*(a1+a2)/2.0*(1.0-xi);
    ////V2=one_minus_exp_mx(TWOPI*a1)*one_minus_exp_mx(TWOPI*a2)*(a1+a2)/a1/a2/TWOPI/one_minus_exp_mx(TWOPI*(a1+a2));
    //V2=exp(-TWOPI*a1)*(1.0-2.0*a1*a2*(1.0-xi));
    
    //double f=pow(one_minus_exp_mx(TWOPI*a1)/(TWOPI*a1), 2), ReH=Harmonic_number_Re(a1);
    //cout << " xi= " << xi << " " << xieval << endl;
    //cout << " V2= " << V2 << " " << f*(1.0+pow(a1*(2.0*ReH-log(xi-1.0)), 2)) << endl;
    //cout << " W2= " << W2 << " " << f*pow((2.0*ReH-log(xi-1.0)), 2) << endl;
    //cout << " ImVsW= " << ImVsW << " " << f*a1*pow((2.0*ReH-log(xi-1.0)), 2) << endl;

    //W2=f*pow((2.0*ReH-log(xi-1.0)), 2);
    //V2=f+a1*a1*W2;
    //ImVsW=a1*W2;
    //DJ2=DJ3=0.0;

    //----------------------------------------------------------------------------------------------
    // cross section evaluation
    //----------------------------------------------------------------------------------------------
    double norm=F_NR(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    double M2=J1*(V2-2.0*a1*ImVsW+a1*a1*W2)/(q2*q2);
    M2+=(-2.0*DJ2*ImVsW+DJ3*W2)/(q2*q2);
    
    //double M2=J1*f/(q2*q2);
    //M2+=(-2.0*DJ2*ImVsW+DJ3*W2)/(q2*q2);
    //cout << xi << " " << Da << " " << norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z) << endl;

    return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
//
// exact Elwert-Haug differential cross section rewritten using Dxi variables
//
//==================================================================================================
int Eval_V2W2ImVsW_func_xi(double a1, double a2, double ainf,
                           Momenta_and_Angles &VV, int do_interpol,
                           double Da, int Z,
                           double &A2, double &W2, double &C2)
{
    double xi=VV.xi, p1=VV.p1, w=VV.w;
    
    int verbose=0;
    double xi_interpol=(do_interpol==1 ? 1.3 : VV.xi_interpol);
    double w_switch=1.0e-6; // no interpolation above this value of w

    if(do_interpol==1 && xi_interpol<xi && w<=w_switch)
    {
        W2=W2_interpolated(xi, p1, Z);
        A2=A2_interpolated(xi, p1, Z);
        C2=W2*(a1+a2+Da*xi)/2.0/xi/a1;
        
        if(verbose>1)
            cout << " interpol-I" << xi << " " << W2 << " " << A2 << " " << C2 << endl;
    }
    else if(do_interpol>1 && 1.0+1.0e-8<xi && xi<=xi_interpol && w<=w_switch)
    {
        arm_V_W_ImVsW_interpolation_xi(VV, Z, "A2W2");
        Eval_A2W2_interpol_xi(xi, A2, W2, verbose);
        C2=(a1+a2+Da*xi)/(2.0*xi*a1) * W2;

        if(verbose>1)
            cout << " ODE-int   " << xi << " " << W2 << " " << A2 << " " << C2 << endl;
    }
    // use asymptotic expressions for w<<1 and xi>>1
    else if(do_interpol>1 && xi_interpol<xi && w<=w_switch)
    {
        W2=W2_approx_high(xi, p1, Z);
        A2=A2_approx_high(xi, p1, Z);
        C2=W2*(a1+a2+Da*xi)/2.0/xi/a1;

        if(verbose>1)
            cout << " asymp    " << xi << " " << W2 << " " << A2 << " " << C2 << endl;
    }
    else
    {
        complex<double> V, W, I(0.0, 1.0);
        Hyper_Geo_2F1(I*a1, I*a2, 1, 1.0-xi, -PI*a1, V, WV);
        Hyper_Geo_2F1(1.0+I*a1, 1.0+I*a2, 2, 1.0-xi, -PI*a1, W, WW);
        
        double V2=norm(V);
        double ImVsW=-imag(conj(V)*a1*xi*W);
        W2= norm(a1*xi*W);
        A2= V2-2.0*ImVsW+W2;
        C2= W2-ImVsW;
        
        if(verbose>0)
            cout << " direct   " << xi << " " << W2 << " " << A2 << " " << C2 << endl;
    }
    
    return 0;
}

void update_J_vars_xi(double q2, double ainf, double a1, double a2,
                      const Momenta_and_Angles &V, EF_vars &EFV)
{
    double kappa=V.gamma1/V.p1+V.gamma2/V.p2, rho=1.0/V.p1+1.0/V.p2;
    double chi1=V.p1*V.S1, chi2=V.p2*V.S2;

    double f0=V.gamma2*chi1/V.kap1-V.gamma1*chi2/V.kap2, f1=chi1/V.kap1-chi2/V.kap2;
    double tau1=4.0*V.gamma1*V.gamma1-q2, tau12=4.0*V.gamma1*V.gamma2-q2;
    double chi12=chi1*chi2*(1.0-2.0*V.Dxi/V.Dxitot);
    double zeta1=chi1*chi1-chi12, zeta2=chi2*chi2-chi12;
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2, pi12=pi1*pi2+chi12;
    double fac=ainf/V.mut/a1;
    double facb=V.omega/2.0/rho;

    EFV.F1=F12_func( V.omega, pi1, pi2, pi12, zeta1, V.p1, V.p2, 0, kappa); // canceled rho terms
    EFV.F2=F12_func(-V.omega, pi2, pi1, pi12, zeta2, V.p2, V.p1, 0, kappa); // canceled rho terms
    EFV.F3=F3_func ( V.gamma1, V.gamma2, pi1, pi2, V.p1, V.p2, rho, V.mut*V.o2);
    
    //----------------------------------------------------------------------------------------------
    // rewritten J_i terms; first add small terms then the larger ones
    //----------------------------------------------------------------------------------------------
    EFV.J1=(zeta1+zeta2)*2.0*V.o2/V.kap12;
    EFV.J1+=tau12*V.Dxi/V.mut;
    EFV.J1+=4.0*pow(f0, 2)-pow(f1, 2)*q2;

    //----------------------------------------------------------------------------------------------
    // common terms
    //----------------------------------------------------------------------------------------------
    EFV.Delta1 = 2.0*zeta1*V.o2/V.kap12;
    EFV.Delta1+= facb*(EFV.F1/V.kap1 + EFV.F2/V.kap2);
    EFV.Delta1+= tau1*pow(chi2/V.kap2, 2)-tau12*chi12/V.kap12;

    EFV.Delta2 = (1.0+chi1*chi1)*2.0*V.o2/V.kap12;
    EFV.Delta2+= facb*facb*EFV.F3;
    EFV.Delta2+= 2.0 *facb*EFV.F2/V.kap2;
    EFV.Delta2+= tau1*pow(chi2/V.kap2, 2);

    return;
}

double dsigma_dkdOmega1dOmega2_xi_Ji(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_xi:: angles not correct" << endl;
    
    V.compute_derived_values();
    
    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;

    double Delta=V.Dxi*(V.Dxitot-V.Dxi);
    if(Delta<=0) return 0.0;
    
    double q2=V.xi*V.kap12/V.mut;
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    //----------------------------------------------------------------------------------------------
    // general cross section evaluation
    //----------------------------------------------------------------------------------------------
    double Da=Deta1eta2(V, Z);
    EF_vars EFV;
    update_J_vars_xi(q2, ainf, a1, a2, V, EFV);
    
    //----------------------------------------------------------------------------------------------
    // Imaginary functions
    //----------------------------------------------------------------------------------------------
    double A2, W2, C2;
    int do_int=(V.info=="DEH-interpol" ? 2 : 1);
    Eval_V2W2ImVsW_func_xi(a1, a2, ainf, V, do_int, Da, Z, A2, W2, C2);
    
    //----------------------------------------------------------------------------------------------
    // cross section evaluation [comments JC: factors of a1 absorbed in A2, C, W2 terms...]
    //----------------------------------------------------------------------------------------------
    double norm=F_NR(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    double M2=pow(Da/a1, 2)*EFV.Delta2*W2;
    M2+=-2.0*Da/a1*EFV.Delta1*C2;
    if(V.info=="DEH" || V.info=="DEH-interpol") M2+=EFV.J1*(A2 - F_EWF(a1, a2)/F_NR(a1, a2));
    else M2+=EFV.J1*A2;
    M2*= 1.0/pow(q2, 2);
    M2*= 2.0/sqrt(Delta); // 2*dphi2/dxi: remove when comparing cross sections
    
    //return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
// Elwert-Haug soft photon approximation (omega/Emax << 1)
//==================================================================================================
double dsigma_dkdOmega1dOmega2_xi_soft(Momenta_and_Angles &V, int Z)
{
    V.compute_derived_values();
    
    double dsigma_BH=dsigma_dkdOmega1dOmega2_BH_xi(V, Z);
    double a1=const_alpha*Z * V.gamma1/V.p1;

    // phi(a1) in Haug is directly related to 2*Re[H(i a1)]
    double corr=1.0-2.0*a1*a1/(V.xi) * (1.0 + log(V.xi) - 2.0*Harmonic_number_Re(a1));
    
    return dsigma_BH*corr;
}

//==================================================================================================
//
// exact Elwert-Haug differential cross section rewritten using Dxi variables and D1 & D2
//
//==================================================================================================
int Eval_V2W2ImVsW_func_xi_G0(double a1, double a2, double ainf,
                              Momenta_and_Angles &VV,
                              double Da, int Z,
                              double &G02, double &DG02)
{
    double xi=VV.xi, p1=VV.p1, w=VV.w;
    
    int verbose=0;
    double xi_interpol=VV.xi_interpol;
    double w_switch=1.0e-6; // no interpolation above this value of w

    if(1.0+1.0e-8<xi && xi<=xi_interpol && w<=w_switch)
    {
        arm_V_W_ImVsW_interpolation_xi(VV, Z, "G0");
        Eval_A2W2_interpol_xi(xi, DG02, G02, verbose);
        
        if(verbose>1)
            cout << " ODE-int   " << xi << " " << G02 << " " << DG02 << endl;
    }
    // use asymptotic expressions for w<<1 and xi>>1
    else if(xi_interpol<xi && w<=w_switch)
    {
        double W2=W2_approx_high(xi, p1, Z);
        double A2=A2_approx_high(xi, p1, Z);
        
        G02 =W2*pow((1.0-xi)/xi/a1, 2);
        DG02=(A2-W2*pow((a1+a2+Da*xi)/2.0/xi/a1, 2))/pow(xi, 2);

        if(verbose>1)
            cout << " asymp    " << xi << " " << G02 << " " << DG02 << endl;
    }
    else
    {
        complex<double> V, W, I(0.0, 1.0);
        Hyper_Geo_2F1(I*a1, I*a2, 1, 1.0-xi, -PI*a1, V, WV);
        Hyper_Geo_2F1(1.0+I*a1, 1.0+I*a2, 2, 1.0-xi, -PI*a1, W, WW);
        
        double V2=norm(V);
        double ImVsW=-imag(conj(V)*a1*xi*W);
        double W2= norm(a1*xi*W);
        double A2= V2-2.0*ImVsW+W2;
        
        G02=W2*pow((1.0-xi)/xi/a1, 2);
        DG02=(A2-W2*pow((a1+a2+Da*xi)/2.0/xi/a1, 2))/pow(xi, 2);

        if(verbose>0)
            cout << " direct   " << xi << " " << G02 << " " << DG02 << endl;
    }
    
    return 0;
}

void update_J_vars_xi_G0(double q2, double ainf, double a1, double a2,
                         const Momenta_and_Angles &V, EF_vars &EFV)
{
    double kappa=V.gamma1/V.p1+V.gamma2/V.p2, rho=1.0/V.p1+1.0/V.p2;
    double chi1=V.p1*V.S1, chi2=V.p2*V.S2;
    
    double f0=V.gamma2*chi1/V.kap1-V.gamma1*chi2/V.kap2, f1=chi1/V.kap1-chi2/V.kap2;
    double tau1=4.0*V.gamma1*V.gamma1-q2, tau2=4.0*V.gamma2*V.gamma2-q2, tau12=4.0*V.gamma1*V.gamma2-q2;
    double chi12=chi1*chi2*(1.0-2.0*V.Dxi/V.Dxitot);
    double zeta1=chi1*chi1-chi12, zeta2=chi2*chi2-chi12;
    double pi1=V.p1*V.mu1, pi2=V.p2*V.mu2, pi12=pi1*pi2+chi12;
    double fac=ainf/V.mut/a1;
    double facb=V.omega/rho;
    
    EFV.F1=F12_func( V.omega, pi1, pi2, pi12, zeta1, V.p1, V.p2, 0, kappa); // canceled rho terms
    EFV.F2=F12_func(-V.omega, pi2, pi1, pi12, zeta2, V.p2, V.p1, 0, kappa); // canceled rho terms
    EFV.F3=F3_func ( V.gamma1, V.gamma2, pi1, pi2, V.p1, V.p2, rho, V.mut*V.o2);
    
    //----------------------------------------------------------------------------------------------
    // rewritten J_i terms; first add small terms then the larger ones
    //----------------------------------------------------------------------------------------------
    EFV.J1=(zeta1+zeta2)*2.0*V.o2/V.kap12;
    EFV.J1+=tau12*V.Dxi/V.mut;
    EFV.J1+=4.0*pow(f0, 2)-pow(f1, 2)*q2;
    
    //----------------------------------------------------------------------------------------------
    // common terms
    //----------------------------------------------------------------------------------------------
    EFV.D1 = 2.0*(chi1-chi2)*(chi1+chi2)*V.o2/V.kap12;
    EFV.D1+= facb*(EFV.F1/V.kap1 + EFV.F2/V.kap2);
    EFV.D1+= tau1*pow(chi2/V.kap2, 2)-tau2*pow(chi1/V.kap1, 2);
    
    EFV.D2 = (chi1*chi1+chi2*chi2+2.0*chi12+4.0)*2.0*V.o2/V.kap12;
    EFV.D2+= facb*facb*EFV.F3;
    EFV.D2+=-2.0*facb*(EFV.F1/V.kap1 - EFV.F2/V.kap2);
    EFV.D2+= tau1*pow(chi2/V.kap2, 2)+tau2*pow(chi1/V.kap1, 2)+tau12*2.0*chi12/V.kap12;

    return;
}

double dsigma_dkdOmega1dOmega2_xi_G0(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    if(err==2) cerr << " dsigma_dkdOmega1dOmega2_xi_G0:: angles not correct" << endl;
    
    V.compute_derived_values();
    
    double ainf =const_alpha*Z;
    double sigT =const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2;
    
    double Delta=V.Dxi*(V.Dxitot-V.Dxi);
    if(Delta<=0) return 0.0;
    
    double q2=V.xi*V.kap12/V.mut;
    if(q2<=0.0) return 0.0;
    
    double q=sqrt(q2);
    if(q<=V.p1-V.p2-V.omega || q>=V.p1+V.p2+V.omega) return 0.0;
    
    //----------------------------------------------------------------------------------------------
    // general cross section evaluation
    //----------------------------------------------------------------------------------------------
    double Da=Deta1eta2(V, Z);
    EF_vars EFV;
    update_J_vars_xi_G0(q2, ainf, a1, a2, V, EFV);

    //----------------------------------------------------------------------------------------------
    // Imaginary functions
    //----------------------------------------------------------------------------------------------
    double G02, DG02;
    Eval_V2W2ImVsW_func_xi_G0(a1, a2, ainf, V, Da, Z, G02, DG02);
    
    //----------------------------------------------------------------------------------------------
    // cross section evaluation [comments JC: factors of a1 absorbed in A2, C, W2 terms...]
    //----------------------------------------------------------------------------------------------
    double norm=F_NR(a1, a2) * 3.0/(8.0*PI3)*sigT * const_alpha*Z*Z * V.p2/V.p1 / V.omega;
    double ap=a1+a2, A=EFV.J1-2.0*Da/ap*V.xi*EFV.D1+pow(Da/ap*V.xi, 2)*EFV.D2;
    double M2=EFV.J1*V.xi*V.xi*DG02;
    if(V.info=="DEH" || V.info=="DEH-interpol") M2+=-EFV.J1*F_EWF(a1, a2)/F_NR(a1, a2);
    M2+=A*pow(ap/2.0/(1.0-V.xi), 2) * G02;
    M2*= 1.0/pow(q2, 2);
    M2*= 2.0/sqrt(Delta); // 2*dphi2/dxi: remove when comparing cross sections
    
    //return norm*max(0.0, M2)*norm_function(V.p1, V.omega, Z);
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
double dsigma_dkdOmega1dOmega2_xi(Momenta_and_Angles &V, int Z)
{
    //return dsigma_dkdOmega1dOmega2_xi_Ji(V, Z);
    return dsigma_dkdOmega1dOmega2_xi_G0(V, Z);
}

double dsigma_dkdOmega1dOmega2_xi_DEH(Momenta_and_Angles &V, int Z)
{
    V.info="DEH";
    //return dsigma_dkdOmega1dOmega2_xi(V, Z);
    return dsigma_dkdOmega1dOmega2_xi_G0(V, Z);
}

double dsigma_dkdOmega1dOmega2_xi_DEH_int(Momenta_and_Angles &V, int Z)
{
    V.info="DEH-interpol";
    //return dsigma_dkdOmega1dOmega2_xi(V, Z);
    return dsigma_dkdOmega1dOmega2_xi_G0(V, Z);
}

//==================================================================================================
// Elwert-Haug soft photon approximation (omega/Emax << 1) using extrapolation
//==================================================================================================
double Gaunt_extrapolated_EH_soft(double p1, double omega, int Z)
{
    // this routine assumes g~sqrt(3)/(2 pi) * FEW (eta1, eta2) * A * (log(-x) - B)
    
    // get two evaluations of standard Gaunt-factor at two frequencies
    double E1=Emax_func(p1);
    double omega1=1.0e-6*Emax_func(p1);
    double omega2=1.0e-8*Emax_func(p1);
    double gamma1=gamma_func(p1);
    double ainf =const_alpha*Z, a1=ainf*gamma1/p1;
    
    Momenta_and_Angles V1(p1, omega1, 0.0);
    double Da1a2_1=Deta1eta2(V1, Z), a2_1=a1-Da1a2_1;
    double y1=4.0*a1*a2_1/pow(Da1a2_1, 2);
    double F1=F_EWF(a1, a2_1);
    
    Momenta_and_Angles V2(p1, omega2, 0.0);
    double Da1a2_2=Deta1eta2(V2, Z), a2_2=a1-Da1a2_2;
    double y2=4.0*a1*a2_2/pow(Da1a2_2, 2);
    double F2=F_EWF(a1, a2_2);
    
    double G1=Gaunt_interpolated_EH(p1, omega1, Z)/(sqrt(3)/TWOPI*F1);
    double G2=Gaunt_interpolated_EH(p1, omega2, Z)/(sqrt(3)/TWOPI*F2);
    
    double A=(G2-G1)/log(y2/y1);
    double B=(G2*log(y1)-G1*log(y2))/(G2-G1);
    
    Momenta_and_Angles V(p1, omega, 0.0);
    double Da1a2=Deta1eta2(V, Z), a2=a1-Da1a2;
    double y=4.0*a1*a2/pow(Da1a2, 2);
    double F=F_EWF(a1, a2);
    
    return sqrt(3)/TWOPI*F*A*(log(y)-B);
}

//==================================================================================================
//==================================================================================================
