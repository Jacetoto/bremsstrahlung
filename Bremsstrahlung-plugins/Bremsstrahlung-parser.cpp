//==================================================================================================
struct Run_Parameters
{
    string path, add;
    int verbosity;
    int Z, nw, np, p1start, p1end;
    double wmin, wval;
    double p1min, p1max;
    vector<double> p1vals;
    double Th_e, The_min, The_max;
    double xmin, xmax;
    int nx, nThe, nthreads;
    string low_freq_integrator, high_freq_integrator;
    string sel;            // which cross section to use
    string therm_av_limit; // which thermal averaging limit to use
    string batchmode;      // set to 'plot' for convenience
    
    Run_Parameters()
    {
        path="./outputs";
        add=".dat";
        verbosity=0;
        Z=1;
        nw=np=p1start=p1end=0;
        wmin=1.0e-6;
        wval=0.01;
        p1min=0.001;
        p1max=1.0;
        Th_e=0.01;
        nx=nThe=0; nthreads=1;
        The_min=1.0e-5; The_max=0.1;
        xmin=0.01; xmax=50.0;
        low_freq_integrator="Patterson-xi";
        high_freq_integrator="CUBA-xi";
        sel="DEH";
        therm_av_limit="exact";
        batchmode="table";
    }
};

void read_runfile_info(string rfname, Run_Parameters &inp)
{
    //==============================================================================================
    // read values (if given) from file
    //==============================================================================================
    int ival;
    bool found, show_params=(inp.verbosity>0 ? 1 : 1);
    double dval;
    vector<double> dvals;
    string sval;
    
    cout << " read_runfile_info:: reading runfile " << rfname << endl << endl;
    
    file_content pfc;
    parser_read_file(rfname, pfc, 0);
    
    //==============================================================================================
    // path and add name
    //==============================================================================================
    parser_read(pfc, "output directory", sval, found, show_params);
    if(found) inp.path=sval;

    parser_read(pfc, "addition to name", sval, found, show_params);
    if(found) inp.add=sval;

    //==============================================================================================
    // reset verbosity level
    //==============================================================================================
    parser_read(pfc, "verbosity level Bremsstrahlung", ival, found, show_params);
    if(found) inp.verbosity=ival;
    
    //==============================================================================================
    // parameters
    //==============================================================================================
    parser_read(pfc, "Nuclear charge Z", ival, found, show_params);
    if(found) inp.Z=ival;
    
    parser_read_darr(pfc, "p1 values", dvals, found, show_params);
    if(found) inp.p1vals=dvals;

    parser_read(pfc, "w value", dval, found, show_params);
    if(found) inp.wval=dval;

    //==============================================================================================
    parser_read(pfc, "minimal w value wmin", dval, found, show_params);
    if(found) inp.wmin=dval;

    parser_read(pfc, "number of points in w", ival, found, show_params);
    if(found) inp.nw=ival;
    
    //==============================================================================================
    parser_read(pfc, "minimal electron momentum p1min", dval, found, show_params);
    if(found) inp.p1min=dval;

    parser_read(pfc, "maximal electron momentum p1max", dval, found, show_params);
    if(found) inp.p1max=dval;

    parser_read(pfc, "number of points in p1", ival, found, show_params);
    if(found) inp.np=ival;
    
    parser_read(pfc, "p1 start index", ival, found, show_params);
    if(found) inp.p1start=ival;

    parser_read(pfc, "p1 end index", ival, found, show_params);
    if(found) inp.p1end=ival;

    //==============================================================================================
    parser_read(pfc, "electron temperature in me c^2", dval, found, show_params);
    if(found) inp.Th_e=dval;

    parser_read(pfc, "electron temperature in K", dval, found, show_params);
    if(found) inp.Th_e=const_kb_mec2*dval;

    parser_read(pfc, "xmin", dval, found, show_params);
    if(found) inp.xmin=dval;

    parser_read(pfc, "xmax", dval, found, show_params);
    if(found) inp.xmax=dval;

    parser_read(pfc, "number of points in x", ival, found, show_params);
    if(found) inp.nx=ival;

    parser_read(pfc, "number of threads", ival, found, show_params);
    if(found) inp.nthreads=ival;
    
    parser_read(pfc, "low frequency integrator", sval, found, show_params);
    if(found) inp.low_freq_integrator=sval;

    parser_read(pfc, "high frequency integrator", sval, found, show_params);
    if(found) inp.high_freq_integrator=sval;

    parser_read(pfc, "cross section limit", sval, found, show_params);
    if(found) inp.sel=sval;

    parser_read(pfc, "thermal averaging limit", sval, found, show_params);
    if(found) inp.therm_av_limit=sval;
    
    parser_read(pfc, "Gaunt factor batch mode", sval, found, show_params);
    if(found) inp.batchmode=sval;
    
    parser_read(pfc, "The_min", dval, found, show_params);
    if(found) inp.The_min=dval;
    
    parser_read(pfc, "The_max", dval, found, show_params);
    if(found) inp.The_max=dval;
    
    parser_read(pfc, "number of points in The", ival, found, show_params);
    if(found) inp.nThe=ival;

    if(show_params) cout << endl;

    return;
}

//==================================================================================================
