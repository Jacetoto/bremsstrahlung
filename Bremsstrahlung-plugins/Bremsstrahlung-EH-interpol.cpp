//==================================================================================================
vector<double> xivect_interpol;
vector<double> Vvect_interpol, Wvect_interpol, ImVsWvect_interpol;
vector<double> A2G0_interpol, W2G0_interpol;

void clear_Hyper_interpol_vectors()
{
    xivect_interpol.clear();
    Vvect_interpol.clear();
    Wvect_interpol.clear();
    ImVsWvect_interpol.clear();
    A2G0_interpol.clear();
    W2G0_interpol.clear();

    return;
}

//==================================================================================================
double ximin_interpol, ximax_interpol;
double glob_a1_interpol=-1.0, glob_a2_interpol=-1.0, glob_Da_interpol=-1.0;
vector<int> Intpol_mem_index;

void arm_V_W_ImVsW_interpolation(Momenta_and_Angles &V, int Z, int verbosity=0)
{
    double ainf =const_alpha*Z;
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2, Da=Deta1eta2(V, Z);
    
    // only redo tables if p1 and p2 have changed...
    if(glob_a1_interpol==a1 && glob_a2_interpol==a2 && glob_Da_interpol==Da) return;
    else{ glob_a1_interpol=a1;  glob_a2_interpol=a2;  glob_Da_interpol=Da; }
    
    double o2=V.omega*V.omega, mu=pow(V.p1+V.p2, 2)-pow(V.omega, 2);
    double kap1=2.0*(V.gamma1+V.p1), kap2=2.0*(V.gamma2+V.p2);
    double q2=pow(V.p1-V.p2-V.omega, 2);
    double ximin=max(1.0e-6, mu*q2/(kap1*kap2*o2))/1.1;
    
    kap1=2.0*(V.gamma1-V.p1); kap2=2.0*(V.gamma2-V.p2);
    q2=pow(V.p1+V.p2+V.omega, 2);
    double ximax=mu*q2/(kap1*kap2*o2)*1.1;

    int logdens=600; // logdens points per decade
    int np=min(max(1000, (int)(log10(ximax/ximin)*logdens)), 20000);
    
    // setup memory
    clear_Hyper_interpol_vectors();
    xivect_interpol.resize(np);
    Vvect_interpol.resize(np); Wvect_interpol.resize(np); ImVsWvect_interpol.resize(np);
    
    // fill array
    init_xarr(log(ximin), log(ximax), &xivect_interpol[0], np, 0, 0);
    ximin_interpol=ximin; ximax_interpol=ximax;
    
    double V2, W2, ImVsW;
    
    if(verbosity>0)
        cout << "\n arm_V_W_ImVsW_interpolation:: xi= " << ximin << ".." << ximax << " np= " << np << endl;
    
    ofstream ofile;
    if(verbosity>1) {
        bool dir_c=check_if_dir_exists("./temp");
        if(!dir_c) create_directory_if_it_does_not_exist("./temp");
        
        ofile.open("./temp/hyper.dat");
        ofile.precision(14);
    }
    
    for(int k=0; k<np; k++)
    {
        double xi=exp(xivect_interpol[k]);
        Eval_V2W2ImVsW_func(a1, a2, ainf, xi, V2, W2, ImVsW);
        Vvect_interpol[k]=V2; Wvect_interpol[k]=W2-V2; ImVsWvect_interpol[k]=ImVsW-V2;
        
        if(verbosity>1) ofile << xi << " " << V2 << " " << W2 << " " << ImVsW << endl;
    }

    if(Intpol_mem_index.size()==0)
    {
        Intpol_mem_index.resize(3);
        Intpol_mem_index[0]=calc_spline_coeffies_JC(np, &xivect_interpol[0], &Vvect_interpol[0], "V2");
        Intpol_mem_index[1]=calc_spline_coeffies_JC(np, &xivect_interpol[0], &Wvect_interpol[0], "DW2");
        Intpol_mem_index[2]=calc_spline_coeffies_JC(np, &xivect_interpol[0], &ImVsWvect_interpol[0], "DImVsW");
    }
    else
    {
        update_spline_coeffies_JC(Intpol_mem_index[0], np, &xivect_interpol[0], &Vvect_interpol[0], "V2");
        update_spline_coeffies_JC(Intpol_mem_index[1], np, &xivect_interpol[0], &Wvect_interpol[0], "DW2");
        update_spline_coeffies_JC(Intpol_mem_index[2], np, &xivect_interpol[0], &ImVsWvect_interpol[0], "DImVsW");
    }

    if(verbosity>1) ofile.close();
    if(verbosity>0) cout << " arm_V_W_ImVsW_interpolation:: done. \n" << endl;
    
    return;
}

int Eval_V2W2ImVsW_interpol(double xi, double &V2, double &W2, double &ImVsW)
{
    double logxi=log(xi);
    if(xi<ximin_interpol || xi>ximax_interpol)
    {
        cout << ximin_interpol << " " << xi << " " << ximax_interpol << endl;
        throw_error("Eval_V2W2ImVsW_interpol", "out of range", 1);
    }
    
    V2=calc_spline_JC(logxi, Intpol_mem_index[0]);
    W2=calc_spline_JC(logxi, Intpol_mem_index[1])+V2;
    ImVsW=calc_spline_JC(logxi, Intpol_mem_index[2])+V2;
    
    //cout << xi << " " << V2 << " " << W2 << " " << ImVsW << endl;
    
    return 0;
}

//==================================================================================================
double ximin_interpol_II, ximax_interpol_II;
double glob_a1_interpol_II=-1.0, glob_a2_interpol_II=-1.0, glob_Da_interpol_II=-1.0;
vector<int> Intpol_mem_index_xi;

void arm_V_W_ImVsW_interpolation_xi(Momenta_and_Angles &V, int Z, string sel, int verbosity=0)
{
    if(!(sel=="A2W2" || sel=="G0")) throw_error("arm_V_W_ImVsW_interpolation_xi", "unknown mode", 1);
    
    double ainf =const_alpha*Z;
    double a1=ainf*V.gamma1/V.p1, a2=ainf*V.gamma2/V.p2, Da=Deta1eta2(V, Z);

    // only redo tables if p1 and p2 have changed...
    if(glob_a1_interpol_II==a1 && glob_a2_interpol_II==a2 && glob_Da_interpol_II==Da) return;
    else{ glob_a1_interpol_II=a1;  glob_a2_interpol_II=a2;  glob_Da_interpol_II=Da; }

    V.compute_derived_values();

    double ximin=1.0+1.0e-10;
    double ximax=V.xi_interpol*1.1;
    
    int logdens=200; // logdens points per decade in xi-1
    int np=min(max(1000, (int)(log10((ximax-1.0)/(ximin-1.0))*logdens)), 20000);
    
    // setup memory
    clear_Hyper_interpol_vectors();
    xivect_interpol.resize(np);
    A2G0_interpol.resize(np); W2G0_interpol.resize(np);
    
    // fill array with xi values
    init_xarr(ximin-1.0, ximax-1.0, &xivect_interpol[0], np, 1, 0);
    // convert to 1-xi for ODE function
    for(int k=0; k<np; k++) xivect_interpol[k]=-xivect_interpol[k];
    ximin_interpol_II=ximin; ximax_interpol_II=ximax;

    if(verbosity>0)
        cout << "\n arm_V_W_ImVsW_interpolation_xi:: xi= "
             << ximin << " to " << ximax << " np= " << np
             << " p1= " << V.p1 << " w= " << V.w << endl;
    
    // compute G0 and G0'
    int err=G0x_function(a1, a2, Da, xivect_interpol, W2G0_interpol, A2G0_interpol);

    // store results if needed and convert
    ofstream ofile;
    if(verbosity>1) {
        bool dir_c=check_if_dir_exists("./temp");
        if(!dir_c) create_directory_if_it_does_not_exist("./temp");

        ofile.open("./temp/hyper_xi.dat");
        ofile.precision(14);
    }

    // convert to interpolation functions
    double etap=a1+a2, etam=Da;
    for(int k=0; k<np; k++)
    {
        xivect_interpol[k]=1.0-xivect_interpol[k]; // 1-xi --> xi
        double xi=xivect_interpol[k];

        if(sel=="A2W2")
        {
            // G0, G0' --> W^2 and A^2 > 0
            W2G0_interpol[k]=pow(xi*W2G0_interpol[k]/(1.0-xi), 2);
            A2G0_interpol[k]=pow(etap/xi+etam, 2)/4.0*W2G0_interpol[k]+pow(xi*A2G0_interpol[k], 2);
            
            // add factor of a1^2 per definition
            W2G0_interpol[k]=pow(a1, 2)*W2G0_interpol[k];
        }
        else if(sel=="G0")
        {
            W2G0_interpol[k]=pow(W2G0_interpol[k], 2);
            A2G0_interpol[k]=pow(A2G0_interpol[k], 2);
        }
            
        // xi to log(xi-1)
        xivect_interpol[k]=log(xivect_interpol[k]-1.0);

        //double W2=(xi>1.3 ? W2G0_interpolated(xi, V.p1, Z) : W2G0_interpol[k]);
        //double A2=(xi>1.3 ? A2G0_interpolated(xi, V.p1, Z) : A2G0_interpol[k]);

        //ofile << xi-1.0 << " " << W2G0_interpol[k] << " " << A2G0_interpol[k] << " " << W2 << " " << A2 << endl;
        if(verbosity>1) ofile << xi-1.0 << " " << W2G0_interpol[k] << " " << A2G0_interpol[k] << endl;

        W2G0_interpol[k]=log(W2G0_interpol[k]);
        A2G0_interpol[k]=log(A2G0_interpol[k]);
    }

    if(Intpol_mem_index_xi.size()==0)
    {
        Intpol_mem_index_xi.resize(2);
        Intpol_mem_index_xi[0]=calc_spline_coeffies_JC(np, &xivect_interpol[0], &A2G0_interpol[0], "A2DG0-xi");
        Intpol_mem_index_xi[1]=calc_spline_coeffies_JC(np, &xivect_interpol[0], &W2G0_interpol[0], "W2G0-xi");
    }
    else
    {
        update_spline_coeffies_JC(Intpol_mem_index_xi[0], np, &xivect_interpol[0], &A2G0_interpol[0], "A2DG0-xi");
        update_spline_coeffies_JC(Intpol_mem_index_xi[1], np, &xivect_interpol[0], &W2G0_interpol[0], "W2G0-xi");
    }
    
    if(verbosity>1) ofile.close();
    if(verbosity>0) cout << " arm_V_W_ImVsW_interpolation_xi:: done. \n" << endl;
    
    return;
}
int Eval_A2W2_interpol_xi(double xi, double &A2, double &W2, int verbose)
{
    double logxi=log(xi-1.0);
    if(xi<ximin_interpol_II || xi>ximax_interpol_II)
    {
        cout.precision(16);
        cout << endl << ximin_interpol_II << " " << xi << " " << ximax_interpol_II << endl;
        throw_error("Eval_A2W2_interpol_xi", "out of range", 1);
    }
    
    A2=exp(calc_spline_JC(logxi, Intpol_mem_index_xi[0]));
    W2=exp(calc_spline_JC(logxi, Intpol_mem_index_xi[1]));
    
    if(verbose>0) cout << " ODE-int  " << xi << " " << W2 << " " << A2 << endl;
    
    return 0;
}

//==================================================================================================
//==================================================================================================
