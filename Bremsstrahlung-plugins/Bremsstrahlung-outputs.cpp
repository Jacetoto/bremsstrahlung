//==================================================================================================
void output_dsig_dkdOmega1dOmega2(string name, double p1, double om,
                                  double mu1, double mu2, string sel, int npts=30)
{
    vector<double> phiarr(npts);
    init_xarr(0.0, TWOPI, &phiarr[0], npts, 0, 0);
    
    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_dsig_dkdOmega1dmu2:: computing case = " << sel;
    cout << " using " << selected_variables << " variables" << endl;
    
    for(int k=0; k<(int)phiarr.size(); k++)
    {
        double phi=phiarr[k];
        double y=dsigma_dkdOmega1dOmega2(p1, om, mu1, mu2, phi, 1, sel);
        
        cout << "\33[2K" << " " << phi << " " << y << endl << "\x1b[A";
        ofile << phi << " " << y << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;

    return;
}

//==================================================================================================
void output_dsig_dkdOmega1dOmega2_xi(string name, double p1, double om,
                                     double mu1, double mu2, string sel, int npts=30)
{
    set_integration_scheme("Patterson-xi");
    vector<double> Dxiarr(npts);
    
    Momenta_and_Angles V;
    V.set_values(p1, om, mu1, mu2, 0.0);
    init_xarr(1.0e-10, V.Dxitot, &Dxiarr[0], npts, 1, 0);
    //init_xarr(V.Dxitot/10.0, V.Dxitot, &Dxiarr[0], npts, 1, 0);

    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_dsig_dkdOmega1dmu2_xi:: computing case = " << sel;
    cout << " using " << selected_variables << " variables" << endl;
    
    for(int k=0; k<(int)Dxiarr.size(); k++)
    {
        double Dxi=Dxiarr[k];
        V.set_values_xi(p1, om, mu1, mu2-mu1, Dxi);
        double y=dsigma_dkdOmega1dOmega2_xi(p1, om, mu1, mu2-mu1, Dxi, 1, sel);
        
        cout << "\33[2K" << " " << Dxi << " " << y << endl << "\x1b[A";
        ofile << V.xi << " " << Dxi*y << endl;
        //ofile << Dxi << " " << y << endl;
        //ofile << V.phi << " " << y << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_dsig_dkdOmega1dmu2(string name, double p1, double om, double mu1, string sel, int npts=30)
{
    set_integration_scheme("CUBA");
    vector<double> mu2arr(npts);
    init_xarr(-0.99999, 0.99999, &mu2arr[0], npts, 0, 0);
    //init_xarr(mu1-0.001, mu1+0.001, &mu2arr[0], npts, 0, 0);

    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_dsig_dkdOmega1dmu2:: computing case = " << sel;
    cout << " using " << selected_variables << " variables" << endl;

    for(int k=0; k<(int)mu2arr.size(); k++)
    {
        double mu2=mu2arr[k];
        double y=dsigma_dkdOmega1dmu2(p1, om, mu1, mu2, 1, sel);
        
        cout << "\33[2K" << " " << mu2 << " " << y << endl << "\x1b[A";
        ofile << mu2 << " " << y << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_dsig_dkdOmega1(string name, double p1, double om, string sel, int npts=30)
{
    set_integration_scheme("Patterson-xi");
    vector<double> mu1arr(npts);
    init_xarr(-1.0, 0.9999, &mu1arr[0], npts, 0, 0);
    
    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_dsig_dkdOmega1:: computing case = " << sel;
    cout << " using " << selected_variables << " variables" << endl;
    
    for(int k=0; k<(int)mu1arr.size(); k++)
    {
        double mu1=mu1arr[k];
        double y=dsigma_dkdOmega1(p1, om, mu1, 1, sel);
        
        cout << "\33[2K" << " " << mu1 << " " << y << endl << "\x1b[A";
        ofile << mu1 << " " << y << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_dsig_dk(string name, double p1, double ol, double oh, string sel, int Z, int npts=30)
{
    set_integration_scheme("Patterson-xi");
    
    vector<double> warr(npts);
    init_xarr(ol, oh*0.1, &warr[0], npts/2, 1, 0);
    init_xarr(oh*0.1, oh*0.99999, &warr[npts/2-1], npts+1-npts/2, 0, 0);

    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_dsig_dk:: computing case = " << sel;
    cout << " using " << selected_variables << " variables" << endl;
    
    for(int k=0; k<(int)warr.size(); k++)
    {
        double o=warr[k];
        double y=dsigma_dk_sel(p1, o, Z, sel);
        
        cout << "\33[2K" << " " << o/oh << " " << y << endl << "\x1b[A";
        ofile << o/oh << " " << y << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;

    return;
}

//==================================================================================================
void output_Gaunt_factors(string name, const vector<double> &pvals, double wmin, string sel, int Z, int nw=30)
{
    bool mess=0;
    if(sel=="BH" || sel=="EH" || sel=="DEH" || sel=="EH-interpol" || sel=="EH-soft") mess=1;
    
    vector<double> warr(nw);
    //setup_w_grid(wmin, warr, "lin_high");
    setup_w_grid(wmin, warr, "reverse_log");

    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    int np=pvals.size();

    bool switch_integrator=1;                // for w>w_switch can switch to Patterson/CUBA
    double w_switch=1.0e-2;
    
    string low_freq_integrator="Patterson-xi"; // "CUBA-xi"; // 
    string high_freq_integrator="Patterson"; // "CUBA-xi"; // "CUBA"; // "Patterson-xi"; //
    set_integration_scheme(low_freq_integrator, mess);

    cout << " output_Gaunt_factors:: computing case = " << sel;
    cout << " using " << selected_variables << " variables for Z = " << Z << endl;
    
    ofile << "# w and p1= ";
    for(int m=0; m<np; m++) ofile << pvals[m] << " ";
    ofile << endl;
    
    for(int k=0; k<(int)warr.size(); k++)
    {
        double w=warr[k];

        if(w>w_switch && switch_integrator)
        {
            set_integration_scheme(high_freq_integrator, mess);
            switch_integrator=0;
        }

        cout << "\33[2K" << " " << w << " " << flush;
        ofile << w << " ";
        
        for(int m=0; m<np; m++)
        {
            double p1=pvals[m];
            if(p1<=0.0) throw_error("output_Gaunt_factors", "p1<=0", 1);

            cout << "p1 = " << p1 << " " << flush;

            //double y=Gaunt_sel(p1, w*Emax_func(p1), Z, sel);
            //double y=Gaunt_sel_rel(p1, w*Emax_func(p1), Z, sel);
            double y=Gaunt_sel_BH(p1, w*Emax_func(p1), Z, sel);

            cout << y << " " << flush;
            ofile << y << " ";
            
            // Karzas & Latter case for illustration only
            //ofile << Gaunt_interpolated_NR_KL(p1, w*p1*p1/2.0, Z)/Gaunt_BH_ana(p1, w*Emax_func(p1), Z) << " ";
        }
        
        cout << endl << "\x1b[A";
        ofile << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;

    return;
}

//==================================================================================================
void output_Gaunt_factors(string name, double p1, double wmin, string sel, int Z, int nw=30)
{
    vector<double> pvals;
    pvals.push_back(p1);
    
    output_Gaunt_factors(name, pvals, wmin, sel, Z, nw);
    
    return;
}

//==================================================================================================
void output_Gaunt_factors(string name, double wmin, string sel, int Z, int nw=30)
{
    vector<double> pvals;
    for(int k=1; k<=8; k++) pvals.push_back(1.0e-6*k); // paper cases
 
    output_Gaunt_factors(name, pvals, wmin, sel, Z, nw);

    return;
}

//==================================================================================================
void output_interpolated_Gaunt_factors(string name, double ol_oh, string sel, int npts=30)
{
    double (* Gaunt_f)(double p1, double omega, int Z);
    
    if(sel=="NR") Gaunt_f=Gaunt_interpolated_NR;
    else throw_error("output_interpolated_Gaunt_factors", "case not implented", 1);
    
    vector<double> warr(npts);
    setup_w_grid(0.1, warr, "lin_high");

    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    int np=8;
    double p1v[8]={0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 0.5, 0.7};
    
    cout << " output_interpolated_Gaunt_factors:: computing case = " << sel << endl;
    
    for(int k=0; k<(int)warr.size(); k++)
    {
        double w=warr[k];
        cout << "\33[2K" << " " << w << " " << flush;
        ofile << w << " ";
        
        for(int m=0; m<np; m++)
        {
            double p1=p1v[m];
            double y=Gaunt_f(p1, w*Emax_func(p1), 1);
            
            cout << y << " " << flush;
            ofile << y << " ";
        }
        
        cout << endl << "\x1b[A";
        ofile << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_thermally_averaged_Gaunts(string name, double Th_e, int Z,
                                      double xl, double xh,
                                      string sel, string therm_av_limit, int npts=30)
{
    int verbosity=0;
    
    double (* Gaunt_f)(double p1, double omega, int Z);
    
    if(sel=="NR") Gaunt_f=Gaunt_interpolated_NR;
    else if(sel=="NR-KL") Gaunt_f=Gaunt_interpolated_NR_KL;
    else if(sel=="NR-rel") Gaunt_f=Gaunt_interpolated_NR_rel_corr;
    else if(sel=="EH") Gaunt_f=Gaunt_interpolated_EH;
    else if(sel=="Gen") Gaunt_f=Gaunt_interpolated_general;
    else if(sel=="BH") Gaunt_f=Gaunt_BH_ana;
    else throw_error("output_thermally_averaged_Gaunts", "case not implented", 1);

    vector<double> xarr(npts);
    init_xarr(xl, xh, &xarr[0], npts, 1, 0);
    
    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    cout << " output_thermally_averaged_Gaunt_factors:: computing case = " << sel
         << " for Th_e = " << Th_e << " or Te = " << Th_e/const_kb_mec2 << " K" << endl;
    cout << " using thermal averaging limit '" << therm_av_limit << "'"
         << " for Z= " << Z << endl;
    
    double y, ypre=0.0;
    bool eval=1;
    for(int k=0; k<(int)xarr.size(); k++)
    {
        double x=xarr[k];
        double yBH=(sel=="NR-KL" ? 1.0 :  // avoid calling functions with inconsistent energies
                    Thermally_averaged_gaunt_factor(x*Th_e, Th_e, Z, Gaunt_BH_ana, therm_av_limit));

        if(eval) y= Thermally_averaged_gaunt_factor(x*Th_e, Th_e, Z, Gaunt_f     , therm_av_limit);
        
        cout << "\33[2K" << " " << x << " " << y << endl << "\x1b[A";
        ofile << x << " " << y << " " << y/yBH << endl;
        
        // estimate derivative to stop at high frequencies
        if(x>100.0 && eval && therm_av_limit=="exact-hf")
        {
            double dgdx=(y-ypre)/(x-xarr[k-1]);
            if(abs(x/y*dgdx)<1.0e-5) eval=0;
        }
        else if(!eval && verbosity>0) cout << " extrapolation " << x << " " << y << endl;
            
        ypre=y;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
void output_Gaunt_factors_Z(string name, double wmin, double p1, string sel, int nw=30)
{
    vector<double> warr(nw);
    //setup_w_grid(wmin, warr, "lin_high");
    setup_w_grid(wmin, warr, "reverse_log");
    
    ofstream ofile(name.c_str());
    ofile.precision(16);
    
    bool switch_integrator=1;               // for w>w_switch can switch to Patterson/CUBA
    double w_switch=1.0e-2;
    
    string low_freq_integrator="Patterson-xi"; // "CUBA-xi"; // 
    string high_freq_integrator="Patterson"; // "CUBA-xi"; // "CUBA"; // "Patterson-xi"; //
    set_integration_scheme(low_freq_integrator, 0);
    
    cout << " output_Gaunt_factors:: computing case = " << sel;
    cout << " using " << selected_variables << " variables for p1 = " << p1 << endl;
    
    vector<int> Zarr;
    //Zarr.push_back(1); Zarr.push_back(10); Zarr.push_back(20); Zarr.push_back(30); Zarr.push_back(40);
    for(int Z=1; Z<=10; Z++) Zarr.push_back(Z);
    
    for(int k=0; k<(int)warr.size(); k++)
    {
        double w=warr[k];
        
        if(w>w_switch && switch_integrator)
        {
            set_integration_scheme(high_freq_integrator, 0);
            switch_integrator=0;
        }
        
        cout << "\33[2K" << " " << w << " " << flush;
        ofile << w << " ";
        
        for(int iZ=0; iZ<(int)Zarr.size(); iZ++)
        {
            int Z=Zarr[iZ];
            double p1eff=p1*Z;
            double y=Gaunt_sel_BH(p1eff, w*Emax_func(p1eff), Z, sel);
            
            cout << "Z= " << Z << " " << y << " " << flush;
            ofile << y << " ";
        }
        
        cout << endl << "\x1b[A";
        ofile << endl;
    }
    
    cout << "\33[2K" << " done " << endl << endl;
    
    return;
}

//==================================================================================================
//==================================================================================================
