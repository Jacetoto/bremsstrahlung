//==================================================================================================
// total cross section in classical Kramers' approximation
//==================================================================================================
double dsigma_dk_K(Momenta_and_Angles &V, int Z)
{
    int err=V.check_values();
    if(err==1){ cout << " dsigma_dk_K :: zero value" << endl; V.show(); return 0.0; }
    
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    sigT*=norm_function(V.p1, V.omega, Z);
    
    return sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(V.p1*V.p1 * V.omega); // Kramers confirmed from K&L
}

//==================================================================================================
// total cross section in non-relativistic limit
//==================================================================================================
int evaluate_G_ell(double eta_i, double eta_f, double Deta_if, int l, double &G_ell)
{
    complex<double> a(1.0+l, eta_f), b(1.0+l, eta_i), Hy;
    complex<double> etap(0.0, (eta_i+eta_f)/2.0);
    int c=2*(1+l);
    double x=-4.0*eta_i*eta_f/pow(Deta_if, 2);
    
    //int err=Hyper_Geo_2F1(a, b, c, x, -PI*eta_i, Hy);
    //G_ell=real(pow(-x, l+1)*pow(1.0-x, etap)*Hy);
    int err=G_ell_function(eta_i, eta_f, Deta_if, l, x, G_ell);

    //cout << " G_ell " << x << " " << G_ell << " " << l << endl;
    
    return err;
}

//==================================================================================================
double dsigma_dk_NR_soft(Momenta_and_Angles &V, int Z)
{
    // NR cross section integrated over all angles (Karzas & Latter)
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    
    double a1=const_alpha*Z*V.gamma1/V.p1, a2=const_alpha*Z*V.gamma2/V.p2, Da1a2=Deta1eta2(V, Z);
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double norm= sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(V.p1*V.p1 * V.omega); // Kramers formula
    
    double ReJ, ImJ;
    Re_Im_J_GSL(a1, a2, Da1a2, 0, ReJ, ImJ);
    double z=Da1a2*Da1a2/(4.0*a1*a2); // == -1/x
    double A=4.0*(0*ReJ*Da1a2/2.0+ImJ*a1*a2)*Da1a2/(a1*a2);
    double B=4.0*(0*ImJ*Da1a2/2.0+ReJ*a1*a2)*Da1a2/(a1*a2);
    double G0=ReJ*cos(Da1a2/2.0*log(z))-ImJ*sin(Da1a2/2.0*log(z));
    double DG=A*cos(Da1a2/2.0*log(z))-B*sin(Da1a2/2.0*log(z));
    double gNR=sqrt(3.0)/FOURPI * F_NR(a1, a2) * G0 * DG;
    
    double M2=gNR;
    //M2*=F_EWF(a1, a2);
    
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
double dsigma_dk_NR(Momenta_and_Angles &V, int Z)
{
    //if(V.omega/V.E1<1.0e-4) return dsigma_dk_NR_soft(V, Z);
    
    // NR cross section integrated over all angles (Karzas & Latter)
    int err=V.check_values();
    if(err==1){ cout << " zero value" << endl; V.show(); return 0.0; }
    
    double a1=const_alpha*Z*V.gamma1/V.p1, a2=const_alpha*Z*V.gamma2/V.p2, Da1a2=Deta1eta2(V, Z);
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double norm= sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(V.p1*V.p1 * V.omega); // Kramers formula

    // Gaunt-factor (eta_i == a1 and eta_f == a2)
    double G0, G1;
    evaluate_G_ell(a1, a2, Da1a2, 0, G0);
    evaluate_G_ell(a1, a2, Da1a2, 1, G1);
    
    double gNR=sqrt(3.0)/FOURPI * F_NR(a1, a2) * G0
             *( (a1*a2 + (a1/a2+a2/a1)/2.0 )*G0-(1.0+a1*a1)*(1.0+a2*a2)/6.0*G1 );

    double M2=gNR;
    
    return norm*M2*norm_function(V.p1, V.omega, Z);
}

//==================================================================================================
// Gaunt-factors
//==================================================================================================
double Gaunt_NR(double p1, double omega, int Z)
{
    if(omega/Emax_func(p1)>0.999999999) omega=0.999999999*Emax_func(p1);
    Momenta_and_Angles V(p1, omega, 0.0);
    
    return dsigma_dk_NR(V, Z)/dsigma_dk_K(V, Z);
}

double Gaunt_NR_soft(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    
    return dsigma_dk_NR_soft(V, Z)/dsigma_dk_K(V, Z);
}

double Gaunt_NR_rel_corr(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return V.gamma1*V.gamma1*Gaunt_NR(p1, omega, Z);
}

double Gaunt_NR_rel_corr_soft(double p1, double omega, int Z)
{
    Momenta_and_Angles V(p1, omega, 0.0);
    return V.gamma1*V.gamma1*Gaunt_NR_soft(p1, omega, Z);
}

//==================================================================================================
//
// explicit KL definitions
//
//==================================================================================================
double dsigma_dk_K_KL(double p1, double omega, int Z)
{
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    sigT*=norm_function(p1, omega, Z);
    
    return sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(p1*p1 * omega); // Kramers confirmed from K&L
}

//==================================================================================================
double dsigma_dk_NR_KL_soft(double p1, double omega, int Z)
{
    double p2=sqrt(p1*p1-2.0*omega);
    double a1=const_alpha*Z/p1, a2=const_alpha*Z/p2, Da1a2=Deta1eta2_nr(p1, p2, omega, Z);
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double norm= sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(p1*p1 * omega); // Kramers formula

    double ReJ, ImJ;
    Re_Im_J_GSL(a1, a2, Da1a2, 0, ReJ, ImJ);
    double z=Da1a2*Da1a2/(4.0*a1*a2); // == -1/x
    double A=4.0*ImJ*a1*a2*Da1a2/(a1*a2);
    double B=4.0*ReJ*a1*a2*Da1a2/(a1*a2);
    double G0=ReJ*cos(Da1a2/2.0*log(z))-ImJ*sin(Da1a2/2.0*log(z));
    double DG=A*cos(Da1a2/2.0*log(z))-B*sin(Da1a2/2.0*log(z));
    double gNR=sqrt(3.0)/FOURPI * F_NR(a1, a2) * G0 * DG;
    
    double M2=gNR;
    
    return norm*M2*norm_function(p1, omega, Z);
}

double dsigma_dk_NR_KL(double p1, double omega, int Z)
{
    double p2=sqrt(p1*p1-2.0*omega);
    double a1=const_alpha*Z/p1, a2=const_alpha*Z/p2, Da1a2=Deta1eta2_nr(p1, p2, omega, Z);
    double sigT=const_sigT_0*1e+27; // in mbarn == 10^-27 cm^2
    double norm= sigT * const_alpha*Z*Z * 2.0/sqrt(3.0) /(p1*p1 * omega); // Kramers formula
    
    // Gaunt-factor (eta_i == a1 and eta_f == a2)
    double G0, G1;
    evaluate_G_ell(a1, a2, Da1a2, 0, G0);
    evaluate_G_ell(a1, a2, Da1a2, 1, G1);
    
    double gNR=sqrt(3.0)/FOURPI * F_NR(a1, a2) * G0
             *( (a1*a2 + (a1/a2+a2/a1)/2.0 )*G0-(1.0+a1*a1)*(1.0+a2*a2)/6.0*G1 );
    
    double M2=gNR;
    
    return norm*M2*norm_function(p1, omega, Z);
}
double Gaunt_NR_KL(double p1, double omega, int Z)
{
    if(omega*2.0/(p1*p1)>0.999999999) omega=0.999999999*(p1*p1)/2.0;
    
    return dsigma_dk_NR_KL(p1, omega, Z)/dsigma_dk_K_KL(p1, omega, Z);
}

double Gaunt_NR_KL_soft(double p1, double omega, int Z)
{ return dsigma_dk_NR_KL_soft(p1, omega, Z)/dsigma_dk_K_KL(p1, omega, Z); }

//==================================================================================================
//==================================================================================================
