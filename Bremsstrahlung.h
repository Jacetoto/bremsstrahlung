//==================================================================================================
//
// This code is compute the free-free Gaunt factors and integrated cross sections in the general
// case. The expressions from Elwert & Haug, 1969 are used.
//
//==================================================================================================
//
// Author: Jens Chluba
// first implementation: Aug 2019
// last modification   : Aug 2019
//
//==================================================================================================

#ifndef BREMSSTRAHLUNG_H
#define BREMSSTRAHLUNG_H

//==================================================================================================
// Standards
//==================================================================================================
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <limits.h>
#include <vector>
#include <complex>
#include "Definitions.h"

//==================================================================================================
// namespaces
//==================================================================================================
using namespace std;

//==================================================================================================
// structures with variables
//==================================================================================================
struct Momenta_and_Angles
{
    double p1, p2, Dp21, E1, E2;
    double gamma1, gamma2;
    double omega, w;
    double mu1, mu2, Dmu21;    // Dmu21=mu2-mu1;
    double phi;
    double xi, Dxi;            // Dxi = xi - ximin; these variables are related to q2
    string info;
    double xi_interpol;        // info for interpolation scheme at xi < xi_interpol
    
    // derived variables
    double S1, S2, DS21;
    double ximin, Dxitot;
    double mut, o2;
    double kap1, kap2, kap12;
    
    void set_default_Momenta_and_Angles();
    Momenta_and_Angles();
    Momenta_and_Angles(double p1v, double omv, double mu1v);
    
    void set_values   (double p1v, double omv, double mu1v, double mu2v  , double phiv);
    void set_values_xi(double p1v, double omv, double mu1v, double Dmu21v, double Dxiv);
    int check_values();
 
    void compute_derived_values();
    void compute_energy_values();
    double compute_Dxi(double phi); // get xi value corresponding to chosen case
    void show();
};

//==================================================================================================
typedef double (*dsigma_func)(Momenta_and_Angles &V, int Z);

struct Precision_info
{
    double epsrel, epsabs;
    double hyper_precision_fac;
    double Int1_fac, Int2_fac;
    
    Precision_info()
    {
        epsrel=5.0e-5;
        epsabs=1.0e-60;
        Int1_fac=4.0;
        Int2_fac=2.0;
    }
};

struct Integration_variables
{
private:
    Precision_info P;
    
public:
    int Z;
    Momenta_and_Angles V;
    dsigma_func dsigma;
    
    const Precision_info& Get_Precision_info(){ return P; };
    
    int cbreg;

    Integration_variables()
    {
        Z=1.0;
        dsigma=NULL;
    }
    
    Integration_variables(int Zv, Momenta_and_Angles Vv, double (*dsigmav)(Momenta_and_Angles &V, int Z))
    {
        Z=Zv;
        V=Vv;
        dsigma=dsigmav;
    }
};

//==================================================================================================
double relative_precision_estimate(double epsrel, Momenta_and_Angles &V, int Z);
double absolute_precision_estimate(double epsrel, Momenta_and_Angles &V, int Z);

//==================================================================================================
double dsigma_dkdOmega1dmu2_EH(double p1, double omega, double mu1, double mu2, int Z);
double dsigma_dkdOmega1dmu2_BH(double p1, double omega, double mu1, double mu2, int Z);

//==================================================================================================
double dsigma_dkdOmega1_EH(double p1, double omega, double mu1, int Z);
double dsigma_dkdOmega1_BH(double p1, double omega, double mu1, int Z);

//==================================================================================================
double dsigma_dk_EH(double p1, double omega, int Z);
double dsigma_dk_BH(double p1, double omega, int Z);
double dsigma_dk_EH_diff(double p1, double omega, int Z);

//==================================================================================================
// integrated functions
//==================================================================================================
double dsigma_dk_K(double p1, double omega, int Z);
double dsigma_dk_BH_ana(double p1, double omega, int Z);
double dsigma_dk_NR(double p1, double omega, int Z);
double dsigma_dk_NR_soft(double p1, double omega, int Z);
double dsigma_dk_NR_rel(double p1, double omega, int Z);       // dsigma_NR * gamma1^2
double dsigma_dk_NR_rel_soft(double p1, double omega, int Z);  // dsigma_NR * gamma1^2

//==================================================================================================
// general interface for cross section cases
//--------------------------------------------------------------------------------------------------
// analytic:
// sel == "K"                            : Kramers' result
// sel == "NR", "NR-interpol", "NR-soft" : Karzas & Latter results
// sel == "BHa"                          : Bethe-Heitler analytic
// sel == "EH-Z-interpol"                : Interpolated Elwert-Haug result for Z=1, 2, 3 and 4
// sel == "Gen"                          : combination of NR, EH, BH to represent the full case
//--------------------------------------------------------------------------------------------------
// to add extra factor of gamma1^2 in NR case use
// sel == "NR-rel", "NR-interpol-rel", "NR-soft-rel"
//--------------------------------------------------------------------------------------------------
// numerical integration:
// sel = "EH", "EH-soft", "DEH", "BH"
//==================================================================================================
double dsigma_dk_sel(double p1, double omega, int Z, string sel="EH");

// same but using xi variables for numerical integration
double dsigma_dk_sel_xi(double p1, double omega, int Z, string sel="EH");

//==================================================================================================
// Gaunt-factors
//==================================================================================================
double Gaunt_BH_ana(double p1, double omega, int Z);
double Gaunt_NR(double p1, double omega, int Z);
double Gaunt_NR_soft(double p1, double omega, int Z);
double Gaunt_NR_KL(double p1, double omega, int Z);
double Gaunt_NR_KL_soft(double p1, double omega, int Z);

// uses g~sqrt(3)/(2 pi) * FEW (eta1, eta2) * A * (log(-x) - B) for extrapolation towards low w
double Gaunt_extrapolated_EH_soft(double p1, double omega, int Z);

//==================================================================================================
// general interface for Gaunt factor with options as for general cross section.
// xi variables are used when performing numerical integration.
//==================================================================================================
double Gaunt_sel    (double p1, double omega, int Z, string sel="EH");
double Gaunt_sel_rel(double p1, double omega, int Z, string sel="EH"); // renormalize by gamma1^2 ds_K
double Gaunt_sel_BH (double p1, double omega, int Z, string sel="EH"); // renormalize by ds_BH

//==================================================================================================
// general interface for thermally-averaged Gaunt factor.
// The thermal average is carried out using precomputed tables and analytic expressions
//--------------------------------------------------------------------------------------------------
// cases:
// sel == "NR", "KL", "NR-rel"           : Karzas & Latter results
// sel == "BHa"                          : Bethe-Heitler analytic
// sel == "Gen"                          : general Elwert-Haug results for Z=1, 2, 3 and 4
//==================================================================================================
double Gaunt_th_sel    (double Th_e, double xe, int Z, string sel="EH");
double Gaunt_th_sel_rel(double Th_e, double xe, int Z, string sel="EH");

// precomputed tables an high frequency extrapolation [xe=hnu/kTe]
double Gaunt_th_interpol(double Th_e, double xe, int Z);

#endif

//==================================================================================================
//==================================================================================================
